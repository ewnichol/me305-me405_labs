'''
@file   mainpage.py
@brief  This page contains a full list of projects for me 405 with links to further documentation.

@mainpage

@section sec_intro Introduction & Links
@author Enoch Nicholson

This section of the documentation portfolio showcases all of the work completed 
in ME 405. During Winter Quarter 2021.

Currently, this documentation fully captures the following assignments:
    
    Homeworks:
        * \ref pg_hw0
        * \ref pg_hw1
        
    Labs:
        * \ref pg_lab1
        * \ref pg_lab2
        * \ref pg_lab3
        * \ref pg_lab4
        * \ref term_proj

To return to the ME 305 - ME 405 Landing Page use this link:
    
https://ewnichol.bitbucket.io/

The mainpage file for this documentation can be found here: 
    
https://bitbucket.org/ewnichol/me305-me405_labs/src/master/mainpage.py

@page pg_hw0 HW 0x00: State Transition Diagrams
@author E. Nicholson
@date January 11, 2021

This homework asignment was designed to get us comfortable working with finite 
state machines again. We were given an image of a simple vending machine and a 
set of behaviors. Using these, we developed a state transition diagram for an 
FSM that implemented these features.  

The following is an image of Vendotron.

@image html hw0x00_vendotron.png

The following is the state transition diagram I developed for the Vendotron FSM.

@image html hw0x00_stateflow.png

No python files were created for this assignemnt.

@page pg_hw1 HW 0x01: Python Review (Functions/Tuples)
@author E. Nicholson
@date January 13, 2021

This homework asignment was intended as a refresher on functions and variable types 
(specifically tuples). We were tasked to write a function that computes the correct
change given a purchase price, and a selection of coins/bills.

One python file was created for this assignemnt. The source code for this files can be found here:
    * HW_0x01_change.py: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me405/Homework/HW_0x01_change.py

@page pg_lab1 Lab 0x01: Reintroduction to Python and Finite State Machines
@author E. Nicholson
@date January 17, 2021

Lab 1 was designed to reintroduce coding in python and working with doxygen. 
Our task was to create a finite state machine that implemented behavior close to 
that of the vending machine in Homework 0x00.

The following is the updated state transition diagram I developed for the 
Vendotron FSM. I made some simplifications to the original HW 0x00 diagram 
based on the Lab 1 requirements.

@image html lab0x01_stateflow.png

One file was created for this assignemnt. The source code and documentation for this asignment can be found here:
    * LAB_0x01_Vendotron.py: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me405/me405%20Lab%200x01/LAB_0x01_Vendotron.py

@page pg_lab2 Lab 0x02: Think Fast!
@author E. Nicholson
@date January 24, 2021

Lab 2 challenged us to write a program that responded quickly and reliably to an
external event. We were asked to create a script that could acurately measure a
user's reaction time. To do this an interrupt triggered on the user's button press
was used. The links at the bottom of this page lead to detaild documentation
and the source code.

The Nucleo has two 80MHz 32 bit timers (2 and 5). I used Timer 5, and slowed it down
with a prescaler of 80 so that it counted in microseconds. I used a period of
0x7FFFFFFF, giving me ~36mins of time without overflow. To avoid ever reaching an
overflow state, I implemented a two minute timeout on the reaction test, and I
chose to reset the timer count for every test.

One file was created for this assignemnt. The source code and documentation for this asignment can be found here:
    * LAB_0x02_ThinkFast.py: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me405/me405%20Lab%200x02/LAB_0x02_ThinkFast.py

@page pg_lab3 Lab 0x03: Pushing the Right Buttons
@author E. Nicholson
@date January 31, 2021

Lab 3 was an opportunity for us to continue exploring how physically interfacing 
with our Nucleo works. We were asked to use one of the Nucleo's analog to digital 
converters to measure the voltage of the user button pin in response to a press.
It was also an opportunity to refresh ourselves on serial communication and
plotting in python.

I chose to repurpose two finite state machine from ME 305 Labs 6&7 that already 
implemented serial communication and plotting. Both FSMs had unneeded 
funtionality that I removed for simplicity. Once both peices of software were 
simplified, I added an interrupt to Back End FSM that signaled when the user 
pressed the Nucleo's button. On a button press, I set the Nucleo to collect a batch sample
of the user button's voltage. The Nucleo then transmits the data back to the PC. The 
analog to digital converter parameters needed some tuning so that I could reliably 
capture a clear step resposne. I found that a sampling timer frequency of
20kHz and a buffer size of 2000 was able to capture almost all button presses.
To handle the situations where an edge was not captured, I added code that checked 
for a voltage transition. If found, the data is sent, if not, the user is prompted 
to try again.

The following two diagrams layout the state transitions that occur in both FSMs.
The transitions are still quite similar to those used for ME 305's
Labs 6&7.

@image html lab0x03_FE_BE_FSMS.png

The following plot shows a step response captured by the Nucleo and plotted by
the PC. The user button on the Nucleo defaults to a high state. When it is pressed down
it is pulled low. This means that every button press generates two edges one falling 
edge and one rising edge. The Nucleo is trigged by the falling edge (High to Low),
it then captures the rising edge as the button is released. The reason that we see a 
first order response is the debouncing circuit connected to the user button. It is 
an RC circuit designed to filter out the unwanted edges created when the button is pressed.
The capacitor in this circuit cuases the voltage of button pin to respond
far slower than it otherwise would. This slow response is the reason we are able to
see the voltage transition on the user button pin.

@image html lab0x03_stepresponse.png

Two python files were created for this assignemnt. The source code and 
documentation for this asignment can be found below. The Back End UI file should be 
renamed main.py and placed on the Nucleo. A csv file containing the data shown 
in the step response plot is also liked below:
    * LAB_0x03_UI_FrontEnd.py: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me405/me405%20Lab%200x03/LAB_0x03_UI_FrontEnd.py
    * LAB_0x03_UI_BackEnd.py: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me405/me405%20Lab%200x03/LAB_0x03_UI_BackEnd.py
    * data_example_EWN.csv: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me405/me405%20Lab%200x03/data_example_EWN.csv
        
@page pg_lab4 Lab 0x04: Hot or Not?
@author E. Nicholson
@author K. Chuang
@date February 08, 2021

Lab 4 provided an opportunity for us to learn how to interface with I2C devices.
It was also a chance to practice using the file sharing capabilities of bitbucket.
For this partner asignment we used an MCP9808 temperature sensor on a breakout
board as the I2C practice device. We created a driver that allowed us to read 
ambient temperature from the sensor and obtain the manufacturere identification
number. I also built out additional methods that allowed access to the other features
on the sensor (setting alarms limits, setting device configration, reading identification
information, setting measurement resolution, and checking alert status). These
additional methods were written, but not tested by the date this asignment was 
submitted.

In addition to the driver, my partner ant I created a main file that used it and
a the ADCALL object within the pyb module to record time stamped ambient and 
CPU core temperature mesurements to a csv file. I spent some time trying to externally
power my board so that I could collect data away from my PC. Unfortunately I was 
unable to sucessfully retrieve a data file when not using USB power from my PC.
I believe that it may have something to do with the program being interrupted 
while writing to the csv, but I was not able to confirm this. The following plot 
shows 8 hours of temperature data recorded between Monday night and Tuesday morning
the week of February 8th 2021.

@image html lab0x04_temp_plot.png

Two python files were created for this assignemnt. The source code and 
documentation for this asignment can be found below. A csv file containing the 
data shown in the temperature plot is also liked below:
    * main.py: https://bitbucket.org/ewnichol/me405_shared_work/src/master/me405%20Lab%200x04/main.py
    * mcp9808.py: https://bitbucket.org/ewnichol/me405_shared_work/src/master/me405%20Lab%200x04/mcp9808.py
    * temp_data_EWN.csv: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me405/me405%20Lab%200x04/temp_data_EWN.csv
        
'''
"""
@file LAB_0x01_Vendotron.py
@brief This file contains a finite state machine that implements the behavior of a vending machine.
@author Enoch Nicholson
@date January 12, 2021
@deails     This file contains a finite state machine that implements the behavior
            of a vending machine. Users can use the 0-7 keys to inpud different
            coins/bills. The FSM track the balance, and when there are sufficent funds,
            one of four beverages can be dispensed. At any time, the user can
            use the eject button to return their money.
            
            The source code for this file can be found here: 
    
            https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me405/me405%20Lab%200x01/LAB_0x01_Vendotron.py
"""

import numpy as np
import keyboard

'''
Initialize code to run FSM (not the init state). This code initializes the
program, not the FSM
'''

## The current state of the Vendotron FSM.
state = 0

def on_keypress (thing):
    ''' 
    @brief Callback which runs when the user presses a key.
    @details This function was provided to us as a way to obtain user input 
             cooperatively. It defines a global variable pushed_key that represents
             string name of the current pressed key.
    '''
    
    global pushed_key

    pushed_key = thing.name

def addCoin(index, balance):
    '''
    @brief Adds the monetary value of an inserted coin to the vending machine balance
    @author Enoch Nicholson
    @date January 12, 2021
    
    @details This function takes and index interger 0-7 and the current balance.
             Using the following mapping:
                 0 = penny
                 1 = nickle
                 2 = dime
                 3 = quarter
                 4 = dollar bill
                 5 = five dollar bill
                 6 = ten dollar bill
                 7 = twenty dollar bill
             The function adds the coresponding number of cents to the balance
             and returns the new balance.
    @param index    The number used to communicate what denomination of currency was inserted.
    @param balance  The current balance of the vending machine. An interger value with units of cents. 
    @return The new balance
    '''
    if index >= 0 and index <= 7:
        balance += denom_arr[index]
    else:
        print('Invalid Input!')
    
    return balance

def getChange(balance):
    '''
    @brief Returns a tuple representing the highest denomination change breakdown for the given balance.
    @author Enoch Nicholson
    @date January 12, 2021
   
    @details This function accepts a positive interger balance in cents and returns a 
             tuple representing the highest denomination change breakdown for the 
             given balance.
    @param balance A positive interger representing the current Vendotron balance in cents.
    @return A tuple representing the highest denomination change breakdown for the 
             given balance.
    '''
    
    if balance >= 0:
        change = np.array([0,0,0,0,0,0,0,0])
        change_idx = 7
        change_bal = balance
        
        for val in list(reversed(denom_arr)):
            n = np.floor(change_bal/val)
            change[change_idx] = n
            change_bal -= n*val
            change_idx -= 1
            # print(change_bal)
        change = tuple(change)
    else:
        print('Invalid Balance')
        change = (0,0,0,0,0,0,0,0)
        
    return change

def printWelcome():
    '''
    @brief Prints a VendotronˆTMˆ welcome message with beverage prices
    @author Enoch Nicholson
    @date January 12, 2021
    
    @details This function prints a welcome message for Vendotron. The message
             list the avalable drinks and their prices as well as the current
             vending machine balance.
    '''
    
    print(
        '''Thank you for choosing Vendotron!!!
              Currently we offer the following refreshing beverages:
                  {:} -- {:}
                  {:} -- {:}
                  {:} -- {:}
                  {:} -- {:}
                  
              Balance -- {:}
              
        '''.format('Cuke', drinks['c'][1]/100,
                   'Popsi', drinks['p'][1]/100,
                   'Spryte', drinks['s'][1]/100,
                   'Dr. Pupper', drinks['d'][1]/100,
                   balance/100)
        )

while True:
    '''
    Implement FSM using a while loop and an if statement
    will run eternally until user presses CTRL-C
    '''

    if state == 0:
        '''
        Perform state 0 operations
        
        This init state, initializes the FSM itself, with all the
        code features already set up.
        '''
        
        ## The Vendotron balance in interger cents
        balance = 0
        
        ## Thr price of the currently selected item in interger cents
        price = 0
        
        ## The string describing the currently selected item
        selected = ''
        
        ## A dictionary containing the drinks option offered by vendotron. Drinks are
        # keyed by selection letter. Entries contain the name of the drink and 
        # its price.
        drinks = {
            'c':('Cuke',100),
            'p':('Popsi',120),
            's':('Spryte',85),
            'd':('Dr. Pupper',110)
            }
        
        ## An array containing the cent values of the seven denominations used.
        # This array is used to quickly convert from an index number to a cent 
        # value.
        denom_arr = np.array([1,5,10,25,100,500,1000,2000])
        
        ## A variable that updates wih the strin identifying the name of the currently pressed key.
        pushed_key = None
        
        keyboard.on_press (on_keypress)  # Set callback
        
        printWelcome()

        
        state = 1 # on the next iteration, the FSM will run state 1
    
    elif state == 1:
        '''
        perform state 1 operations
        '''
        
        if pushed_key in ('0','1','2','3','4','5','6','7'):
            balance = addCoin(int(pushed_key), balance)
            print('Balance -- {:}'.format(balance/100))
                        
        elif pushed_key in ('c','p','s','d'):
            price = drinks[pushed_key][1]
            selected = drinks[pushed_key][0]
            state = 2 # s1 -> s2
        elif pushed_key == 'e':
            state = 3 # s1 -> s3
        else:
            pass
        pushed_key = None
    
    elif state == 2:
        '''
        perform state 2 operations
        '''
        
        if balance < price:
            print('     Insufficent Funds! The Price is ${:}, You have ${:}'.format(price/100,balance/100))
            price = 0
            selected = None
            state = 1 # s2 -> s1
        elif balance >= price:    
            print('     Now vending a {:}! Please enjoy responsibly'.format(selected))
            balance -= price
            print('     You have ${:} remaining.'.format(balance))
            state = 3 # s2 -> s3
    
    elif state == 3:
        '''
        perform state 3 operations
        '''
        
        print('     Dispensing Change...')
        
        ## A tuple describing the change dispensed by Vendotron
        change = getChange(balance)
        print('''
                  Change Recieved:
                      {:} - Pennies
                      {:} - Nickles
                      {:} - Dimes
                      {:} - Quarters
                      {:} - Dollars
                      {:} - Fives
                      {:} - Tens
                      {:} - Twenties
              '''.format(change[0],
                         change[1],
                         change[2],
                         change[3],
                         change[4],
                         change[5],
                         change[6],
                         change[7]))
        balance = 0
        printWelcome()
        
        state = 1 # s3 -> s1
    
    else:
        '''
        this state shouldn't exist!
        '''
        pass
"""
@file LAB_0x02_ThinkFast.py
@brief A script that uses interrupts to test a user's reaction time.
@author Enoch Nicholson
@date January 24, 2021

@details     This script performs a reaction time test. Once begun, the program
            waits for a random time between 2-3 seconds. It then flashes the
            onboard LED of the Nucleo for one second. When the LED turns on a
            microsecond timer starts counting. When the user reacts by pressing 
            the blue user button, an interrupt is triggered, recording the timer 
            count at the time of the press. The prgram then validates the test,
            checking for early / multiple presses. If the button is not pressed,
            the program times out after 2 minutes, starting the test again. 
            
            The test is repeted, resetting the timer each test, until the user 
            exits using Ctrl+C. On exit, the program calculates the average 
            response time across all of the valid tests and displays it for 
            the user.
            
            The source code for this file can be found here: 
    
            https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me405/me405%20Lab%200x02/LAB_0x02_ThinkFast.py
"""

import pyb
import micropython
import random
import utime

## A pin object bound to the user button (Pin PC13)
user_button = pyb.Pin(pyb.Pin.cpu.C13, mode=pyb.Pin.IN)

## A pin object bound to the on board LED (Pin PA5)
board_LED = pyb.Pin(pyb.Pin.cpu.A5, mode=pyb.Pin.OUT)

## A timer object using Timer 5, configured to count every 1 microseconds.
# The period is set to the largest interger that does not necessitate memory
# allocation.
tim = pyb.Timer(5, prescaler=79, period=0x7FFFFFFF)

# Preallocation of memory incase the callback function throws an exception. 
#   This allows the user to view the exception.
micropython.alloc_emergency_exception_buf(100)

def user_button_pressed(IRQ_source):
    ''' 
    @brief Callback which runs when the user presses the blue input button on the Nucleo
    @details This function is triggered when the Nucleo's onboard button is pressed.
             It records the reaction timer count, indexes the number of presses,
             and check to see if the button was pressed early.
    @param IRQ_source The source of the interrupt. This must be taken as an arg, but is not used
    '''
    # This provides acress to global variables within the callback function
    global press_time, press_num, press_early, start_time, delay_time
    
    ## The time when the user button was pressed
    press_time = tim.counter()
    
    ## The number of presses during a test
    press_num += 1
    
    # print(utime.ticks_us())
    
    if utime.ticks_diff(utime.ticks_us(),start_time) < delay_time:
        ## A boolean indicating if the button was pressed early
        press_early = True

## The timer count when the user button was pressed
press_time = 0x7FFFFFFF

## The number of user button pressses that occured during a test period
press_num = 0

## A boolean that marks if the user button was pressed during the delay sequence.
press_early = False

## A list that stores all of the valid reaction times
reaction_log = []

## The system time that a test starts in microseconds
start_time = 0

## The callback for the user button. Activation occurs on a falling edge.
extint = pyb.ExtInt(user_button,pyb.ExtInt.IRQ_FALLING,pyb.Pin.PULL_NONE,user_button_pressed)
    
'''
This try-finally statement allows for the user to exit the program using the
keyboard interrupt. Once Ctrl+C is pressed the script enters the finally block,
computing the average of all the valid tests before exiting.
'''
try:
    print('\nUse Ctrl+c to stop the test at any time.\n')
    print('Testing will continue automatically until you exit.\n')
    input('     Press Enter to begin')
    while True:
        ## The start time of the test
        start_time = utime.ticks_us()
        print('Now Testing! Get Ready :) \n')
        ## A psudo randomly generated delay time in microseconds
        delay_time = random.randint(2000000,3000000)
        pyb.udelay(delay_time)
        board_LED.on()
        tim.counter(0)
        pyb.delay(1000)
        board_LED.off()
        
        while press_num == 0 and press_early == False:
            if tim.counter() > 0x7270E00:
                print(' Reaction test timeout! You are slow...')
                break
                
        
        if press_num == 1 and press_early == False:
            reaction_log.append(press_time)
            print('Successful run!')
        elif press_num >= 2 and press_early == True:
            print('Invalid! You pressed the button '+str(press_num)+' times and you pressed it early. Stop cheating!')
        elif press_num >= 2:
            print('Invalid! You pressed the button '+str(press_num)+' times. Stop cheating!')
        elif press_early == True:
            print('Invalid! You pressed the button early. Stop cheating!')
        else:
            pass
        press_time = 0x7FFFFFFF
        press_num = 0
        press_early = False
finally:
    reaction_log = [el for el in reaction_log if el != 0x7FFFFFFF]
    
    ## The number of valid tests recorded
    reaction_number = len(reaction_log)
    if reaction_number > 0:
        ## The average response time across all the valid tests.
        avg_resp_time = sum(reaction_log)/reaction_number
        
        print('There were '+str(reaction_number)+' attempts made!\n'+
              '     Your average response time was '+str(avg_resp_time)+' microseconds\n\n')
    else:
        print('No reactions were recorded! Please restart the program and try again.\n\n')
        

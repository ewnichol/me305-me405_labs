'''
@file   mainpage.py
@brief  This file contains term project documentation for ME405.

@page term_proj Term Project: Two Axis Balancing Platform
@author E. Nicholson
@date February 15, 2021

This section of the documentation portfolio is a landing page for all work 
associated with the ME405 term project.

Currently, this documentation captures the following portions of the project:
    
        
    Project Components:
        * \ref term_proj_modeling
        * \ref term_proj_model_testing
        * \ref rtp_driver_testing
        * \ref enc_m_drivers
        * \ref imu_driver
        * \ref signal_filtering
        * \ref final_int
    
@image html proj_view.jpg
    
@page imu_driver BNO055 IMU Driver
@author E. Nicholson
@date March 16, 2021

Due to the delays that shortened the time we had to spend with our hardware, interfacing
with the BNO055 Inertial Measurement Unit (IMU) was left as an optional deliverable.
Through researching digital filtering in Micropython, I found a GitHub repo with
a robust Micropython port of Adafruit's BNO055 circuit python driver by Peter Hinch.
This discovery greatly reduced my workload, and gave me enough time to incorporate 
the IMU into my final project.

@image html imu_view.jpg

Peter Hinch's repo can be found here:
    
https://github.com/micropython-IMU/micropython-bno055

After reading through the included documentation and the BNO055's chip data sheet, I
modified the included test script to run on the Nucleo instead of the pyboard, and I
changed the driver's unit scaling to return radian values. I initially stuggled
to get the IMU to read over I2C, but after reading Adafruit's data sheet for their 
BNO055 breakout, I decided to try adding extra pullups to the data and clock pins.
Placing a 2k resistor on the data line and a 3k resistor on the clock fixed my issues.
These were added in series with the 10k pull ups on the breakout, tying them to 5V. 
This was done before I knew about the 3.3k resistors on the ME 405 hardware kit that 
tied the clock and data lines to Vdd at 3.3V. In the moment, I did not know that I was
connecting two different power rails. Looking back on this, it does not seem like
the best decision. Since the Adafruit data sheet mentioned that the I2C
pins on the breakout could be used at either 3.3V or 5V, I figured that I was safe. 
Fortunately, nothing went wrong. With my pullups in place, I could finally read from
the IMU. One day later, I realized that the hardware kit was set up with pullups to 3.3V.
After realizing this I tried removing the pull ups that I added, and everything still worked.
I have not needed them since.

@image html i2c_resistor_view.jpg

I am still not sure what happened and why clumsily adding resistors fixed my issues.
As I was troubleshooting, I had the clock and data lines connected to a scope.
I did not notice anything out of the ordinary with or without the pull ups I added. If any instructors reading this have ideas as to what happened, I would love to recieve some input.

@image html i2c_scope_view.jpg

@page enc_m_drivers Encoder & Motor Drivers
@author E. Nicholson
@date March 06, 2021

For the first week of our  term project assignment, we checked and updated our
encoder and motor drivers that we used last quarter in ME 305. The only initail functionality 
change was incorporating fault detection into the motor driver.
The physical driver (DRV8847 motor driver) has a fault detection pin that is
pulled low under a number of circumstances (thermal, over current, open load...).
I added interrupt based fault handling to my motor driver. When a falling edge is
detected on the 'nFAULT' an Interrupt Service Routine (ISR) is triggered that disables the
motors in software and sets an internal driver fault attribute to True, latching the fault state. This latch prevents any further action that would supply power to the motors (enable() and set_duty()). Instead these methods route to a fault checking method that tests the value of the nFAULT. If the hardware fault has beeen cleared, the driver is unlatched and operations can continue.

Since we are using the two H-Bridges within the DRV8847 motor driver to control independent motors, they both share nSLEEP and nFAULT pins. Only one ISR can be attached to a pin, so
I updated my motor driver constructor to allow the desegnation of a 'primary' motor.
By setting this designator to True, the user can select which motor object will create
the fault detection ISR and generate the interface prompts to clear the fault. When locked out due to a fault, the promary motor should be used to clear it. In practice, the nFAULT pin
is reset through either some delay time due to hysteresis in the fault triggereing parameter
(temp, current...) or through a power cycle. My driver implements code that cycles power upon
user input until the fault has been cleared. Since both motor objects have control over
the nSLEEP pin, both technically can be used to clear the fault, but only the primary motor
will repond to the fault and provide feedback to the user.

To test this feature, I passed the user button to the driver as if it were the fault pin.
I checked the fault detecting behavior by creating a loop that sets the duty of both motors once every second. I was able to trigger the fault ISR using the button. This disabled both motorsand prompted me to clear the fault.

@image html mot_enc_view.jpg

I also spent time improving the test code included with both drivers. For both, I
added simple terminal UIs which prompt the user to select different types of tests
and which device to test. For both driver I made tests to time methods (timed tests) 
and output (running tests). In the motor driver, I also included a test that runs the
motors through timed forward and backward movements and a test for fault detection. 
This testing code is tailored to the ME 405 hardware, and would need to be updated 
if used in a different setting.

When intergrating these two drivers into the final system, I ran into two issues
that required changes to the motor driver. The first was a 'sticking' of the fault
state in software. I found that when initialized the nFAULT pin momentarily triggers.
When my driver tried to self correct, any attempt to unlatch the fault caused another
edge on the nFAULT pin. To over come this, I disabled the fault ISR when my driver sucessfully latched the Fault state. I then added a 100 us delay after enabling my motors before
turning on the ISR. This stopped the driver fault latch from sticking.

The other issue that I came across was the large dead band present in both of our
motors. I found that for both axes, PWM duty percentages between around -30% - 30%
resulted in little to no motion. This made it impossible to level my platform. 
When using reasonable gains, I was unable to consistently get closer than 5 degrees from level on both axes. I initially tried increasing my system gains, to raise the control signal outside of the motor's dead band at small angles. This overtuning worked, but made the system unstable when farther than 5 degrees off of level. I found an actual solution in a 2006 laberatory report produced by a Grand Valley State University enginereering student. In this report, this student provided a dead band compensation algorithim for bidirectional motor control.

After reading the report and reviewing the student's findings, I used the new test code that I added to my motor driver to sweep positive and negative PWM dutys for both motors.
I found that my X and Y axis motors had dead band of (-28,28) and (-25,25).
I added the motor dead band as an initialization argument for my motor object. This 
allowed me to adjust duty requests sent to the motor using the following equation:

@image html db_comp_orig.png

Here, C_adj is the adjusted output, C_des is the desired output, C_max and C_min are the pos 
and neg the maximum PWM values, and C_db,max and Cdb,min are the pos or neg dead band values. 
Using this alone resulted in high frequency vibrations as the motor bounced between 
positve and negative duty values. To fix this, I widened the range of desired PWM valuse
for which the duty is set to 0. My final dead band compensation algorithm is shown below:
    
@image html db_comp_EWN.png

This motor driver change allowed me to sucessfully balance my platform reliably to
within 1 degree of level. It also allowed me to reduce my controller gains by a
factor of aproximately 50.

The paper that I referenced can be found at the following link:
    
https://www.yumpu.com/en/document/read/17370920/dead-band-compensation-for-bidirectional-motion-claymore-

Two python files was created for this assignemnt. The source code for this files can be found here:
    * enc_driver.py: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me405/me405%20Term%20Project/enc_driver.py
    * m_driver.py: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me405/me405%20Term%20Project/m_driver.py


@page signal_filtering Fun with Filters
@author E. Nicholson
@date March 06, 2021

During the first week of the term project I also spent some time
learning to implement and test digital filtering. I found a few online resources
that discussed the theory and implemetation of digital filtering, and a python 
library that implements finite impulse response (FIR) filtering using a custom inline
assembler. I decided to use the prebuilt FIR tool because it was written using code that
operates much faster than the python that I could have written to create a FIR filter.
Since all of the drivers and control code that will come together to complete
the term project will likely stack up to impose a restriction on how fast the system
can run, I felt that designing for speed wherever possible was important.

I tested out a few different FIR filters using a simple script that streams CSV
data over USB to my PC. This script, in combination with a tool called Serial Oscilloscope,
allowed me to compare raw data to averaging and FIR low pass filtering.
Initially my filter design efforts were challenged by the slow speed at which I could
transmit data over UART 2. It was taking ~5000 us to send each data point, imposing
a nyquist frequency limit on my LP filter around 100 Hz. After speaking with one of
my instructors, I decided that it would be worth switching UARTs and increasing the 
baudrate. I have not had the opportunity to return to filter desing yet, but I plan to
use the toolchain I create for this testing to implement FIR filters for my touch
pannel data and my IMU data. I chose to hold off until I had my controller operating so that
I have a better idea of my sampling frequency limits. 

Once I had my system operating, I saw that the signal coming from the touch pannel was quite poor. It exhibited a large amont of noise, and frequent loss of contact resulting in incosistent position control and large incorrect velocity spikes. To correct this, I began working on a signal processing method to adress both of these problems. 

I started by adding batch averaging to all of my measurements. This helped to cut down on the 
highest frequency loss of contact events. I configured my touch pannel driver to take 1, 5 or 10 samples based on a parameter passed to the scan() method. By doing this interally to the driver, I could eliminate unnecessary pin configuration changes that could have added a few miliseconds to run time. Batch averaging was not quite enough, so I also designed a low pass digital filter.

I measured the average run time of control cycle to be aproximately 5000 us (200 Hz). I used this as my design frequency. I implemented the filter using the library mentioned above. To come up with filter coefficents I used the online filter designer TFilter.com (http://t-filter.engineerjs.com/). I tried to keep the pass band very tight since the loss of contacct event I was trying to attenuate could occure at fairly low frequencies 15-20 Hz. To reduce the number of coefficents required, I allowed for a fairly shallow cutoff. The following image shows the gain vs frequency plot of my filter along with its parameters.

@image html final_filt.png

For now, I hard coded the filter coeffiecnts and scaling parameters into my touch pannel driver. If there are any issues with the magnitude of the touch pannel signal it is likely due to an issue with the scaling parameters being set for my hardware. If a new filter is deployed, new scaling values will be needed.

The filter greatly improved the quality of my signal, but it did not fully remove the effect of high frequency loss-of-contact events. For this, I added a running average based position rejection system. Essentially, I track the running average of the last 5 calls to scan(). If the most recent position values fall outside a threshold I set, the value is replaced with the average. To prevent real loss of contact events from being rejected, I update the average with the bad value. This is bad, and it directly causes some of the signal quality issues that persist in my final product. Unfortunately, I did not have the time to rework and test my rejection method  for the final submittal. I believe that with a little more work this method could be improved significatly. This would fix some of the stability problems I have in my controller. It would also eliminate the need for some of the blunt workarounds I implemented in my control loop to avoid weird signal artifacts that my processing scheme created.

The following plots displays the effect of my touch pannel signal processing on raw ADC counts. To generate this response, I slid my finger along the touch pannel so that it vibrated, causing rapid loss of contact. The scope channels contain the following information:

On the top:
    
     Green: Raw x-axis data
    Purple: Raw z-axis data
    Yellow: Batch sampled (n=10), FIR filtered, and mean rejected z-axis data

On the bottom:
    
     Green: Batch sampled x-axis data  (n=10)
    Purple: Batch sampled (n=10) and mean rejected x-axis data
    Yellow: Batch sampled (n=10), mean rejected, and FIR filtered x-axis data

@image html filt_demo2.png

This picture highlights the fact that I am far from a qualified filter designer. Adding all of this complexity improved some aspects of my touch pannel signal while making others worse. I was able to attenuate some of the large position changes that resulted from loss of contact events (both repeated and isolated). This improvement came at the price of a clear lag in the filtered signal, and slow settling after repeated loss of contact events. With more time I am confident that I would be able to greatly improve the performance of my filter. I would start with a more robust outlier rejection method. Tuning the rejection crieteria and changing the running average would dramatically improve the quality of the data being fed to my filter. This would allow me to use a filter with far less lag.

While the final results of my plunge into filtering and signal process were lack luster, the knowledge and experience I gaines was the oposite. After this term project, I feel far more comfortable tackling a problem like this in the future. I have a much clearer view of where issues could exist, and I know how to deploy open source tools to handle desing and implementaion of digital filtering.

The following are links to the python test script that I created, and the github 
repo containing the FIR and averaging libraries:
    * rtp_filter_testing.py: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me405/me405%20Term%20Project/rtp_filter_testing.py
    * FIR & Averaging Library: https://github.com/peterhinch/micropython-filters
 

@page term_proj_model_testing System Model Testing
@author E. Nicholson
@date February 22, 2021

Before we could start working with our hardware we needed to be sure that our
system model was correct, and that we could control it with closed loop full state
feedback. Using the system model we obtained previously, we continued our
analysis by transforming our system to state space form and by applying a 
jacobian linearization. This resulted in the two coefficent matrices (A and B) 
for our state space model. I chose to complete this portion of the lab in MATLAB.
My analysis can be seen in the \ref term_proj_modeling_MATLAB section.

Once I had created my coefficent matrices, I moved to python to simulate the system.
I used python's control toolbox and numpy since together they provide functionality 
similar to that of MATLAB. I thought that it would be a good opportunity to test out
how well python can be used for controls.

I wrote a simulation script that read the matrices MATLAB wrote to two text
files. These two were then used to run four open loop test to confirm that the
system behaved as expected. Each of the four open loop test cases displayed the
behavior that I anticipated. The following three figures show the results of these 
test.

(Case A) Ball at rest on a level platform directly above platform CG. 
No torque is applied by the motor. The scenario is simulated for 1 sec.
@image html lab0x06_OL_A.png

This first test was used to prove that the OL system remained at rest when initialized
at a zero state. The simulation showed that all four state variables stayed at zero.

(Case B) Ball at rest on a level platform offset horizontally from the platform's
center by 5 cm. No torque is applied by the motor.The scenario is 
simulated for 0.4 sec.
@image html lab0x06_OL_B.png

The second test was designed to check the system's response to an initial ball 
displacement. Under the conditions listed above, the system showed the platform 
tilting towards the ball offset, and the ball continuing roll farther away from 
the platform center. Within 0.4 s the ball had rolled 3 cm, and the platform 
had tilted by 0.6 rad (~34 deg).

(Case C) Ball at rest on a platform inclined at 5 deg directly above the platform's
center of gravity. No torque is applied. Scenario is simulated for 0.4 sec.
@image html lab0x06_OL_C.png

The third test focused on checking the system's response to an initial platform
tilt. Under the conditions listed above, the system showed the platform 
continuing to tip in the direction of the offset, and the ball rolling 
farther away from the platform center down the increasing slope.  Within 0.4 s 
the ball had rolled 8 cm, and the platform had tilted by an additional 0.7 rad 
(~40 deg).

(Case D) Ball at rest on a level platform directly above platform CG. A torque
of 1 [m Nm s]is applied by the motor. The scenario is simulated for 0.04 sec.
@image html lab0x06_OL_D.png

The final OL test checked the response to an initial torque impulse applied at
the motor. This test confirmed that the correct sign relationship between motor
torque and platform acceleration was implemented. A positive motor torque causes a
negative change in platform angle. I applied a pseudo impulse to the platform
using a very short step signal with a magnitude of 1 mNm s. The simulation showed 
that this brief purturbation was sufficent to move the system out of equilibrium.
The platform angle responds the fastest, initially decreasing slowly, but speeding
up as the platform mass carries the system further out of balance. The ball responds
correctly to the platform angle, rolling down the slope. The Ball's speed initially 
bends very slightly positive since the inertia of the ball tries to keep it in its
initial position. Quickly the velocity moves negative as the ball is accelerated 
down the slope.

Next I used the given feedback gains to create a closed loop system. I then applied
Cases A-C again to observe the difference in behavior. I did not apply case D because
our closed loop system is a regulator. The motor torque is nnow used to force
the system back to equilibrium.

(Case A) Ball at rest on a level platform directly above platform CG. 
No torque is applied by the motor. The scenario is simulated for 1 sec.
@image html lab0x06_CL_A.png

This test just confirmed that the system still remained steady at equilibrium
if ran with zero initial conditions.

(Case B) Ball at rest on a level platform offset horizontally from the platform's
center by 5 cm. No torque is applied by the motor.The scenario is simulated for 20 sec.
@image html lab0x06_CL_B.png

This test checked the closed loop system's response to an initially off center ball.
By closing the loop, we were able to create a system that can self regulate back
to equilibrium. The controller gains used were provided in the lab handout, and 
are not yet optimized. This just proved that the system could be stabalized. The system
responded as expected to the displacement, decreasing platform angle to correct for the
positive ball displacement. As the ball rolled back the center, the platform angle
adjusted to slowly bring the ball to rest.

(Case C) Ball at rest on a platform inclined at 5 deg directly above the platform's
center of gravity. No torque is applied. Scenario is simulated for 20 sec.
@image html lab0x06_CL_C.png

This final test showed that the system could also regulate an initial purturbation
in platform angle. The same feedback gains were used here. Once again the response
met expectations. The ball initially accelerated down the platform slope. Straight away,
the system corrected the platform displacement, and began counteracting the ball's motion.

One python file was created for this assignemnt. The source code for this files can be found here:
    * system_modeling.py: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me405/me405%20Term%20Project/system_modeling.py


@page rtp_driver_testing Resistive Touch Pannel Driver
@author E. Nicholson
@date March 01, 2021

We are using a resistive touch pannel (RTP) to sense the position of the ball that
we are balancing. For Lab 0x07, we were asked to create and test a driver for generic RTPs that
enables fast contact and position measurements. Before I began writing the driver,
I soldered the RTP cable breakout board to the top pannel of my hardware kit. I then
adhered the RTP to the top pannel, and connected it to the breakout. With everything connencted,
I used the RTP data sheet and the lab manual pin mappings to determine where each RTP
wire was connected. Next I checked the functionality of each axis using the REPL.
When everything worked properly, I began writing the driver. The following picture
shows my RTP connected. I removed the top platform to allow for easier testing.

@image html lab0x07_rtp_setup.jpg

The driver takes four arbitrary pins along with two tuples describing the x and y
values of the RTP active area and center point. I implemented separate methods for 
checking x, y, and z on the pannel. I also created a methods that scan all three axes. 
Originally it used the individual scan methods, and implemented some additional features 
like returning raw counts or absolute position (with respect to pannel zero).

Once the driver was functional, I began testing it. I wrote my test code in the driver file
so that it runs when the driver is used as '__main__'. My test allows for two types
of testing, running and timed. The running test simply outputs RTP measuremnets every
250 ms. The timed test scans the RTP once and computes the time in micro seconds
that it took to read. Subsequent tests are triggered by the user pressing enter. Both
types of tests can be run one x, y, or z individually, or on the full RTP scan.

I first used the running test mode to check the values that the driver was returning.
I saw that at all four corners, the mm measurements for x and y that I was recieving
were slightly lower than expected. Initially, I used the following equation to 
obtain my center refferenced position measurements:

@image html lab0x07_eqn1.png

In the equation above, Cmax is ((2**12)-1) or 4095. To correct this, I changed 
the way I converted counts to mm to include calibration values. The values I used 
were the min and max ADC counts for each axis. My new equation was:

@image html lab0x07_eqn2.png

Here, Cmin and Cmax are now measured values. I obtained them by using a running test,
and slowly appraching the edges of the RTP active area. Since these values could fluctuate
a decent about (~20-30 counts) I chose to use min and max values that fell in the
middle of the spread. This new count conversion method worked well, and allowed my
driver to output measurments that match the RTP active dimensions. These dimensions,
referenced to an origin at the center of the pannel are as shown in the following image.

@image html lab0x07_rtp_dims.png

Next I focused on checking the timing of my scan methods. As originally implemented,
I was obtaining scan times of ~1300 us. This likely was the result of optimizing
the read order bsed on the previously read axis. I used conditional statements and
a class atribute to track which axis was last read and to only reconfigure the pins required
for the next scan. I still felt like this was a too slow, so I began eliminating calls to
pyb.Pin.init that I suspected were not necessary. By removing this unnecessary configuration
I was able to reduce my scan time to ~970 us. To further reduce this, I changed my
three axis scan method so that it read all three axes without using any other methods.
Using this method I was able to shave about 180 us off for a final scan
time of ~790 us. With the updated requirement of 1500 us max, this was sufficent.

To demonstrate the performance that I have described in the previous paragraphs,
I created a short video that walks through the performance that I have described in this
writeup.

The direct link:
    
https://youtu.be/Cp7zBzlnz1A

One item that I chose not to implement was a 3-5 us delay after pin configuration.
After reading some class discussion on piazza, I confirmed that configuring an 
ADC takes aproximately 60 us. By placing the ADC setup after my pin config,
I minimized the concerns about pin settling time outline in the lab handout. To be safe, I confirmed that
my position reading did not noticably change after I removed the short delays.

When I originally wrote the touch pannel driver, I chose not to include any type of filtering.
After experimenting with both FIR filtering and averaging, I found that one of the most
significant variables in effective filter design is sampling rate. Since I did not know
how fast I would be able to run my controller, I felt that it was best to wait to
implement filtering last. During the final intergration phase of the project, I found
that both averaging and filtering were important for sucessfully controlling the system.
The filtering specific section of my term project (\ref signal_filtering) contains a complete discussion of
my filter design process.

Since I knew that running speed speed was going to be important for effective system 
control, I decided to try to further decrease the run time of this driver. I found a
very useful resource produced by the creator of Micropython that touched on simple 
speed optimizations for micropython code. Following the creators advice, I made my imports
more specific ('from pyb import Pin' instead of 'import pyb' etc) this droped my run time 
by ~150 us. By pre allocating memory in a few spots, and by using micropython's native
inline assember for my methods, I was able to cut my run time all the way down to
540 us for a full three axis scan. This gave me plenty of headroom to add averaging and
filtering calculations to my driver for the final integrating.

The resource that I used can be found at the following link:
    
https://www.youtube.com/watch?v=hHec4qL00x0

One python file was created for this assignemnt. The source code for this files can be found here:
    * rtp_driver.py: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me405/me405%20Term%20Project/rtp_driver.py


@page final_int Final Intergration
@author E. Nicholson
@date March 16, 2021

The final two weeks of the quarter were spent intergrating all of the components
of our term project into a functioning system. We used the model we developed 
and tested in Labs 0x06 and 0x07 to design a full state feedback controller 
for the system. To sense our system state and provide actuation, we interfaced
with the motor, encoder, and touch pannel drivers that we had built previously.
I also chose to interface with the BNO055 breakout board to provide an absolute
orientation for my platform.

As the term project fit together, I came across a number of performance issues 
that required me to revisit the modeling calculations and hardware drivers. All of the
term project pages and software documentation previously produced have been updated 
with a discussion of the changes that I implemented for the final integration.
Please revist these sections to view the updates. This section will
focus on how all of these changes came together and the final status of the project.

I began by revisiting the system model testing script that I wrote for Lab 0x07 and
repurposed it as a controller design tool. Using the python control systems tool box, I used the Linear Quadratic Regulator (LQR) method to generate initail controller
gains for both a reduced system containing only the platform and the full system.
The script, system_ctrl_design.py, walks through my design process, and generates
comparison plots to show the results of tuning. My partner Kyle Chuang completed
a controller design hand analysis using pole placement and coefficent matching. His
results confirmed the validity of both of our software based design techniques.

I followed the same tuning process for both the reduced and the full system. I started checking the system response to an initail 5 mm ball displacement with all my LQR weights set to 1. From there I began adjusting weights, starting with the R matrix (actualt effort weight). I reduced this to bring up the gains on my state variables. From there I increased the weights on the lower order terms and reduced the weights on the higher order terms. After the balancing software was functional, I used it to test analytically determined gain sets. The system response informed my weighting scheme. My analytical tuned an untuned gains for the platform system were:
    
    Kplat_untuned = [-1.09643,	-0.996143]
      Kplat_tuned = [-10.0926,	-1.41904]

The following plot shows a comparison between the tuned and untuned reduced system model. 

@image html plat_tune_comp.png

The tuning process for the full system was identical to that used on the reduced system. My analytical tuned an untuned gains for the whole system were:    

    Ksys_untuned = [-1.17333,	-4.90108,	-1.54464,	-1.02376]
      Ksys_tuned = [-10.1618,	-10.1002,	-5.43733,	-1.08662]
      
The following plots show a comparisons between the tuned and untuned reduced system model I broke the behavior of the platform and the ball into separate plots for clarity. 

@image html sys_ball_tune_comp.png
@image html sys_plat_tune_comp.png

Once I had a controller gain set in which I felt confident, I started integrating 
all of the components of the term project into a main script. Before writing
any code, I drew out how all of the peices fit together. The following image is 
the final version of my system diagram with important elements broken out:
    
@image html TermProj_SystemDiag.png

This diagram provided a very useful outline for creating my main balancing script.
First I wrote a class for my full state feedback controller using the PID
controller I created for ME 305 as a skeleton. Once this was working
I brought in all of the relevant setup code for my other drivers and created a 
time controlled loop that read sensor data, passed the resulting the control 
signals to the motors. At a high level, my script operates in two modes. The first
occurs when there is no contact detected on the touch pannel. In this state, the system
is controlled using the gains derrived from the platform only model, and the orientation
measurements from the BNO055 IMU. This allows for repeatable and automatic absolute leveling. When in this mode, the time that the platform is stable (no omega) is tracked. If there has been no motion for 50 controller cycles the encoders are zeroed.

The second mode begins when contact is detected on the touch pannel. The controller
waits for 100 cycles with positive contact before changing the control system to
incorperate the ball position and velosity. This was done to allow the ball position
signal to stabalize before using it for control. The FIR filter and averaging scheme
that I applied to reduce noise and accidental loss of contact also acts on the 
real contact changes. The easiest way to handle this was to add an artificial settling
time before changing control state. In this mode, platform orientation data is
read from the encoders. This allows for much faster controller speeds, and as a result, better performance.

Before attempting to balance the ball, I chose to focus on leveling the platform.
I started with the tuned analytical controller that I had developed. for the reduced system. Initially,I was unsucessful due to the large deadband in the ME 405 kit motors. 
As I discussed in the motor driver section (\ref enc_m_drivers) I solved this 
issue by adding dead band compensation to my motor driver. This change enabled 
my platform to level itself from any orientation to within 1 degree of level. 
I set 1 degree as the threshold because, outside of that, I could not reliably get 
the ball to stay on the platform. I found that my tuned gains for theta were far more adequate, with a slight increase, once the dead band compensator was active. The gain on omega, however, was far to high. In order to get a stable response, I had to reduce the gain on omega by a factor of ~100. I also increased slightly increade the the 

My final manually tuned gains for the platform system were:
    
    Kplaty = [-14.4129,	-0.00223331]
    Kplatx = [-10.4129,	-0.00223331]

I tuned each axis sepatately to address the different levels of friction in the two
drive assembies. The systems still has a tendancy to oscillate more than I would like, but reducing the gains led to a far less consistent final stable position. The following plots shows the system's response to a disturbance (me poking the -x,-y corner of the platofrm). 

@image html plat_lev_disturb.png

The left scope window shows the control signals, while the right shows platform angle
as read by the BNO055 IMU. X data is shown in green, and y data is shown in purple. In this test I disturbed the plafor four times. The first time by pressing down on the (-x,-y) corner. The second time, by pressing up on the (-x,-y) corner. The final two disturbances were produced by pressing down on -x and -y axes individually.

Once the platform could reliably balance itself, I turned to incorperating the dynamics. At this point, I created the second operating mode discussed above. The first thing that I noticed was the poor quality of the signal coming from the touch pannel. The signal itself was noisy, but worse was the lask of consistent contact. I found that, especially near the edges of the platform, I would get contact loss that translated into percieved jumps in the ball's position. This lead to high frequency vibrations as the controll signal was pushed and pulld by the poor positional measurements. My toruble shooting process and eventualy solution is fully discussed in my signal filtering section (\ref signal_filtering). I was unable to fully clean up the touch pannel signal in the time I had available to finish this project. However, I was able to vastly imporve it. The lingering issues with the touch pannel data is a significant hurlde that is preventing form consistently balancing the ball on the platform.

As I was manually tuning the full system, I found that the gain on the ball's velocity was far too high (similar to the gain on omega). The poor position signaly quality on the was only amplified when I calculated velosity. This forced me to drom its gain by a factor of about 6 to achieve a stable response.

My final manually tuned gains for the whole system were:    

    Ksysy = [-14.17333,	-14.4129,	-1.254464,	-0.00223331]
    Ksysx = [-14.17333,	-10.4129,	-1.254464,	-0.00223331]

The final performance of the balancing system was not what I hoped it would be, but it is far better than where I started. I still get high frequency jittering at the edges of the touch pannel that lead to a loss of stability. For very light disturbances of the ball, or of a larger object (a lime was used), I can stop the motion of the object, but I usually fail to bring it back to the center of the platform. This image shows the system response to balancing a lime on the platform. The performance depicted is not the highest possible since the controller cannot run at max speed while visualizing data, but it is adequate for demonstration purposes.

@image html sys_lime_bal.png

From left to right, the scope windows depict control signal, platform angle, and touch pannel position. The purple traces corespond to y axis values while the green traces corespond to x axis values. The y axis control signal (purlpe) responds to the y axis platform tilt (purple), and the x axis position (green). The x axis control signal (green) responds to the x axis platform tilt (green), and the y axis position (purple). 

The following link leads to a video demonstration of my hardware with comenatry about the system:
    
Direct Video Link: https://youtu.be/ZhBD2UM5fwg

While I was unable to achive high performace with my system, the process of designing, troubleshooting, and tuning the system taught me a lot. This term project has been a great opportunity for me to deepen my understanding of mechatronics and to practice using my skills.

Three additional python files was created for the final part of this assignemnt. The source code for these files can be found here:
    * system_ctrl_design.py: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me405/me405%20Term%20Project/system_ctrl_design.py
    * balance.py: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me405/me405%20Term%20Project/balance.py
    * fsfb_control.py: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me405/me405%20Term%20Project/fsfb_control.py


@page term_proj_modeling System Modeling
@author E. Nicholson
@date February 22, 2021

Note: This page contains scans of my corrected hand calculations. A MATLAB live
      script was created alongside the hand analysis. My updated hand calculations
      rely more heavily on the MATLAB script for detail due to the complexity 
      that was introduced when the calculations were corrected.
      This script is where the equations were implemented and where my hand 
      calculations were verified / completed. It is configured to display only 
      the key symbolic results when run.
      
Note: The HTML output of my MATLAB live script can be viewed here: \ref term_proj_modeling_MATLAB  
      The matlab file can be found here:
          https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me405/me405%20Term%20Project/ME405_BalancingPlat_SYS_Model.mlx      

Note: My original hand calculations are still available here: \ref term_proj_modelingR1
      
@image html lab0x06_pg1.jpg
@image html lab0x06_pg2.jpg
@image html lab0x06_pg3.jpg
@image html lab0x06_pg4.jpg
@image html lab0x06_pg5.jpg
@image html lab0x06_pg6.jpg
@image html lab0x06_pg7.jpg
@image html lab0x06_pg8.jpg
@image html lab0x06_pg9.jpg
@image html lab0x06_pg10.jpg
@image html lab0x06_pg11.jpg
@image html lab0x06_pg12.jpg
@image html lab0x06_pg13.jpg

@page term_proj_modeling_MATLAB MATLAB System Modeling
@author E. Nicholson
@date February 22, 2021

\htmlinclude ME405_BalancingPlat_SYS_Model.html

@page term_proj_modelingR1 System Modeling (First Attempt)
@author E. Nicholson
@date February 15, 2021

Note: This page contains scans of R1 of my hand calculations. This is the work
      that I completed and turned in for Lab 0x05. Many parts of this analysis 
      are incorect. My Hand calculations have been updated and corrected for 
      Lab 0x06. This material is included to show how my model has changed.
      
@image html lab0x05_pg1_R1.jpg
@image html lab0x05_pg2_R1.jpg
@image html lab0x05_pg3_R1.jpg
@image html lab0x05_pg4_R1.jpg
@image html lab0x05_pg5_R1.jpg
@image html lab0x05_pg6_R1.jpg
@image html lab0x05_pg7_R1.jpg
@image html lab0x05_pg8_R1.jpg
@image html lab0x05_pg9_R1.jpg
@image html lab0x05_pg10_R1.jpg
@image html lab0x05_pg11_R1.jpg


'''

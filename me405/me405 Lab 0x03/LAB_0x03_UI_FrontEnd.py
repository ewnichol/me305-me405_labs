'''
@file LAB_0x03_UI_FrontEnd.py

@author E. Nicholson

@date January 30, 2021

@brief      This file holds the Lab 0x03 front end UI Finite State Machine class and the code to run it
            
@details    This file was repurposed from my ME305 code for Labs 6 & 7. It
            contains a Finite State Machine that operates on a PC and
            and communicates with code running on a Nucleo. This file also
            has the code that facilitates the operation of this FSM.
            
            The source code for this file can be found here: 
            
            https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me405/me405%20Lab%200x03/LAB_0x03_UI_FrontEnd.py
'''

import serial
import time
import matplotlib.pyplot as plt
import numpy as np

class FrontEnd_UIFSM:
    '''
    @brief      A front end UI that recieves user input and sends commands to the Nucleo
    @details    This FSM handles user input processing, and sends screened commands to
                the Nucleo. It also recieves data transmissions, and plots 
                test results. This FSM was repurposed from the front end UI
                from ME305's Labs 6&7.
    
    @image html lab0x03_FE_FSM.png
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT             = 0    
    
    ## Constant defining State 1 - Setup
    S1_SETUP            = 1
    
    ## Constant defining State 2 - Wait
    S2_WAIT             = 2
    
    ## Constant defining State 3 - Recieve Transmit
    S3_RECIEVE_TRANSMIT = 3
    
    ## Constant defining State 4 - Process
    S4_PROCESS          = 4
    
    ## Constant defining State 5 - Exit
    S5_EXIT             = 5

    def __init__(self, ID):
        '''
        @brief              Creates a UI object
        @param ID           A string identifying the object for tracing output.
        '''
        
        # ---- General Setup ----
        
        print('UI Task Created')
        self.com_open()
        
        ##  The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ##  The unique ID string for the object, used for trace output
        self.ID = ID
        
    def run(self):
        '''
        @brief      Runs one iteration of the Front End UI task
        '''
        
        if(self.state == self.S0_INIT):
            # Run State 0 Code

            self.transitionTo(self.S1_SETUP)

        elif(self.state == self.S1_SETUP):
            # Run State 1 Code         
            
            # Sends the start message to the Nucleo
            print('Enter "G" to enable data collection: ', end = '')
            start_msg = self.get_input('str')
            self.send(str(start_msg).encode('ascii'))
            
            self.transitionTo(self.S2_WAIT)

        elif(self.state == self.S2_WAIT):
            # Run State 2 Code         
            
            msg = self.recieve()
            
            # Checks for transmissions and prints status messages
            if msg == 'TRANSMIT':
                self.clear_data()
                self.transitionTo(self.S3_RECIEVE_TRANSMIT)
            elif msg != '':
                print(msg)
            else:
                pass
            
            if msg == 'Invalid Command':
                self.transitionTo(self.S1_SETUP)

        elif(self.state == self.S3_RECIEVE_TRANSMIT):
            # Run State 4 Code

            msg = self.recieve()
            
            # Checks for end of transmission and frequency
            if msg == 'END':
                # print(self.recieve())
                self.transitionTo(self.S4_PROCESS) 
            elif msg[0:4] == 'FREQ':
                ##  Data collection frequency in kHz
                self.data_freq = int(msg[4:])
            else:
                data_point = msg.strip().split(',')
                self.tim = np.append(self.tim, float(data_point[0]))
                self.signal = np.append(self.signal, float(data_point[1]))
                
        elif(self.state == self.S4_PROCESS):
            # Run State 5 Code                 
            
            print('Processing Data')
            
            # The entire data transmittal is written to a csv file
            
            with open('data.csv','w') as f:
                for i in range(len(self.tim)):
                    f.write('{:f},{:f}\n'.format(self.tim[i],self.signal[i]))
                print('CSV Created')
            
            # The following code identifies where the step occurs, slices 
            #   out the portion of interest, and relabels/scales the timestamps
            step_time_index = np.where(self.signal > 100)[0][0]
            
            signal_slice = self.signal[step_time_index-20:step_time_index+75]
            tim_slice = (1000/self.data_freq)*np.array(range(len(signal_slice)),dtype=np.float32)
            
            
            # This plots the region of the recording with the step response
            fig1 = plt.figure()
            plt.plot
       
            fig1.suptitle('Response Test: User Button')
            plt.plot(tim_slice,signal_slice, 'k.')
            plt.ylabel('ADC Signal [counts]')
            plt.legend(['Measured Signal'])
            plt.xlabel('Time [milliseconds]')
            plt.show(False)
            
            # This portion check to see if the user wants to continue
            
            if input('End Task? y/n: ') == 'y':
                self.com_close()
                self.transitionTo(self.S5_EXIT)
                print('You may now close the program')
                
            else:
                self.transitionTo(self.S1_SETUP)
                
        elif(self.state == self.S5_EXIT):
            # Run State 4 Code                 
            pass
        else:
            # Invalid state code (error handling)
            pass
    
    def send(self, msg):
        '''
        @brief Sends a message to the Nucleo and waits to avoid message stacking
        @param msg Message to be sent
        '''
        self.ser.write(msg)
        time.sleep(.001)
        
    def recieve(self):
        '''
        @brief  Reads a message from the Nucleo and removes special characters
        @return proc_msg The message with special characters removed
        '''
        msg = self.ser.readline().decode('ascii')
        proc_msg = msg.strip()
        return proc_msg
    
    def com_open(self):
        '''
        @brief  Opens the serial communication line
        '''
        ## Serieal communication line
        self.ser = serial.Serial(port='COM3',baudrate=115373,timeout=1)
        print('Serial Line Opened')
        
    def com_close(self):
        '''
        @brief  Closes the serial communication line
        '''
        self.ser.close()
        print('Serial Line Closed')

    def clear_data(self):
        '''
        @brief      Resets data recording paramaters
        @details    This function emptys all data arrays/lists and resets the
                    data counter.
        '''
        ## Array for storing encoder data
        self.tim = np.array([], dtype=np.float32)
        
        ## Array for storing encoder data
        self.step = np.array([], dtype=np.float32)
        
        ## Array for storing measured motor speed
        self.signal = np.array([], dtype=np.float32)
        
        ## Current number of data points recorded
        self.curr_data = 0        

    def get_input(self, typ):
        '''
        @brief Obtains user input, and screens for correct type. 
        @details    This method obtains user input and check for a valid type.
                    The input request is repeated until a valid input is entered. 
        @param typ          A string defining the desired input type [int,pos_int,neg_int,float,pos_float,neg_float,setup,str]
        @return user_in     The screened user input
        '''
        while True:
            if typ == 'int':
                user_in = input('   Enter an interger: ')
                if self.check_blank(user_in):
                    break
                try:
                    user_in = int(user_in)
                    break
                except:
                    print('     Invalid')
            elif typ == 'pos_int':
                user_in = input('   Enter a positive interger: ')
                if self.check_blank(user_in):
                    break
                try:
                    user_in = int(user_in)
                    if user_in >= 0:
                        break
                    else:
                        print('     Your input was not positive')
                except:
                    print('     Invalid')
                    
            elif typ == 'neg_int':
                user_in = input('   Enter a negative interger: ')
                if self.check_blank(user_in):
                    break
                try:
                    user_in = int(user_in)
                    if user_in <= 0:
                        break
                    else:
                        print('     Your input was not negative')
                except:
                    print('     Invalid')
            elif typ == 'float':
                user_in = input('   Enter a number: ')
                if self.check_blank(user_in):
                    break
                try:
                    user_in = float(user_in)
                    break
                except:
                    print('     Invalid')
            elif typ == 'pos_float':
                user_in = input('   Enter a positive number: ')
                if self.check_blank(user_in):
                    break
                try:
                    user_in = float(user_in)
                    if user_in >= 0:
                        break
                    else:
                        print('     Your input was not positive')
                except:
                    print('     Invalid')
            elif typ == 'neg_float':
                user_in = input('   Enter a negative number: ')
                if self.check_blank(user_in):
                    break
                try:
                    user_in = float(user_in)
                    if user_in <= 0:
                        break
                    else:
                        print('     Your input was not negative')
                except:
                    print('Invalid')
            elif typ == 'setup':
                user_in = input('--> ')
                if self.check_blank(user_in):
                    break
                if user_in in ['step','signal']:
                    break
                else:
                    print('     That is not valid')
            elif typ == 'str':
                user_in = input('   -->  ')
                if self.check_blank(user_in):
                    break
                break
            else:
                user_in = input('   Enter something: ')
                if self.check_blank(user_in):
                    break
                break
        
        if user_in == '':
            user_in = 'default'
            
        return user_in
    
    def check_blank(self, user_in):
        '''
        @brief Checks if the string passed is empty, ''
        @param user_in The string to be checked
        @return empty Boolean, True if empty, False if not
        '''
        if user_in == '':
            print('     Skipped...')
            empty = True
        else:
            empty = False
            
        return empty
           
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState

if __name__ == '__main__':    
    '''
    This block of code allows this file to run the UI FSM stand alone
    '''
    
    ## A front end UI task object
    task1 = FrontEnd_UIFSM('FSM1')
    
    try:
        while True:
            task1.run()
    except:
        pass
    finally:
        task1.ser.close()

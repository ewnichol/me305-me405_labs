'''
@file LAB_0x03_UI_BackEnd.py

@author E. Nicholson

@date January 30, 2021

@brief      This file holds the Lab 0x03 back end UI Finite State Machine class.

@details    This file holds the back end UI FSM for Lab 0x03 of ME 405 and the
            code to facilitate its operation.

            The source code for this file can be found here: 
    
            https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me405/me405%20Lab%200x03/LAB_0x03_UI_BackEnd.py
'''
import pyb 
import array
import micropython
 
class BackEnd_FSM:
    '''
    @brief      A finite state machine that handles the back end of the Lab 0x03 UI
    @details    This FSM recieves commands from the front end UI, and records 
                data from an analog to digital converter. This FSM also handles 
                the transmission of data back to the front end upon the 
                completion of a test. This FSM was repurposed from the back end UI
                from ME305's Labs 6&7.
    
    @image html lab0x03_BE_FSM.png
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT             = 0    
    
    ## Constant defining State 1 - Wait
    S1_WAIT             = 1
    
    ## Constant defining State 2 - Listen
    S2_LISTEN           = 2
    
    ## Constant defining State 3 - Transmit
    S3_TRANSMIT         = 3

    def __init__(self, ID, UART_ID, user_button, ADC1, buf1, adc_tim):
        '''
        @brief  Creates a BackEnd_UI object
        @param ID                   A string identifying the object for tracing output.
        @param UART_ID              The UART ID number that will be used for serial communication
        @param user_button          A Pin object connected to Pin C13 (the user button)
        @param ADC1                 An analog to digital converter object
        @param buf1                 A buffer array for storing ADC1 data
        @param adc_tim              A timer object set to triger at the desired ADC sampling frequency
        '''
        
        # ---- General Setup ----
        
        ##  The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ##  The unique ID string for the object, used for trace output
        self.ID = ID
        
        ##  UART object for handling simple serial communication
        self.uart = pyb.UART(UART_ID)
        
        ##  A Pin object connected to Pin C13 (the user button)
        self.user_button = user_button
        
        ##  A boolean indicating if the user button has been pressed
        self.PRESS = 0
        
        # ---- ADC Configuration ----
        
        ##  The analog to digital converter object reading the user button
        self.ADC1 = ADC1
        
        ##  A buffer array for storing ADC data
        self.buf1 = buf1
        
        ##  A timer object set to triger at the desired ADC sampling frequency
        self.adc_tim = adc_tim
        
    def run(self):
        '''
        @brief      Runs one iteration of the Back End UI task
        '''

        if(self.state == self.S0_INIT):
            # Run State 0 Code
            
            # Preallocation of memory incase the callback function throws an exception. 
            #   This allows the user to view the exception.
            micropython.alloc_emergency_exception_buf(100)
            
            ## The callback for the user button. Activation occurs on a falling edge.
            self.extint = pyb.ExtInt(self.user_button,pyb.ExtInt.IRQ_RISING_FALLING,pyb.Pin.PULL_NONE,self.user_button_pressed)
            
            self.transitionTo(self.S1_WAIT)
            
        elif(self.state == self.S1_WAIT):
            # Run State 1 Code   
            
            cmd = self.get_cmd()
            
            # Scans for commands, if something unexpected is recieved, an 
            # invalid message is returned.
            if cmd == None:
                pass
            elif cmd[0:1] == b'G':                
                self.PRESS = 0
                print('Waiting for button press...')
                self.transitionTo(self.S2_LISTEN)
            else:
                print('Invalid Command')
        
        elif(self.state == self.S2_LISTEN):
            # Run State 2 Code   
            
            # When a press occurs, the ADC is read
            # print('listen')
            if self.PRESS == 1:
                self.ADC1.read_timed(self.buf1, self.adc_tim)
                
                self.reset_data()
                
                ## A boolean indicating if an edge was seen in the recorded data
                self.edge = False
                
                for i in range(len(self.buf1)):
                    if self.buf1[i] > 4000:
                        self.edge = True
                        break
                    else:
                        pass
                
                if self.edge == True:
                    ## A transmission is started, and the frequency is sent
                    print('TRANSMIT')
                    print('FREQ'+str(self.adc_tim.freq()))
                    self.transitionTo(self.S3_TRANSMIT)
                else:
                    print('Step response not caught. Try again...')
                    print('Waiting for button press...')
                    
        elif(self.state == self.S3_TRANSMIT):
            # Run State 2 Code     
            
            # One data point is sent every FSM cycle
            print('{:f},{:f}'.format((1000/self.adc_tim.freq())*self.data_sent,
                                  self.buf1[self.data_sent]))     
                  
            self.data_sent += 1
           
            # The transmission is terminated when allthe data is sent
            if self.data_sent >= self.tot_data:
                print('END')
                self.transitionTo(self.S1_WAIT)
                self.PRESS = 0
            
        else:
            # Invalid state code (error handling)
            pass
            
    def get_cmd(self):
        '''
        @brief Reads the serial line and assigns a command
        @return cmd     A processed command  
        '''
        
        if self.uart.any() != 0:
            cmd = self.uart.read()
            # print(cmd)
        else:
            cmd = None
        return cmd
    
    def reset_data(self):
        '''
        @brief      Resets data transmitting parameters
        '''
        
        ## Running cound of the number of data points transmitted
        self.data_sent = 0
        
        ## Total number of points for transmittal
        self.tot_data = len(self.buf1)

    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
    
    def user_button_pressed(self, IRQ_source):
        ''' 
        @brief Callback which runs when the user presses the blue input button on the Nucleo
        @details This function is triggered when the Nucleo's onboard button is pressed.
                 It records the reaction timer count, indexes the number of presses,
                 and check to see if the button was pressed early.
        @param IRQ_source The source of the interrupt. This must be taken as an arg, but is not used
        '''
        self.PRESS = -1*self.PRESS + 1
        

if __name__ == '__main__':    
    '''
    This block of code allows this file to run the UI FSM stand alone
    '''
    
    ## A pin object bound to the user button (Pin PC13)
    user_button = pyb.Pin(pyb.Pin.cpu.C13, mode=pyb.Pin.IN)
    
    ## Analog to digital converter on pin A0
    adc1 = pyb.ADC(pyb.Pin.board.PA0)
    
    ## A timer triggering at 20 kHz for ADC sampling
    tim = pyb.Timer(5,freq=20000)
    
    ## A buffer for storing data recorded by the ADC1
    buf1 = array.array('H', (0 for i in range(2000)))
    
    ## A front end UI task object
    task1 = BackEnd_FSM('FSM1', 2, user_button, adc1, buf1, tim)
    
    
    while True:
        task1.run()






# -*- coding: utf-8 -*-
"""
Created on Tue Feb  2 14:33:00 2021

@author: nicho
"""

from pyb import I2C

i2c = I2C(1)                         # create on bus 1
i2c.init(I2C.MASTER, baudrate=100000) # init as a master
print(i2c.is_ready(0x08))
while True:
    
    
    rate = input('Enter an int to change blink rate: ')
    i2c.send(rate,addr=0x08,timeout=5000)
    
    msg = i2c.recv(1,addr=0x08,timeout=5000)
    print(msg)
    # print(int.from_bytes(msg, byteorder='little'))
'''
@file LAB_0x04_main.py

@author E. Nicholson

@date February 06, 2021

@brief      Lab0x04: Hot or Not? main file

@details    This main file is used to measure two sets of data: the internal
            temperature of the microcontroller and the ambient temperature. It
            will record these measurements every 60 seconds along with the
            timestamp at which the measurements were taken. This data is then 
            appended to a CSV file called "temp_data.csv". Column 1 is the time 
            stamp in seconds, column 2 is the core temperature in Celsius, and 
            column 3 is the ambient temperature of the room in Celsius.
            
            ** This is the main file I wrote to test battery powered collection.
               I had to add a way to trigger collection without a PC, so included
               the use of the user button.

            The source code for this file can be found here: 
    
            https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me405/me405%20Lab%200x04/LAB_0x04_main.py
'''

import pyb 
from pyb import I2C
from mcp9808 import MCP9808
import utime
import micropython

def user_button_pressed(IRQ_source):
    ''' 
    @brief Callback which runs when the user presses the blue input button on the Nucleo
    @details This function is triggered when the Nucleo's onboard button is pressed.
             It records the reaction timer count, indexes the number of presses,
             and check to see if the button was pressed early.
    @param IRQ_source The source of the interrupt. This must be taken as an arg, but is not used
    '''
    global PRESS
    
    PRESS = True

## A pin object bound to the user button (Pin PC13)
user_button = pyb.Pin(pyb.Pin.cpu.C13, mode=pyb.Pin.IN)

# Preallocation of memory incase the callback function throws an exception. 
#   This allows the user to view the exception.
micropython.alloc_emergency_exception_buf(100)

## The callback for the user button. Activation occurs on a falling edge.
extint = pyb.ExtInt(user_button,pyb.ExtInt.IRQ_FALLING,pyb.Pin.PULL_NONE,user_button_pressed)

## A pin object bound to the board Led (Pin A5)
board_LED = pyb.Pin(pyb.Pin.cpu.A5, mode=pyb.Pin.OUT)

board_LED.value(0)

##  A boolean indicating if the user button has been pressed
PRESS = 0

## The starting timestamp for the temperature recording program
start_time = utime.ticks_ms()

## The ADCALL object used to measure the Nucleo's core temperature
core_sensor = pyb.ADCAll(12, 0x70000) # 12 bit resolution, internal channels

## The Nucelo's reference voltage
vref = core_sensor.read_core_vref()

print('Ref Voltage: ' + str(vref) + ' V')

## The pyb.I2C object used to communicate with the MCP9808 sensor. The communication
#   channel is opened on bus 1, and the Nucleo is designated as the master.
i2c = I2C(1, I2C.MASTER)

print('The available devices addresses on the bus are...')
print(i2c.scan())

## The mcp9808 object connected to the external temperature sensor
temp_sensor = MCP9808(i2c,0)

print('Checking Sensor...')
print(temp_sensor.check())

with open('temp_data.csv','w') as data_file:
    try:
        print('Press the blue user button to continue...')
        
        # Pauses to wait for user input to begin collection
        while True:
            if PRESS == True:
                Press = False
                board_LED.value(1)
                break

        while True:
            cur_time = utime.ticks_ms()
            el_time = utime.ticks_diff(cur_time,start_time)
            
            ## The Nucleo's core temperature in C
            core_temp = core_sensor.read_core_temp()
            
            ## The ambient temperature read by the MCP9808 sensor in C
            amb_temp = temp_sensor.celcius()
            
            data_file.write('{:},{:},{:}\n'.format(el_time/1000,core_temp,amb_temp))
            
            print('''
                  Data recorded: 
                         Time: {:} min
                         Core: {:} C
                      Ambient: {:} C
                      
                      '''.format(el_time/60000,core_temp,amb_temp))
            
            utime.sleep(60)

    except:
        print('Exiting Program...')
        board_LED.value(0)
    






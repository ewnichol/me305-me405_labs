'''
@file system_ctrl_design.py

@author E. Nicholson

@date March 10, 2021

@brief      This file outlines the controller design process for my balancing platform system.

@details    I completed a symbolic kinematic and kinetic analysis of the system on paper to
            define my equations of motion. I then used MATLAB as a symbolic algebra tool to
            simplfy / linearize my system and  place it in state space form. This script
            pulls the most recent matrix output from my MATLAB script and uses it to design
            two full state feedback controllers. One controller is used to balance the platform 
            by itself. The other is used to balance the platform and ball together.
            
            The source code for this file can be found here: 
    
            LINK
            
'''

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import control

def plot_sim(sim, title='', xlabel='', ylabel1='', ylabel2='', fig_size=(4,5)):
    '''
    @brief This function creates an appropriately formatted system test plot.
    @details This function plots the time response of a given parameter and
             its derrivative. The parameter sim shold be a nx3 array type object
             of the following format [time,x,x_dot]
    @param sim_results a matrix with the system simulation output.
    @param title        A str describing the whole figure.
    @param xlabel       A str describing the x axis
    @param ylabel1      A str describing the y axis of the first plot
    @param ylabel2      A str describing the y axis of the second plot
    @param fig_size A tuple giving the width and height of the figure.
    '''
    fig, axs = plt.subplots(2,1,sharex=True,figsize=fig_size)
    axs[0].plot(sim[0],sim[1])
    axs[0].set_ylabel(ylabel1)
    axs[1].plot(sim[0],sim[2])
    axs[1].set_ylabel(ylabel2)
    axs[1].set_xlabel(xlabel)
    fig.suptitle(title, fontsize=20)
        
def comp_plot_sim(sim1, sim2, ylim1='auto', ylim2='auto', title='', subtitle1='', subtitle2='', xlabel='', ylabel1='', ylabel2='', fig_size=(13,6)):
    '''
    @brief This function creates an appropriately formatted system test plot.
    @details This function plots the time response of a given parameter and
             its derrivative. The parameter sim shold be a nx3 array type object
             of the following format [time,x,x_dot]
    @param sim1         A matrix with the first system simulation output.
    @param sim2         A matrix with the second system simulation output.
    @param ylim1        Tuple describing the ylimits for the first comparison
    @param ylim2        Tuple describing the ylimits for the second comparison
    @param title        A str describing the whole figure.
    @param subtitle1    A str describing the first simulation
    @param subtitle2    A str describing the second simulation
    @param xlabel       A str describing the x axes
    @param ylabel1      A str describing the y axis of the first comparison
    @param ylabel2      A str describing the y axis of the second comparison
    @param fig_size A tuple giving the width and height of the figure.
    '''
    fig, axs = plt.subplots(2,2,sharex=True,sharey='row',figsize=fig_size)
    axs[0,0].plot(sim1[0],sim1[1])
    axs[0,0].set_ylabel(ylabel1)
    axs[0,0].set_title(subtitle1)
    axs[1,0].plot(sim1[0],sim1[2])
    axs[1,0].set_ylabel(ylabel2)
    axs[1,0].set_xlabel(xlabel)
    axs[0,1].plot(sim2[0],sim2[1])
    axs[0,1].set_title(subtitle2)
    axs[1,1].plot(sim2[0],sim2[2])
    axs[1,1].set_xlabel(xlabel)
    fig.suptitle(title, fontsize=20)
    
    if ylim1 != 'auto':
        axs[0,0].set_ylim(ylim1)
    if ylim2 != 'auto':
        axs[1,0].set_ylim(ylim2)
    
    
def plot_pole(p, title, fig_size):
    '''
    @brief This function creates an appropriately formatted system test plot.
    @param p A vector containing the system poles for plotting.
    @param sim_results a matrix with the system simulation output.
    @param title A str describing the plot.
    @param fig_size A tuple giving the width and height of the figure.
    '''
    fig, ax = plt.subplots()
    ax.plot(p, [1, 4, 2, 3])  # Plot some data on the axes.



'''
First the state space matrices produced in MATLAB are imported and the output
matrices are defined. These are used to create a state space system with the
Python Control Systems Toolbox.

    My state variables are:
        
        X = [     x     ;
               theta_y  ;
                x_dot   ;
             theta_y_dot]
'''
A = np.asmatrix(np.loadtxt('A.txt',delimiter=','))
# print(A)
B = np.asmatrix(np.loadtxt('B.txt',delimiter=',')).transpose()
# print(B)
  
C = np.matrix('''
              1 0 0 0;
              0 1 0 0;
              0 0 1 0;
              0 0 0 1
              ''')

D = 0

sys_full_OL = control.StateSpace(A,B,C,D)

'''
I using the full system state space matrices, I also create a model that neglects
all of the dynamics associated with the ball. This 2nd order system is also packaged
into a state space object.
'''
A_p = np.matrix([[A[1,1], A[1,3]], [A[3,1], A[3,3]]])

B_p = np.matrix([[B[1,0]], [B[3,0]]])

C_p = np.matrix('''
                1 0;
                0 1
                ''')

D_p = 0

sys_plat_OL = control.StateSpace(A_p,B_p,C_p,D_p)


'''
Platform Only Controller Design:
'''

# First I record the open loop poles of my system.
plat_OL_pole = control.pole(sys_plat_OL)

# Next, I set up identity weight matrices to begin LQR design. This gives me a
#   reference point for tuning my regulator. 
Q_plat = np.diag([1,1])

R_plat = np.diag([1])

# Using the control sytems library, I compute optimal FSFB controller gains
#   based on the identity weight matrices defined above.
K, S, E = control.lqr(sys_plat_OL,Q_plat,R_plat)
K_plat_0 = np.matrix(K)

# Now with these gains, I find the closed loop sytem dynamics for our regulator
#   using the following formula:
#
#       A_cl = A - B*K
#   
#   Since we are designing a regulator, r = 0 leaving us with a new system 
#   defined by:
#
#       x_dot = A_cl*x
#
A_p_CL0 = A_p - (B_p*K_plat_0)

# Here I create the CL state space system, recorod its poles, and check its 
#   response to an initial tilt of 5 degrees.
sys_plat_CL0 = control.StateSpace(A_p_CL0,0*B_p,C_p,D_p)
plat_CL0_pole = control.pole(sys_plat_CL0)

T = np.arange(0,3,.001)
X0 = [5*3.14159/180, 0]
R_plat_CL0 = control.initial_response(sys_plat_CL0,T,X0)

fig = plt.figure()

plot_sim([R_plat_CL0[0],R_plat_CL0[1][0],R_plat_CL0[1][1]], 
                     title='Platform Only CL Response (untuned)', 
                    xlabel='Time [sec]', 
                    ylabel1='Angle [rad]',
                    ylabel2='Angular Velocity [rad/s]')

# Now that with this as a starting point, we can tune the cost function of our
#   linear quadratic regulator to improve performance.
Q_plat = np.diag([5,.1])

R_plat = np.diag([.05])

K, S, E = control.lqr(sys_plat_OL,Q_plat,R_plat)
K_plat_1 = np.matrix(K)

A_p_CL1 = A_p - (B_p*K_plat_1)

# Here I create the CL state space system, recorod its poles, and check its 
#   response to an initial tilt of 5 degrees.
sys_plat_CL1 = control.StateSpace(A_p_CL1,0*B_p,C_p,D_p)
plat_CL1_pole = control.pole(sys_plat_CL1)

T = np.arange(0,3,.001)
X0 = [5*3.14159/180, 0]
R_plat_CL1 = control.initial_response(sys_plat_CL1,T,X0)

plot_sim([R_plat_CL1[0],R_plat_CL1[1][0],R_plat_CL1[1][1]], 
                     title='Platform Only CL Response (tuned)',  
                    xlabel='Time [sec]', 
                    ylabel1='Angle [rad]',
                    ylabel2='Angular Velocity [rad/s]')

# Plot a side-by-side of the two systems to show the results of tuning
comp_plot_sim([R_plat_CL0[0],R_plat_CL0[1][0],R_plat_CL0[1][1]], 
              [R_plat_CL1[0],R_plat_CL1[1][0],R_plat_CL1[1][1]], 
                     title='Platform Only CL Response (Tuning Results)',  
                 subtitle1='Untuned Response', 
                 subtitle2='Tuned Response', 
                    xlabel='Time [sec]', 
                    ylabel1='Angle [rad]',
                    ylabel2='Angular Velocity [rad/s]')

'''
Full System Controller Design:
'''

# First I record the open loop poles of my system.
sys_OL_pole = control.pole(sys_full_OL)

# Next, I set up identity weight matrices to begin LQR design. This gives me a
#   reference point for tuning my regulator. 
Q_sys = np.diag([1,1,1,1])

R_sys = np.diag([1])

# Using the control sytems library, I compute optimal FSFB controller gains
#   based on the identity weight matrices defined above.
K, S, E = control.lqr(sys_full_OL,Q_sys,R_sys)
K_sys_0 = np.matrix(K)

# Now with these gains, I find the closed loop sytem dynamics for our regulator
#   using the following formula:
#
#       A_cl = A - B*K
#   
#   Since we are designing a regulator, r = 0 leaving us with a new system 
#   defined by:
#
#       x_dot = A_cl*x
#
A_CL0 = A - (B*K_sys_0)

# Here I create the CL state space system, recorod its poles, and check its 
#   response to an initial tilt of 5 degrees.
sys_CL0 = control.StateSpace(A_CL0,0*B,C,D)
sys_CL0_pole = control.pole(sys_CL0)

T = np.arange(0,5,.001)
# X0 = [0.001619, 5*3.14159/180, 0, 0]
X0 = [.005, 0, 0, 0]
R_sys_CL0 = control.initial_response(sys_CL0,T,X0)

plot_sim([R_sys_CL0[0],R_sys_CL0[1][0]*1000,R_sys_CL0[1][2]*1000], 
                     title='System CL Response Ball (untuned)', 
                    xlabel='Time [sec]', 
                    ylabel1='Position [mm]',
                    ylabel2='Velocity [mm/s]')

plot_sim([R_sys_CL0[0],R_sys_CL0[1][1],R_sys_CL0[1][3]], 
                     title='System CL Response Platform (untuned)', 
                    xlabel='Time [sec]', 
                    ylabel1='Angle [rad]',
                    ylabel2='Angular Velocity [rad/s]')

# Now that with this as a starting point, we can tune the cost function of our
#   linear quadratic regulator to improve performance.
Q_sys = np.diag([10,2,.1,.1])

R_sys = np.diag([.1])

# Using the control sytems library, I compute optimal FSFB controller gains
#   based on the identity weight matrices defined above.
K, S, E = control.lqr(sys_full_OL,Q_sys,R_sys)
K_sys_1 = np.matrix(K)

# Now with these gains, I find the closed loop sytem dynamics for our regulator
#   using the following formula:
#
#       A_cl = A - B*K
#   
#   Since we are designing a regulator, r = 0 leaving us with a new system 
#   defined by:
#
#       x_dot = A_cl*x
#
A_CL1 = A - (B*K_sys_1)

# Here I create the CL state space system, recorod its poles, and check its 
#   response to an initial tilt of 5 degrees.
sys_CL1 = control.StateSpace(A_CL1,0*B,C,D)
sys_CL1_pole = control.pole(sys_CL1)

T = np.arange(0,5,.001)
# X0 = [0.001619, 5*3.14159/180, 0, 0]
X0 = [.005, 0, 0, 0]
R_sys_CL1 = control.initial_response(sys_CL1,T,X0)

plot_sim([R_sys_CL1[0],R_sys_CL1[1][0]*1000,R_sys_CL1[1][2]*1000], 
                     title='System CL Response Ball (tuned)', 
                    xlabel='Time [sec]', 
                    ylabel1='Position [mm]',
                    ylabel2='Velocity [mm/s]')

plot_sim([R_sys_CL1[0],R_sys_CL1[1][1],R_sys_CL1[1][3]], 
                     title='System CL Response Platform (tuned)', 
                    xlabel='Time [sec]', 
                    ylabel1='Angle [rad]',
                    ylabel2='Angular Velocity [rad/s]')

# Plot a side-by-side of the two systems ball behavior to show the results of tuning
comp_plot_sim([R_sys_CL0[0],R_sys_CL0[1][0]*1000,R_sys_CL0[1][2]*1000], 
              [R_sys_CL1[0],R_sys_CL1[1][0]*1000,R_sys_CL1[1][2]*1000], 
                     title='System CL Response (Tuning influence on ball)',  
                 subtitle1='Untuned Response', 
                 subtitle2='Tuned Response', 
                    xlabel='Time [sec]', 
                    ylabel1='Position [mm]',
                    ylabel2='Velocity [mm/s]')

# Plot a side-by-side of the two systems platform behavior to show the results of tuning
comp_plot_sim([R_sys_CL0[0],R_sys_CL0[1][1],R_sys_CL0[1][3]], 
              [R_sys_CL1[0],R_sys_CL1[1][1],R_sys_CL1[1][3]], 
                     title='Full System CL Response (Tuning influence on platform)',  
                 subtitle1='Untuned Response', 
                 subtitle2='Tuned Response', 
                    xlabel='Time [sec]', 
                    ylabel1='Angle [rad]',
                    ylabel2='Angular Velocity [rad/s]')

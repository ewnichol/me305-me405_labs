# -*- coding: utf-8 -*-
'''
@file       IMU_Test.py
@brief      A test script for the Bosch BNO055 IMU
@detials    This script streams csv pitch and roll data from a BNO055 IMU 
            using UART. It is designed to be read using Serial Oscilloscope. 
@author     Enoch Nicholson
@date       March 09, 2021
'''

import pyb, utime
from machine import I2C, Pin
from bno055 import BNO055

## Creates UART object for sending serial data
uart = pyb.UART(2)

## Initializes the I2C bus
i2c = I2C(-1, scl=Pin('PB8'), sda=Pin('PB9'), freq=115200)

## Start time of the data collection
start_time = utime.ticks_us()

## Initializes the bno055 driver using the I2C object
imu = BNO055(i2c)
    
# Collect data until the user presses Ctrl-c

try:
    while True:
        curr_time = utime.ticks_us()
        time = utime.ticks_diff(curr_time, start_time)/1e6
        eulr=imu.euler()
        omga=imu.gyro()
        uart.write('{:},{:},{:},{:},{:}\r\n'.format(time,eulr[1],omga[1],eulr[2],omga[0]))
except:
    # Prints confirmation that data is complete
    print ("The file has now automatically been closed.")
    


'''
@file enc_driver.py

@author E. Nicholson

@date October 12, 2020

@brief      This file contains a class that acts as an encoder driver.

@details    This file contains the class 'EncDriver.' This class facilitates
            configuring a Nucleo - L476RG to interface with an encoder.The
            object that this class creates correctly tracks encoder position, and 
            recent position delta. The encoder position can also be zeroed. The
            file also contains code the facilitates performance testing and debugging.
            
            The source code for this file can be found here: 
    
            https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me405/me405%20Term%20Project/enc_driver.py
'''

import pyb
import utime

class EncDriver:
    '''
    @brief      An encoder driver object.
    @details    This class configures hardware, to monitor an encoder's position.
                The encoder ticks are counted using a timer object, and the 
                timer is queried to get updated position values. The diver class
                corrects timer overflow errors, and is able to handle encoder 
                rotation CW and CCW.
    '''

    def __init__(self, ID, Pin1, Ch1, Pin2, Ch2, tim_no, CPR, debug):
        '''
        @brief          Creates an encoder object
        @param ID       A string identifying the object for traceing output.
        @param Pin1     The pin object that will be bound to encoder channel A
        @param Ch1      The timer channel for the first pin
        @param Pin2     The pin object that will be bound to encoder channel B
        @param Ch2      The timer channel for the second pin
        @param tim_no   The timer number that will be used to read the encoder
        @param CPR      Encoder's cycles per revolution
        @param debug    A boolean that toggles the trace output on or off
        '''
        
        # ---- General Setup ----
        
        ##  The unique ID string for the object, used for trace output
        self.ID = ID
        
        ## A boolean that toggles the trace output to the command line
        self.debug = debug
        
        ##  The timer objct that is used to read the encoder. The prescaler is
        # set to zero and the period is set to 0xFFFF or 65535
        self.tim = pyb.Timer(tim_no, prescaler=0, period=0xFFFF)
        
        ## Conversion factor ticks -> degrees
        self.conv_deg = 360/(CPR*4)
 
        ## Conversion factor ticks -> radians
        self.conv_rad = 2*3.14159265359/(CPR*4)
        
        # This binds the two pins passed to the class to the first two channels
        # of the desired timer.
        self.tim.channel(Ch1, pyb.Timer.ENC_AB, pin=Pin1)
        self.tim.channel(Ch2, pyb.Timer.ENC_AB, pin=Pin2)
        
    
        if self.debug:
            print('Channel '+str(Ch1)+' bound to ' + 'Pin1')
            print('Channel '+str(Ch2)+' bound to ' + 'Pin2')
            print('Timer ' + str(tim_no) + ' initialized!')

       
        # ---- Indexing & Counting ----

        ##  The current position of the encoder
        self.pos = 0

        ##  The previous position of the encoder
        self.prev_pos = 0
        
        ##  The difference between the current position and the previous position
        self.delta = 0
        
        ##  The previous delta of the encoder
        self.prev_delta = 0
        
        ## The difference between the current delta and the previous delta
        self.ddelta = 0
        
        self.convert()
        
        self.last_count = 0
        
        # ---- Debug ----
        
        print('Creating an encoder driver')
                
    def update(self):
        '''
        @brief      Updates the encoder
        @details    This function passes the position from the last call to the
                    prev_pos atribute, calculates a new delta value, corrects
                    the delta value, and updates the encoder position.
        '''
        ## Ticks value 1
        self.t1 = self.last_count
        
        ## Ticks value 2 from counter
        self.t2 = self.tim.counter()
        
        ## Previous encoder count
        self.last_count = self.t2
        
        self.delta = self.t2 - self.t1
        #self.delta = (self.tim.counter() - self.prev_pos)

        
        if self.delta > 0xFFFF/2:
            self.delta -= 0xFFFF
        elif self.delta < -0xFFFF/2:
            self.delta += 0xFFFF
        else:
            pass
        
        self.ddelta = self.delta - self.prev_delta
        
        self.pos += self.delta 
        
        self.prev_pos = self.pos
        self.prev_delta = self.delta
        
        self.convert()
        
        if self.debug:
            self.trace()
            
    def get_position(self, units):
        '''
	    @brief     Returns the encoder position in desired units
        '''
        if units == 'deg':
            return self.pos_deg
        elif units == 'rad':
            return self.pos_rad
        else:
            return self.pos


    def set_position(self):
        '''
	    @brief     Sets the encoder position to a specified value in ticks
        '''
        self.pos = 0
        self.prev_pos = 0
        self.delta = 0
        self.prev_delta = 0
        self.ddelta = 0
        self.tim.counter(0)
        self.last_count = 0
        self.t1 = 0
        self.t2 = 0
        self.convert()
        
    def get_delta(self,units):
        '''
        @brief     Returns the current encoder delta in desired units
        '''
        if units == 'deg':
            return self.delta_deg
        elif units == 'rad':
            return self.delta_rad
        else:
            return self.delta
        
    def get_ddelta(self):
        '''
        @brief     Returns the current encoder delta delta in ticks
        '''
        return self.ddelta
    
    def convert(self):
        '''
        @brief  Uses the raw encoder count values to calculate encoder position & delta in other units
        '''
        ## Encoder position in degrees
        self.pos_deg = self.pos*self.conv_deg

        ## Encoder position in radians
        self.pos_rad = self.pos*self.conv_rad

        ## Encoder delta in degrees
        self.delta_deg = self.delta*self.conv_deg

        ## Encoder delta in radians
        self.delta_rad = self.delta*self.conv_rad

        
    def trace(self):
        '''
        @brief  Prints a trace output when called
        '''
        print(self.ID + '\n'
              + 'Pos: ' + str(self.pos) + '\n'
              + 'Delta: ' + str(self.delta) + '\n')
        
if __name__ == '__main__':
    '''
    Test program for encoder driver using ME 405 hardware.
    '''
    
    # Creates pin objects for interfacing with the encoders
    
    ## The pin object connected to Encoder 1's channel 1
    pin_E1CH1 = pyb.Pin(pyb.Pin.cpu.B6)
    
    ## The pin object connected to Encoder 1's channel 2
    pin_E1CH2 = pyb.Pin(pyb.Pin.cpu.B7)
    
    ## An encoder object representing the encoder attached to Motor 1
    e1 = EncDriver('E1', pin_E1CH1, 1, pin_E1CH2, 2, 4, 1000, False)
    
    ## The pin object connected to Encoder 2's channel 1
    pin_E2CH1 = pyb.Pin(pyb.Pin.cpu.C6)
    
    ## The pin object connected to Encoder 2's channel 2
    pin_E2CH2 = pyb.Pin(pyb.Pin.cpu.C7)

    ## An encoder object representing the encoder attached to Motor 2
    e2 = EncDriver('E2', pin_E2CH1, 1, pin_E2CH2, 2, 8, 1000, False)
    
    while True:
        
        ## The test type used (running or timed)
        test_type = input('Running test or time test? (r/t): ')
        
        ## The  encoder selection for testing
        test_enc = input('Running test or time test? (A/B): ')
        
        e1.update()
        e2.update()
        
        # try:
        
        if test_type == 't':
            if test_enc == 'B' or test_enc == 'b':
                print('Testing Encoder B...')
                while True:
                   start = utime.ticks_us()
                   pos = e2.get_position('deg')
                   stop = utime.ticks_us()
                   run_time = utime.ticks_diff(stop,start)
                   print('ENCB read pos of {:} [deg] in {:} us\n'.format(pos,run_time))
                   
                   start = utime.ticks_us()
                   delta = e2.get_delta('deg')
                   stop = utime.ticks_us()
                   run_time = utime.ticks_diff(stop,start)
                   print('ENCB read delta of {:} [deg] in {:} us\n'.format(delta,run_time))
                  
                   e2.update()
                   
                   input('     Press enter to test again: ')
            else:
                print('Testing Encoder A...')
                while True:
                   start = utime.ticks_us()
                   pos = e1.get_position('deg')
                   stop = utime.ticks_us()
                   run_time = utime.ticks_diff(stop,start)
                   print('ENCA read pos of {:} [deg] in {:} us\n'.format(pos,run_time))
                   
                   start = utime.ticks_us()
                   delta = e1.get_delta('deg')
                   stop = utime.ticks_us()
                   run_time = utime.ticks_diff(stop,start)
                   print('ENCA read delta of {:} [deg] in {:} us\n'.format(delta,run_time))
                   
                   e1.update()
                   
                   input('     Press enter to test again: ')
        else:
            if test_enc == 'B' or test_enc == 'b':
                print('Testing Encoder B...')
                while True:
                    pos = e2.get_position('deg')
                    delta = e2.get_delta('deg')
                    print('ENCB read pos:{:}[deg] delta:{:}[deg]'.format(pos,delta))
                    e2.update()
                    utime.sleep_ms(250)
            else:
                print('Testing Encoder A...')
                while True:
                    pos = e1.get_position('deg')
                    delta = e1.get_delta('deg')
                    print('ENCA read pos:{:}[deg] delta:{:}[deg]'.format(pos,delta))
                    e1.update()
                    utime.sleep_ms(250)
        # except:
        #     break
    

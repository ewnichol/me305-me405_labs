'''
@file system_modeling.py

@author E. Nicholson

@date February 22, 2021

@brief      This file test the behavior of my balancing platform system model.

@details    This file works alongside a MATLAB live script that computes the 
            open loop state space system matrices for the ME 405 balancing platform
            term project. It pulls the matrix output of the script from two text files
            and tests the behavior in open loop and closed loop configuration using the
            python controls toolbox. 
            
            The source code for this file can be found here: 
    
            https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me405/me405%20Term%20Project/system_modeling.py
'''

import numpy as np
import matplotlib.pyplot as plt
import control

def plot_plat_sim(sim_time, sim_result, title, fig_size):
    '''
    @brief Th function creates an appropriately formatted system test plot.
    @param sim_time a vector containing the simulation time steps.
    @param sim_results a matrix with the system simulation output.
    @param title A str describing the plot.
    @param fig_size A tuple giving the width and height of the figure.
    '''
    fig, axs = plt.subplots(2,2,sharex='col',figsize=fig_size)
    axs[0,0].plot(sim_time,sim_result[1][0]*100)
    axs[0,0].set_ylabel('Position [cm]')
    axs[0,0].set_title('Ball Position (X)')
    axs[0,1].plot(sim_time,sim_result[1][1])
    axs[0,1].set_ylabel('Angle [rad]')
    axs[0,1].set_title('Platform Angle (theta_y)')
    axs[1,0].plot(sim_time,sim_result[1][2]*100)
    axs[1,0].set_ylabel('Velocity [cm/s]')
    axs[1,0].set_xlabel('Time [s]')
    axs[1,0].set_title('Ball Speed (X_dot)')
    axs[1,1].plot(sim_time,sim_result[1][3])
    axs[1,1].set_ylabel('Angular Velocity [rad/s]')
    axs[1,1].set_xlabel('Time [s]')
    axs[1,1].set_title('Platform Angular Velocity (theta_dot_y)')
    fig.suptitle(title, fontsize=20)

## State space system state matrix A
A = np.asmatrix(np.loadtxt('A.txt',delimiter=','))
# print(A)

## State space system input matrix B
B = np.asmatrix(np.loadtxt('B.txt',delimiter=',')).transpose()
# print(B)
  
## State space system state output matrix C
C = np.matrix('''
              1 0 0 0;
              0 1 0 0;
              0 0 1 0;
              0 0 0 1
              ''')

## State space system input output matrix D
D = 0

## State space system object
sys = control.StateSpace(A,B,C,D)

'''
 Open Loop Testing
   (a) Ball at rest on a level platform directly above platform CG. No torque
       is applied by the motor. The scenario is simulated for 1 sec.
'''

## Simulation A time vector
TA = np.arange(0,1,.001)
## Simulation A initial conditions
X0A = [0, 0, 0, 0]

## Simulation A results
RA = control.initial_response(sys,TA,X0A)

plot_plat_sim(TA, RA, 'System Model OL Testing (case a)', (13,6))

'''
   (b) Ball at rest on a level platform offset horizontally from the platform's
       center by 5 cm. No torque is applied by the motor.The scenario is 
       simulated for 0.4 sec.
'''

## Simulation B time vector
TB = np.arange(0,0.4,.001)
## Simulation b initial conditions
X0B = [.05, 0, 0, 0]

## Simulation B results
RB = control.initial_response(sys,TB,X0B)

plot_plat_sim(TB, RB, 'System Model OL Testing (case b)', (13,6))

'''
   (c) Ball at rest on a platform inclined at 5 deg directly above the platform's
       center of gravity. No torque is applied. Scenario is simulated for 0.4 sec.
'''

## Simulation C time vector
TC = np.arange(0,0.4,.001)
## Simulation C initial conditions
X0C = [0.001619, 5*3.14159/180, 0, 0]

## Simulation C results
RC = control.initial_response(sys,TC,X0C)

plot_plat_sim(TC, RC, 'System Model OL Testing (case c)', (13,6))

'''
   (d) Ball at rest on a level platform directly above platform CG. A torque
       of 1 [m Nm s]is applied by the motor. The scenario is simulated for 
       0.04 sec.
'''

## Simulation D time vector
TD = np.arange(0,0.4,.001)
## Simulation D input signal
u0D = np.zeros((1,len(TD)))
u0D[0:4] = 0.001

## Simulation D input signal
X0D = [0, 0, 0, 0]

## Simulation D results
RD = control.forced_response(sys,TD,u0D,X0D)

plot_plat_sim(TD, RD, 'System Model OL Testing (case d)', (13,6))

# Creating CL system

## Closed loop systems gain matrix
K = np.matrix('''-0.3 -0.2 -0.05 -0.02''')

## Closed loop state space system state matrix
A_cl = np.subtract(A,np.matmul(B,K))

## Closed loop state space system input matrix set to zeros since the system is used as a regulator
B_cl = np.matrix('''
                 0;
                 0;
                 0;
                 0
                 ''')
                 
## Closed loop state space system object
sys_cl = control.StateSpace(A_cl,B_cl,C,D)

'''
Closed Loop Testing
   (a) Ball at rest on a level platform directly above platform CG. No torque
       is applied by the motor. The scenario is simulated for 1 sec.
'''

## Closed loop simulation A time vector
TA_cl = np.arange(0,1,.001)
## Closed loop simulation A initial conditions
X0A_cl = [0, 0, 0, 0]

## Closed loop simulation A results
RA_cl = control.initial_response(sys_cl,TA_cl,X0A_cl)

plot_plat_sim(TA_cl, RA_cl, 'System Model CL Testing (case a)', (13,6))

'''
   (b) Ball at rest on a level platform offset horizontally from the platform's
       center by 5 cm. No torque is applied by the motor.The scenario is 
       simulated for 0.4 sec.
'''

## Closed loop simulation B time
TB_cl = np.arange(0,20,.001)
## Closed loop simulation B initial conditions
X0B_cl = [.05, 0, 0, 0]

## Closed loop simulation B results
RB_cl = control.initial_response(sys_cl,TB_cl,X0B_cl)

plot_plat_sim(TB_cl, RB_cl, 'System Model CL Testing (case b)', (13,6))

'''
   (c) Ball at rest on a platform inclined at 5 deg directly above the platform's
       center of gravity. No torque is applied. Scenario is simulated for 0.4 sec.
'''

## Closed loop simulation C time
TC_cl = np.arange(0,20,.001)
## Closed loop simulation C initial conditions
X0C_cl = [0.001619, 5*3.14159/180, 0, 0]


## Closed loop simulation C results
RC_cl = control.initial_response(sys_cl,TC_cl,X0C_cl)

plot_plat_sim(TC_cl, RC_cl, 'System Model CL Testing (case c)', (13,6))

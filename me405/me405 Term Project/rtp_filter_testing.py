'''
@file rtp_filter_testing
@brief
@details
'''

from rtp_driver import RTP
import pyb
from pyb import UART
import utime
from array import array
from fir import fir
from avg import avg

## The Nucleo pin connected to RTP xm
PIN_xm = pyb.Pin(pyb.Pin.cpu.A0)

## The Nucleo pin connected to RTP xp
PIN_xp = pyb.Pin(pyb.Pin.cpu.A1)

## The Nucleo pin connected to RTP ym
PIN_ym = pyb.Pin(pyb.Pin.cpu.A6)

## The Nucleo pin connected to RTP yp
PIN_yp = pyb.Pin(pyb.Pin.cpu.A7)

## The RTP objct
RTP = RTP(PIN_xm,PIN_xp,PIN_ym,PIN_yp,(176,100),(88,50),filt='fir', debug=False)

## The read mode for testing (ctr obj count)
mode = 'ctr'

# ---- Sample Timing ----

## Current Time step
Ts = utime.ticks_us()

## Desired time step in [us]
dt = 6000

## Next sample time in [us]
Tns = utime.ticks_add(Ts,dt)

## Sample counter
n = 1

## Time of the first run for offsetting time values
offset = 0

## End time in seconds
end_time = 120

# ---- Data Storage / Transmission ----

## UART object for handling simple serial communication
uart = UART(2)

## Elapsed time in [sec]
el = 0

# ---- Filtering ----

coeffs = array('i', (-124,-38,-12,46,143,285,474,708,982,1285,1602,1917,2210,2462,2656
  ,2779,2821,2779,2656,2462,2210,1917,1602,1285,982,708,474,285,143,46
  ,-12,-38,-124))

ncoeffs = len(coeffs)

navg = 10

xfilt_buf = array('i',[0]*int(ncoeffs+3))
xfilt_buf[0] = ncoeffs
xfilt_buf[1] = 13

# xavg_buf = array('i',[0]*(navg+3))
# xavg_buf[0] = len(xavg_buf)

yfilt_buf = array('i',[0]*int(ncoeffs+3))
yfilt_buf[0] = ncoeffs
yfilt_buf[1] = 13

# el = array('f',[0]*int(end_time/(dt/10**6)))
# samp_idx = array('i',[0]*int(end_time/(dt/10**6)))
# x_raw = array('f',[0]*int(end_time/(dt/10**6)))
# y_raw = array('f',[0]*int(end_time/(dt/10**6)))
# z = array('f',[0]*int(end_time/(dt/10**6)))
    
input('Press enter to start: ')

print('Collecting Data...')

# Collect data
while el/10**6 < end_time:
    # print('IN WHILE')
    # print('{:}, {:}'.format(el[n-1],end_time))
    Ts = utime.ticks_us()

    if utime.ticks_diff(Ts,Tns) > 0:
        # print('RUN')
        if n == 1:
            offset = Ts
        
        el = (Ts-offset)
        # samp_idx[n] = n
        
        pos = RTP.scan_demo(mode='counts',avg_type=10)
        
        # x_filt = fir(xfilt_buf,coeffs,pos[0])*(.22026)
        # y_filt = fir(yfilt_buf,coeffs,pos[1])*(.22026)
        
        # x_avg = avg(xavg_buf,pos)
        
        # x_raw[n] = pos[0]
        # y_raw[n] = pos[1]
        
        # z[n] = pos[2]
        
        # uart.write('{:},{:},0,{:},{:},0,{:}\r\n'.format(pos[0],x_filt,pos[1],y_filt,el))
        uart.write('{:},{:},{:},{:},{:},{:}\r\n'.format(pos[0],pos[8],pos[11],pos[1],pos[2],pos[3]))
        # uart.write('{:},{:},{:},{:}\r\n'.format(pos,x_filt,pos-x_filt,el))
        # uart.write('{:},{:},{:},{:},{:},{:},{:}\r\n'.format(x,pos[0],0,y,pos[1],0,el))

        
        Tns = utime.ticks_add(Ts,dt)
        n += 1
        
        # if el > end_time:
            # break
    else:
        pass
        # print('PASS')
            
            
print('End of Collection...')

# # Creates a csv file to append data to
# with open ('rtpData.csv', 'w') as data:
#     data.write('IDX,T,X_r,Y_r,Z\n')
#     for i in samp_idx:
#         data.write('{:},{:},{:},{:},{:}\n'.format(samp_idx[i], el[i], x_raw[i], y_raw[i], z[i]))

# print('CSV Created!')
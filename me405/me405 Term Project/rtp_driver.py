'''
@file rtp_driver.py

@author E. Nicholson

@date February 04, 2021

@brief      This file contains a driver for a resistive touch pannel (RTP)

@details    The class contained within this file is a driver for a resistive touch
            pannel. It allows for the scanning of x and y contact coordinates as
            well as contact confirmation. The file also holds test code for the
            RTP driver. This test code is designed for use with the ME 405 term
            project hardware.
            
            The source code for this file can be found here: 
    
            https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me405/me405%20Term%20Project/rtp_driver.py
'''

from pyb import ADC
from pyb import Pin
import utime
from avg import avg
from fir import fir
from array import array

class RTP:
    '''
    @brief      A driver object for a resistive touch pannel.
    
    @details    This class drives a resistive touch pannel. It allows a user to check
                x and y contact points and to verify that there is contact anywhere (z).
                All measurements can be returned in three forms: raw ADC counts,
                position in mm referenced to a 'pannel center' coordinate tuple
                passed to the constructor, and position in mm referenced to the
                absolute zero point of the pannel.
                
                RTP Driver Updates:  This class was extensively modified 
                for use in the ME 405 term project. The individual axis scan methods 
                were not changed, but the full scan method was updated to perform
                both averaging and filtering. The scan method of the driver can 
                be used with batch averaging running averaging, and FIR filtering.
                
    '''
    
    # ---- Predefinition of Pin Modes ----
    
    ##  Out Push-Pull Mode
    OUT_PP = Pin.OUT_PP
    
    ##  In Mode
    IN = Pin.IN
    

    
    def __init__(self, PIN_xm, PIN_xp, PIN_ym, PIN_yp, pannel_aa, pannel_cntr, ID=1, filt='none', debug=False):
        '''
        @brief          Creates a RTP object. 
        @details        This function creates a resistive touch pannel object.
                        
        @param PIN_xm       The Pin object connected to the xm terminal on the RTP.
        @param PIN_xp       The Pin object connected to the xp terminal on the RTP.
        @param PIN_ym       The Pin object connected to the ym terminal on the RTP.
        @param PIN_yp       The Pin object connected to the yp terminal on the RTP.
        @param pannel_aa    A tuple containing the active area of the pannel (x_aa,y_aa).
        @param pannel_cntr  A tuple containing the center position of the pannel as referenced from pannel origin (x_ctr,y_ctr).
        @param ID           An interger used for identifying the object. Object ID will be 'RTP_ID'
        @param filt         A string describing the desired type of running filtering (none, avg, fir)
        @param debug        A boolean which toggles debug output to the REPL.
        '''
        
        # ---- General Setup ----
        
        ## The Pin object connected to the xm terminal on the RTP
        self.xm = PIN_xm
        
        ## The ADC object on pin xm
        self.ADC_xm = ADC(self.xm)
        
        ## The Pin object connected to the xp terminal on the RTP
        self.xp = PIN_xp
        
        ## The Pin object connected to the ym terminal on the RTP
        self.ym = PIN_ym
        
        ## The ADC object on pin ym
        self.ADC_ym = ADC(self.ym)
        
        ## The Pin object connected to the yp terminal on the RTP
        self.yp = PIN_yp
        
        # ---- Pannel Sizing ----
        
        ## The x active area dimension for the RTP in [mm]
        self.x_aa = pannel_aa[0]
        
        ## The y active area dimension for the RTP in [mm]
        self.y_aa = pannel_aa[1]
        
        ## The x pannel center coordinate measured from the RTP origin in [mm]
        self.x_ctr = pannel_cntr[0]
        
        ## The y pannel center coordinate measured from the RTP origin in [mm]
        self.y_ctr = pannel_cntr[1]
        
        ## The x calibration ADC counts for creating distances from ADC reading. These must be measured for each RTP used
        self.cal_x = (200,3800)
        
        ## The y calibration ADC counts for creating distances from ADC reading. These must be computed for each RTP used
        self.cal_y = (380,3600)
        
        ## A value for converting ADC counts to x absolute position in [mm]
        self.conv_x = self.x_aa/(self.cal_x[1]-self.cal_x[0])
        
        ## A value for converting ADC counts to y absolute position in [mm]
        self.conv_y = self.y_aa/(self.cal_y[1]-self.cal_y[0])
        
        # ---- Averaging / Filtering ----
        
        ## A string describing the type of filtering performed can be 'none' 'avg' or 'fir'
        self.filt = filt
        
        ## The hard coded number of running averages taken for signal rejection
        self.navg = 5
        
        ## Z Running average buffer for use with the avg library created by Peter Hinch
        self.zavg_buf = array('i',[0]*(self.navg+3))
        self.zavg_buf[0] = len(self.zavg_buf)
        
        ## Class atribute tracking the Z running average
        self.zavg = 0
        
        ## X Running average buffer for use with the avg library created by Peter Hinch
        self.xavg_buf = array('i',[0]*(self.navg+3))
        self.xavg_buf[0] = len(self.xavg_buf)
        
        ## Class atribute tracking the X running average
        self.xavg = 0
        
        ## Y Running average buffer for use with the avg library created by Peter Hinch
        self.yavg_buf = array('i',[0]*(self.navg+3))
        self.yavg_buf[0] = len(self.yavg_buf)
        
        ## Class atribute tracking the Y running average
        self.yavg = 0
        
        ## The FIR filter coefficents designed using TFilter
        self.coeffs = array('i', (-124,-38,-12,46,143,285,474,708,982,1285,1602,1917,2210,2462,2656
                                  ,2779,2821,2779,2656,2462,2210,1917,1602,1285,982,708,474,285,143,46
                                  ,-12,-38,-124))
        
        ## The numebr of coefficents
        ncoeffs = len(self.coeffs)
        
        ## Z FIR filter buffer for use with the fir library created by Peter Hinch
        self.zfilt_buf = array('i',[0]*int(ncoeffs+3))
        self.zfilt_buf[0] = ncoeffs
        self.zfilt_buf[1] = 13
        
        ## X FIR filter buffer for use with the fir library created by Peter Hinch
        self.xfilt_buf = array('i',[0]*int(ncoeffs+3))
        self.xfilt_buf[0] = ncoeffs
        self.xfilt_buf[1] = 13
        
        ## Y FIR filter buffer for use with the fir library created by Peter Hinch
        self.yfilt_buf = array('i',[0]*int(ncoeffs+3))
        self.yfilt_buf[0] = ncoeffs
        self.yfilt_buf[1] = 13
        
        
        # ---- Debug ----
        
        ## A string used for identifying the object
        self.ID = 'RTP_'+str(ID)
        
        ## A boolean that toggles the trace output to the REPL
        self.debug = debug
        
        if self.debug:
            print('INIT RTP object: '+ self.ID)
            
        # ---- Setup ----
               
        # Configuring the to read Z
        self.xm.init(mode=self.OUT_PP,value=0)
        self.yp.init(mode=self.OUT_PP,value=1)
        # self.ym.init(mode=self.IN)
        # self.ym.init(mode=Pin.ANALOG)
        self.ADC_ym = ADC(self.ym)
        self.xp.init(mode=self.IN)
        # self.xp.init(mode=Pin.ANALOG)
        
        ## An interger representing the last read coodinate. It is defined as follows (0=z 1=x 2=y)
        self.last_read = 0
    
    @micropython.native
    def scan_z(self):
        '''
        @brief Checks the RTP z-coordinate (contact)
        @details This method energizes both x and y resistive networks and check 
                 for contact between the two. It returns a boolean indicating the
                 contact state (0 == no contact, 1 == contact).
        @return The z coordinate (0 or 1)
        '''
        
        # Optimizing Pin config based on last read coodinate
        if self.last_read == 1:
            self.yp.init(mode=self.OUT_PP,value=1)
            self.xp.init(mode=self.IN)
            # self.xp.init(mode=Pin.ANALOG)
            
        elif self.last_read == 2:
            self.xm.init(mode=self.OUT_PP,value=0)
            # self.ym.init(mode=self.IN)
            # self.ym.init(mode=Pin.ANALOG)
            self.ADC_ym = ADC(self.ym)
            self.xp.init(mode=self.IN)
            # self.xp.init(mode=Pin.ANALOG)

        # utime.sleep_us(3)
        
        # Read Z
        z = self.ADC_ym.read()
        self.last_read = 0
        
        # Assign boolean based on ADC value
        if z > 4000:
            z = 0
        else:
            z = 1

        if self.debug:
            print('     Checking z... Last was {:}'.format(self.last_read))
            
        return z
    
    @micropython.native
    def scan_x(self, mode='ctr'):
        '''
        @brief Checks the RTP x-coordinate
        @details This method energizes the x resistive network and check 
                 the divider voltage using the ym pin. It returns a float 
                 indicating the x coordinate in [mm]. The zero point of the x 
                 coordinate can be the pannel center or the pannel origin.
        
        @param mode  A string describing the output mode, either 'ctr' 'org' or 'count'
        @return The x coordinate (as a float)
        '''
        
        # Optimizing Pin config based on last read coodinate
        if self.last_read == 0:
            self.xp.init(mode=self.OUT_PP,value=1)
            self.yp.init(mode=self.IN)
            # self.yp.init(mode=Pin.ANALOG)
            
        elif self.last_read == 2:
            self.xm.init(mode=self.OUT_PP,value=0)
            self.xp.init(mode=self.OUT_PP,value=1)
            # self.ym.init(mode=self.IN)
            # self.ym.init(mode=Pin.ANALOG)
            self.ADC_ym = ADC(self.ym)
            self.yp.init(mode=self.IN)
            # self.yp.init(mode=Pin.ANALOG)

        # utime.sleep_us(3)
        
        # Read X
        x = self.ADC_ym.read()
        self.last_read = 1

        if mode == 'ctr':
            x = (self.conv_x*(self.ADC_ym.read()-self.cal_x[0]))-(self.x_ctr)
        elif mode == 'org':
            x = (self.conv_x*(self.ADC_ym.read()-self.cal_x[0]))
        else:
            pass
        

        if self.debug:
            print('     Checking x... Last was {:}'.format(self.last_read))
            
        return x
    
    @micropython.native
    def scan_y(self, mode='ctr'):
        '''
        @brief Checks the RTP y-coordinate
        @details This method energizes the y resistive network and check 
                 the divider voltage using the xm pin. It returns a float 
                 indicating the y coordinate in [mm]. The zero point of the y 
                 coordinate can be the pannel center or the pannel origin.
        
        @param mode  A string describing the output mode, either 'ctr' 'org' or 'count'
        @return The y coordinate (as a float)
        '''
        
        # Optimizing Pin config based on last read coodinate
        if self.last_read == 0:
            self.ym.init(mode=self.OUT_PP,value=0)
            # self.xm.init(mode=self.IN)
            # self.xm.init(mode=Pin.ANALOG)
            self.ADC_xm = ADC(self.xm)
            self.xp.init(mode=self.IN)
            # self.xp.init(mode=Pin.ANALOG)
            
        elif self.last_read == 1:
            self.ym.init(mode=self.OUT_PP,value=0)
            self.yp.init(mode=self.OUT_PP,value=1)
            # self.xm.init(mode=self.IN)
            # self.xm.init(mode=Pin.ANALOG)
            self.ADC_xm = ADC(self.xm)
            self.xp.init(mode=self.IN)
            # self.xp.init(mode=Pin.ANALOG)

        # utime.sleep_us(3)
        
        # Read Y
        y = self.ADC_xm.read()
        self.last_read = 2

        if mode == 'ctr':
            y = (self.conv_y*(self.ADC_xm.read()-self.cal_y[0]))-(self.y_ctr)
        elif mode == 'org':
            y = (self.conv_y*(self.ADC_xm.read()-self.cal_y[0]))
        elif mode == 'count':
            pass

        if self.debug:
            print('     Checking y... Last was {:}'.format(self.last_read))
            
        return y
    
    @micropython.native
    def scan(self,mode='ctr',avg_type='none'):
        '''
        @brief Checks all RTP coordinates as fast as possible
        @details This method runs the three coordinate scan methods without calling other
                 class methods. It returns a tuple containing the coordinates 
                 in the following order: (x,y,z). If no contact is registered 
                 (ie z = 0), the x and y coordinates are set to 0. The coordinates 
                 are returned in [mm] and are only referenced to the 'center' 
                 coordinate passed to the constructor.
        
        @param mode     A string describing the output mode, either 'ctr' 'org' or 'count'
        @param avg_type A string describing the batch averaging performed on each sample 'none' '5' or '10'
        @return A tuple of all three RTP coordinates ref'ed to center.
        '''
        # Optimizing Pin config based on last read coodinate
        if self.last_read == 1:
            self.yp.init(mode=self.OUT_PP,value=1)
            self.xp.init(mode=self.IN)
            
        elif self.last_read == 2:
            self.xm.init(mode=self.OUT_PP,value=0)
            self.ADC_ym = ADC(self.ym)
            self.xp.init(mode=self.IN)

        # Read Z
        if avg_type == 'ten' or avg_type == 10:
            z0 = self.ADC_ym.read()
            z1 = self.ADC_ym.read()
            z2 = self.ADC_ym.read()
            z3 = self.ADC_ym.read()
            z4 = self.ADC_ym.read()
            z5 = self.ADC_ym.read()
            z6 = self.ADC_ym.read()
            z7 = self.ADC_ym.read()
            z8 = self.ADC_ym.read()
            z9 = self.ADC_ym.read()
            z = (z0 + z1 + z2 + z3 + z4 + z5 + z6 + z7 + z8 + z9)/10
        elif avg_type == 'five' or avg == 5:
            z0 = self.ADC_ym.read()
            z1 = self.ADC_ym.read()
            z2 = self.ADC_ym.read()
            z3 = self.ADC_ym.read()
            z4 = self.ADC_ym.read()
            z = (z0 + z1 + z2 + z3 + z4)/5
        else:
            z = self.ADC_ym.read()
        
        self.zavg = avg(self.zavg_buf,z)
        
        if (z-self.zavg) > 500 or (z-self.zavg) < -500:
            z = self.zavg
        
        if self.filt == 'avg':    
            z = self.zavg
        elif self.filt == 'fir':
            z = fir(self.zfilt_buf,self.coeffs,z)*(.22026)
        else:
            pass
            
        if z > 4000:
            z=0
        else:
            z = 1
        
        # Scan Y
        self.ym.init(mode=self.OUT_PP,value=0)
        self.ADC_xm = ADC(self.xm)

        if avg_type == 'ten' or avg_type == 10:
            y0 = self.ADC_xm.read()
            y1 = self.ADC_xm.read()
            y2 = self.ADC_xm.read()
            y3 = self.ADC_xm.read()
            y4 = self.ADC_xm.read()
            y5 = self.ADC_xm.read()
            y6 = self.ADC_xm.read()
            y7 = self.ADC_xm.read()
            y8 = self.ADC_xm.read()
            y9 = self.ADC_xm.read()
            y = (y0 + y1 + y2 + y3 + y4 + y5 + y6 + y7 + y8 + y9)/10
        elif avg_type == 'five' or avg_type == 5:
            y0 = self.ADC_xm.read()
            y1 = self.ADC_xm.read()
            y2 = self.ADC_xm.read()
            y3 = self.ADC_xm.read()
            y4 = self.ADC_xm.read()
            y = (y0 + y1 + y2 + y3 + y4)/5
        else:
            y = self.ADC_xm.read()
        
        self.yavg = avg(self.yavg_buf,y)
        
        if (y-self.yavg) > 100 or (y-self.yavg) < -100:
            y = self.yavg
        
        if self.filt == 'avg':    
            y = self.yavg
        elif self.filt == 'fir':
            y = fir(self.yfilt_buf,self.coeffs,y)*(.22026)
        else:
            pass
        
        # Scan X
        self.xm.init(mode=self.OUT_PP,value=0)
        self.xp.init(mode=self.OUT_PP,value=1)
        self.ADC_ym = ADC(self.ym)
        self.yp.init(mode=self.IN)
         
        if avg_type == 'ten' or avg_type == 10:
            x0 = self.ADC_ym.read()
            x1 = self.ADC_ym.read()
            x2 = self.ADC_ym.read()
            x3 = self.ADC_ym.read()
            x4 = self.ADC_ym.read()
            x5 = self.ADC_ym.read()
            x6 = self.ADC_ym.read()
            x7 = self.ADC_ym.read()
            x8 = self.ADC_ym.read()
            x9 = self.ADC_ym.read()
            x = (x0 + x1 + x2 + x3 + x4 + x5 + x6 + x7 + x8 + x9)/10
        elif avg_type == 'five' or avg_type == 5:
            x0 = self.ADC_ym.read()
            x1 = self.ADC_ym.read()
            x2 = self.ADC_ym.read()
            x3 = self.ADC_ym.read()
            x4 = self.ADC_ym.read()
            x = (x0 + x1 + x2 + x3 + x4)/5
        else:
            x = self.ADC_ym.read()
            
        self.xavg = avg(self.xavg_buf,x)
            
        if (x-self.xavg) > 500 or (x-self.xavg) < 500:
            x = self.xavg
        
        if self.filt == 'avg':    
            x = self.xavg
        elif self.filt == 'fir':
            x = fir(self.xfilt_buf,self.coeffs,x)*(.22026)
        else:
            pass
        
        if mode == 'ctr':
            y = (self.conv_y*(y-self.cal_y[0]))-(self.y_ctr)
            x = (self.conv_x*(x-self.cal_x[0]))-(self.x_ctr)
        elif mode == 'org':
            y = (self.conv_y*(y-self.cal_y[0]))
            x = (self.conv_x*(x-self.cal_x[0]))
        else:
            pass
        
        if self.last_read != 1:
            self.last_read = 1
            
        return (x, y, z)
        
        
    @micropython.native
    def scan_old(self,mode='ctr',avg='none'):
        '''
        @brief Checks all RTP coordinates as fast as possible
        @details This method is the implementaion of my scan method before I added FIR
                 filtering or running averaging. I kept this mostly for my own reference.
                 It is not used in the ME 405 Term Project.
        
        @param mode  A string describing the output mode, either 'ctr' 'org' or 'count'
        @param avg   A string describing the batch averaging performed on each sample 'none' '5' or '10'
        @return A tuple of all three RTP coordinates ref'ed to center.
        '''
        
        # Optimizing Pin config based on last read coodinate
        if self.last_read == 1:
            self.yp.init(mode=self.OUT_PP,value=1)
            self.xp.init(mode=self.IN)
            
        elif self.last_read == 2:
            self.xm.init(mode=self.OUT_PP,value=0)
            self.ADC_ym = ADC(self.ym)
            self.xp.init(mode=self.IN)

        # Read Z
        # z = self.ADC_ym.read()
        
        # Assign boolean based on ADC value
        if self.ADC_ym.read() > 4000:
            return (0, 0, 0)
        else:
            
            # Scan Y
            self.ym.init(mode=self.OUT_PP,value=0)
            self.ADC_xm = ADC(self.xm)

            
            if avg == 'ten' or avg == 10:
                y0 = self.ADC_xm.read()
                y1 = self.ADC_xm.read()
                y2 = self.ADC_xm.read()
                y3 = self.ADC_xm.read()
                y4 = self.ADC_xm.read()
                y5 = self.ADC_xm.read()
                y6 = self.ADC_xm.read()
                y7 = self.ADC_xm.read()
                y8 = self.ADC_xm.read()
                y9 = self.ADC_xm.read()
                y = (y0 + y1 + y2 + y3 + y4 + y5 + y6 + y7 + y8 + y9)/10
            elif avg == 'five' or avg == 5:
                y0 = self.ADC_xm.read()
                y1 = self.ADC_xm.read()
                y2 = self.ADC_xm.read()
                y3 = self.ADC_xm.read()
                y4 = self.ADC_xm.read()
                y = (y0 + y1 + y2 + y3 + y4)/5
            else:
                y = self.ADC_xm.read()
            

           # Scan X
            self.xm.init(mode=self.OUT_PP,value=0)
            self.xp.init(mode=self.OUT_PP,value=1)

            self.ADC_ym = ADC(self.ym)
            self.yp.init(mode=self.IN)
            
            if avg == 'fir':
                pass
            elif avg == 'ten' or avg == 10:
                x0 = self.ADC_ym.read()
                x1 = self.ADC_ym.read()
                x2 = self.ADC_ym.read()
                x3 = self.ADC_ym.read()
                x4 = self.ADC_ym.read()
                x5 = self.ADC_ym.read()
                x6 = self.ADC_ym.read()
                x7 = self.ADC_ym.read()
                x8 = self.ADC_ym.read()
                x9 = self.ADC_ym.read()
                x = (x0 + x1 + x2 + x3 + x4 + x5 + x6 + x7 + x8 + x9)/10
            elif avg == 'five' or avg == 5:
                x0 = self.ADC_ym.read()
                x1 = self.ADC_ym.read()
                x2 = self.ADC_ym.read()
                x3 = self.ADC_ym.read()
                x4 = self.ADC_ym.read()
                x = (x0 + x1 + x2 + x3 + x4)/5
            else:
                x = self.ADC_ym.read()
                
            if mode == 'ctr':
                y = (self.conv_y*(y-self.cal_y[0]))-(self.y_ctr)
                x = (self.conv_x*(x-self.cal_x[0]))-(self.x_ctr)
            elif mode == 'org':
                y = (self.conv_y*(y-self.cal_y[0]))
                x = (self.conv_x*(x-self.cal_x[0]))
            else:
                pass
            
            if self.last_read != 1:
                self.last_read = 1
            
            return (x, y, 1)

    
if __name__ == '__main__':
    '''
    Test code for the RTP driver.
    '''
    
    ## The Nucleo pin connected to RTP xm
    PIN_xm = Pin(Pin.cpu.A0)
    
    ## The Nucleo pin connected to RTP xp
    PIN_xp = Pin(Pin.cpu.A1)
    
    ## The Nucleo pin connected to RTP ym
    PIN_ym = Pin(Pin.cpu.A6)
    
    ## The Nucleo pin connected to RTP yp
    PIN_yp = Pin(Pin.cpu.A7)
    
    ## The RTP objct
    RTP = RTP(PIN_xm,PIN_xp,PIN_ym,PIN_yp,(176,100),(88,50),debug=False)
    
    ## The read mode for testing (ctr obj count)
    mode = 'ctr'
    
    while True:
        
        ## The test type used (running or timed)
        test_type = input('Running test or time test? (r/t): ')
        
        ## The axis or collection of axes being sampled
        test_axis = input('Which axis would you like to test (X Y Z ALL): ') 
        
        try:
            
            if test_type == 't':
                if test_axis == 'X' or test_axis == 'x':
                    while True:
                        ## Test start time in ticks
                        start = utime.ticks_us()
                        ## x position
                        x = RTP.scan_x()
                        ## Test stop time in ticks
                        stop = utime.ticks_us()
                        ## Test run time in us
                        run_time = utime.ticks_diff(stop,start)
                        print('RTP read X = '+str(x)+' mm in '+str(run_time)+' us\n')
                        input('     Press enter to test again: ')
                        
                elif test_axis == 'y' or test_axis == 'Y':
                    while True:
                        start = utime.ticks_us()
                        ## y position
                        y = RTP.scan_y()
                        stop = utime.ticks_us()
                        run_time = utime.ticks_diff(stop,start)
                        print('RTP read Y = '+str(y)+' mm in '+str(run_time)+' us\n')
                        input('     Press enter to test again: ')
                        
                elif test_axis == 'Z' or test_axis == 'z':
                    while True:
                        start = utime.ticks_us()
                        ## z position
                        z = RTP.scan_z()
                        stop = utime.ticks_us()
                        run_time = utime.ticks_diff(stop,start)
                        print('RTP read Z = '+str(z)+' mm in '+str(run_time)+' us\n')
                        input('     Press enter to test again: ')
                        
                else:
                     while True:
                        start = utime.ticks_us()
                        ## Tuple containg x y and z positions
                        pos = RTP.scan()
                        stop = utime.ticks_us()
                        run_time = utime.ticks_diff(stop,start)
                        print('RTP read ({:},{:},{:}) [mm] in {:} us\n'.format(pos[0],pos[1],pos[2],run_time))
                        input('     Press enter to test again: ')
                    
            else:
                if test_axis == 'X' or test_axis == 'x':
                    print('Testing x...')
                    while True:
                        x = RTP.scan_x(mode=mode)
                        print('X: {:} mm'.format(x))
                        utime.sleep_ms(250)
                        
                        
                elif test_axis == 'y' or test_axis == 'Y':
                    print('Testing y...')
                    while True:
                        y = RTP.scan_y(mode=mode)
                        print('Y: {:} mm'.format(y))
                        utime.sleep_ms(250)
                        
                elif test_axis == 'Z' or test_axis == 'z':
                    print('Testing z...')
                    while True:
                        z = RTP.scan_z()
                        print('Z: {:} mm'.format(z))
                        
                else:
                    print('Testing all...')
                    while True:
                       pos = RTP.scan(mode=mode)
                       print('RTP read ({:},{:},{:}) [mm]'.format(pos[0],pos[1],pos[2]))
                       utime.sleep_ms(250)
            
        except:
            break








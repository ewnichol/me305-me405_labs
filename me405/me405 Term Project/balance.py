'''
@file       balance.py
@brief      This script balances the ME 405 term project platform
@detials    The script operates in two modes. The first
            occurs when there is no contact detected on the touch pannel. In this state, the system
            is controlled using the gains derrived from the platform only model, and the orientation
            measurements from the BNO055 IMU. This allows for repeatable and automatic absolute leveling. When in this mode, the time that the 
            platform is stable (no omega) is tracked. If there has been no motion for 50 controller cycles the encoders are zeroed.
            
            The second mode begins when contact is detected on the touch pannel. The controller
            waits for 100 cycles with positive contact before changing the control system to
            incorperate the ball position and velosity. This was done to allow the ball position
            signal to stabalize before using it for control. The FIR filter and averaging scheme
            that I applied to reduce noise and accidental loss of contact also acts on the 
            real contact changes. The easiest way to handle this was to add an artificial settling
            time before changing control state. In this mode, platform orientation data is
            read from the encoders. This allows for much faster controller speeds, and as a result, better performance.
            
            The source code for this file can be found here: 
                
            https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me405/me405%20Term%20Project/balance.py

@author     Enoch Nicholson
@date       March 11, 2021
'''

import pyb, utime
from machine import I2C, Pin
from pyb import UART

from enc_driver import EncDriver
from m_driver import MotorDriver
from rtp_driver import RTP
from bno055 import BNO055
from fsfb_control import FSFB

'''
---- Encoder Initialization ----
'''

## The pin object connected to Encoder 1's channel 1
pin_E1CH1 = pyb.Pin(pyb.Pin.cpu.B6)

## The pin object connected to Encoder 1's channel 2
pin_E1CH2 = pyb.Pin(pyb.Pin.cpu.B7)

## An encoder object representing the encoder attached to Motor 1
e1 = EncDriver('E1', pin_E1CH1, 1, pin_E1CH2, 2, 4, 1000, False)

## The pin object connected to Encoder 2's channel 1
pin_E2CH1 = pyb.Pin(pyb.Pin.cpu.C6)

## The pin object connected to Encoder 2's channel 2
pin_E2CH2 = pyb.Pin(pyb.Pin.cpu.C7)

## An encoder object representing the encoder attached to Motor 2
e2 = EncDriver('E2', pin_E2CH1, 1, pin_E2CH2, 2, 8, 1000, False)

## Conversion factor for encoder angle to platform angle
enc_conv = -60/110

'''
---- Motor Initialization ----
'''

## The pin object used for the H-bridge sleep pin
pin_nSLEEP = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
pin_nSLEEP.low()

## The pin object used for the motor driver fault pin. Here the user-button is used
# to simulate a fault for testing behavior.
pin_nFAULT = pyb.Pin(pyb.Pin.board.PB2, pyb.Pin.IN)

## The pin object used for the Motor 1's H-bridge input 1
pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4)

## The pin object used for the Motor 1's H-bridge input 2
pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5)

## The pin object used for the Motor 2's H-bridge input 1
pin_IN3 = pyb.Pin(pyb.Pin.cpu.B0)

## The pin object used for the Motor 2's H-bridge input 2
pin_IN4 = pyb.Pin(pyb.Pin.cpu.B1)

## The timer object used for PWM generation
tim = pyb.Timer(3,freq=20000);

## A motor object passing in the pins and timer
mA = MotorDriver(pin_nSLEEP, pin_nFAULT, pin_IN1, 1, pin_IN2, 2, tim, new_db=2.5, dead_band=(-45,45), debug=False, primary=True)

## A second motor object passing in the pins and timer
mB = MotorDriver(pin_nSLEEP, pin_nFAULT, pin_IN3, 3, pin_IN4, 4, tim, new_db=2.5, dead_band=(-45,45))

# Enable the motors
mA.enable()

'''
---- Touch Pannel Initialization ----
'''

## The Nucleo pin connected to RTP xm
PIN_xm = Pin(Pin.cpu.A0)

## The Nucleo pin connected to RTP xp
PIN_xp = Pin(Pin.cpu.A1)

## The Nucleo pin connected to RTP ym
PIN_ym = Pin(Pin.cpu.A6)

## The Nucleo pin connected to RTP yp
PIN_yp = Pin(Pin.cpu.A7)

## The RTP objct
RTP = RTP(PIN_xm,PIN_xp,PIN_ym,PIN_yp,(176,100),(88,50),filt='fir',debug=False)


'''
---- IMU Initialization ----
'''

## Initializes the I2C bus
i2c = I2C(-1, scl=Pin('PB8'), sda=Pin('PB9'), freq=115200)

## Initializes the bno055 driver using the I2C object
imu = BNO055(i2c)

'''
---- Controller Initialization ----
'''

## Y axis reduced system gains
Kplaty = [-14.4129,	-0.00223331]

## X axis reduced system gains
Kplatx = [-8.4129,	-0.00223331]

## Y axis full system gains
Ksysy = [-14.17333,	-14.4129,	-1.254464,	-0.00223331]

## X axis full system gains
Ksysx = [-14.17333,	-10.4129,	-2.54464,	-0.00223331]

# Ksysy = [-10.1618,	-14.0758,	-11.8285,	-1.19342]

# Ksysx = [-10.1618,	-14.0758,	-11.8285,	-1.19342]

## Motor nominal voltage [V]
Vdc = 18

## Motor torque constant [Nm/A]
Kt = 13.8/10**3

## Motor terminal resistance [Ohms]
R = 2.21

## Torque to PWM duty conversion factor
conv = R/(Kt*Vdc)

## FSFB controller for the y-axis of the platform only system 
plat_y = FSFB('KPY', Kplaty,100, conv=conv)

## FSFB controller for the x-axis of the platform only system 
plat_x = FSFB('KPX', Kplatx, 100, conv=conv)

## FSFB controller for the y-axis of the full system 
sys_y = FSFB('KY', Ksysy, 100, conv=conv)

## FSFB controller for the x-axis of the full system 
sys_x = FSFB('KX', Ksysx, 100, conv=conv)

## PWM duty sent to motor A controlling platform x-axis
x_duty = 0

## PWM duty sent to motor B controlling platform y-axis
y_duty = 0

'''
---- Balancing script ----
'''

## Hand balancing limits in degrees
bal_lim = 20

## Number of runs
runs = 1

## Start time of the data collection
start_time = utime.ticks_us()

## Last time of the data collection
last_time = start_time

## Current time of the data collection
curr_time = start_time

## Elapsed time
el_time = curr_time - start_time

## Desired controller time step
dt = 5*10**3
# dt = 1

## Time of next run
next_time = start_time

## Measured controller time interval
dt_meas = 1

## Average measued time interval
dt_avg = 1

## UART communication line for debugging
uart = UART(2)

## Ball Position from the touch pannel (x,y,z) in [m]
pos = (0,0,0)

## Previous ball Position from the touch pannel (x,y,z) in [m]
prev_pos = (0,0,0)

## Ball Velocity from the touch pannel in [m/s]
vel = (0,0,0)

## Platform angle [rad]
thta = (0,0,0)

## Platform angular velocity [rad/s]
omga = (0,0,0)

## A counter that increments when the platform is stable
steady_count = 0

## A counter that increments when the ball is off of the platform
contact_count = 0

try:
        
    input('Press Enter to zero the platform: ')
    while True:
        
        curr_time = utime.ticks_us()
        # ----
        if utime.ticks_diff(curr_time,next_time) > 0:
    
            if runs == 1:
                ## Elapsed time offset, recorded on the first run
                offset = curr_time
            
            el_time = utime.ticks_diff(curr_time, offset)/1e6
            
            dt_meas = utime.ticks_diff(curr_time,last_time)/10**6
            # print(dt_meas)
            
            pos = RTP.scan(avg_type=10)
            vel = ((pos[0]-prev_pos[0])/dt_meas,(pos[1]-prev_pos[1])/dt_meas,0)
            
            if (vel[0]**2+vel[1]**2)**.5 > 400:
                vel = (0,0,0)
            
            # A counter tracking contact
            if pos[2] == 1:
                contact_count += 1
                if contact_count > 200:
                    contact_count = 200
            else:
                contact_count -= 1
                if contact_count < 0:
                    contact_count = 0
            
            # If no contact
            if contact_count < 100:
                thta=imu.euler()
                omga=imu.gyro() 
                
                if (thta[1]**2+thta[2]**2)**.5 > bal_lim*3.14159/180:
                    mA.disable()
                    
                    print('Please level the platform by hand...')
                    print('     The controller will take over when the platform is within '+str(bal_lim)+' deg of level')
                    
                    while True:
                        thta=imu.euler()
                        omga=imu.gyro()
                        if (thta[1]**2+thta[2]**2)**.5 < bal_lim*3.14159/180:
                            print('Now Balancing')
                            mA.enable()
                            break
                        
                elif steady_count == 50 and (thta[1]**2+thta[2]**2)**.5 < 1*3.14159/180:
                    
                    e1.set_position()
                    e2.set_position()            
                        
                else:
                    
                    x_duty = plat_y.feedback([thta[1],omga[1]])
                    y_duty = plat_x.feedback([thta[2],omga[2]])
                    
                    # x_duty = sys_y.feedback([pos[1]/1000,thta[1],vel[1]/1000,omga[1]])
                    # y_duty = sys_x.feedback([pos[0]/1000,thta[2],vel[0]/1000,omga[2]])
                    
                    mA.set_duty(x_duty)
                    mB.set_duty(y_duty)
                    
                    if runs == 2:
                        dt_avg = dt_meas
                    else:
                        dt_avg = (dt_avg+dt_meas)/2
                        
                    runs += 1
                
                # A counter tracking steadyness
                if (omga[1]**2+omga[2]**2)**.5 < .01:
                    steady_count += 1
                else:
                    steady_count = 0
              
            # If contact
            else:
                # Encoders used for orientation
                thta = (0,-e1.get_position('rad')*enc_conv,-e2.get_position('rad')*enc_conv)
                omga = (0,-e1.get_delta('rad')*enc_conv/dt_meas,-e2.get_delta('rad')*enc_conv/dt_meas)
                
                if (thta[1]**2+thta[2]**2)**.5 > bal_lim*3.14159/180:
                    mA.disable()   
                    print('Please level the platform by hand...')
                    print('     The controller will take over when the platform is within '+str(bal_lim)+' deg of level')
                    
                    while True:
                        thta=imu.euler()
                        omga=imu.gyro()
                        if (thta[1]**2+thta[2]**2)**.5 < bal_lim*3.14159/180:
                            print('Now Balancing')
                            mA.enable()
                            break
    
                else:
                    
                    x_duty = sys_y.feedback([pos[1]/1000,thta[1],vel[1]/1000,omga[1]])
                    y_duty = sys_x.feedback([pos[0]/1000,thta[2],vel[0]/1000,omga[2]])
                    
                    mA.set_duty(x_duty)
                    mB.set_duty(y_duty)
                    
                    if runs == 2:
                        dt_avg = dt_meas
                    else:
                        dt_avg = (dt_avg+dt_meas)/2
                        
                    runs += 1
            
            
            prev_pos = pos
            e1.update()
            e2.update()
        
                
            # For general purpose debugging
            # uart.write('{:.2f},{:.2f},{:6f},{:.4f},{:.4f},0,{:.4f},{:.4f},0\r\n'.format(x_duty,y_duty,dt_meas,thta[1]*180/3.14159,thta[2]*180/3.14159,pos[0],pos[1]))
            
            # For comparing IMU to encoders
            # uart.write('{:.2f},{:.2f},0,{:.4f},{:.4f},0,{:.4f},{:.4f},0\r\n'.format(x_duty,y_duty,thta[1]*180/3.14159,thta[2]*180/3.14159,thta1[1]*180/3.14159,thta1[2]*180/3.14159))
            
            # For checking velocity measurements
            # uart.write('{:.2f},{:.2f},{:6f},{:.4f},{:.4f},0,{:.4f},{:.4f},0\r\n'.format(x_duty,y_duty,dt_meas,vel[0],vel[1],pos[0],pos[1]))
    
            # For checking x state vector
            # uart.write('{:.2f},0,{:6f},{:.4f},{:.4f},0,{:.4f},{:.4f},0\r\n'.format(x_duty,dt_meas,pos[0],vel[0],thta[1],omga[1]))
    
            
            last_time = curr_time
            next_time = utime.ticks_add(curr_time,dt)
            
        else:
            pass

        
    
except: 
    mA.disable()
    print('Average time interval: '+str(dt_avg)+' us')


    


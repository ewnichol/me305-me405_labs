''' 
@file cl_driver.py

@author E. Nicholson

@date March 11, 2021

@brief      This file contains a class based implementation of a full state feedback regulator
           
@details    The source code for this file can be found here: 
    
            https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me405/me405%20Term%20Project/fsfb_control.py
'''

class FSFB:
    ''' 
    @brief  This class implements a full state feedback regulator.
    
    @details    This controller is initialized by passing in a feedback gain matrix.
                The size of this matrix configures the controller. This class currently
                has one method, feedback(), which takes a state vector and uses
                the internal gain matrix to compute the actuation signal.
    
    '''

    def __init__(self, ID, K, sat_lim, dead_band=(0,0), conv=1):
        '''
        @brief  Creates a FSFB regulator object based on a given gain matrix and saturation limits
        @param ID           The ID string for the object
        @param K            FSFB gain matrix
        @param sat_lim      Saturation limits for the system
        @param dead_band    A tuple describing the actuator deadband
        @param conv         Control signal conversion factor. Use if output does not match actuator input
        '''
        
        print ('Creating FSFB regulator ' + ID + ' with K of size ' + str(len(K)))
        
        # ---- General Setup ----
        
        ## The unique ID string for the object, used for trace output
        self.ID = ID
        
        ## The controller's gain matrix
        self.K = K
        
        ## The controller's integral gain
        self.n = len(K)
        
        ## The controller's output saturation limits
        self.sat_lim = sat_lim
        
        ## The actuator dead_band limits
        self.dead_band = dead_band
        
        ## The conversion factor for the control signal should have units of the form,
        #   [actuator input]/[controller output]. For ME 405 our units are [PWM%]/[Nm]
        self.conv = conv
        
        ## The control signal
        self.ctrl_signal = 0
    
    def feedback(self, x, dt=0):
        '''
        @brief Computes actuation values using current state vector
        @param x        The state vector
        @return ctrl_signal The actuation signal
        '''
                
        if len(x) == self.n:
        
            ctrl_signal = 0
            
            for i in range(self.n):
                ctrl_signal += self.K[i]*x[i]
            
            # Here the controll signal is placed into the desired units.
            ctrl_signal = ctrl_signal*self.conv
                    
            if ctrl_signal > self.sat_lim:
                ctrl_signal = self.sat_lim
            elif ctrl_signal < -1*self.sat_lim:
                ctrl_signal = -1*self.sat_lim
                
            self.ctrl_signal = ctrl_signal
            
            return ctrl_signal
        
        else:
            print('Invalid state vector dimesion')
            return 0
    
    def get_K(self):
        '''
        @brief Returns the current gain of the controller
        @returns Current gain matrix
        '''
        return self.K
    
    def set_K(self, K):
        '''
        @brief      Sets the gain of the controller
        @param KP   New proportional gain
        '''
        self.K = K
        
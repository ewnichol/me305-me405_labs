'''
@file rtp_filter_testing
@brief
@details
'''

from rtp_driver import RTP
import pyb
import utime
from array import array


## The Nucleo pin connected to RTP xm
PIN_xm = pyb.Pin(pyb.Pin.cpu.A0)

## The Nucleo pin connected to RTP xp
PIN_xp = pyb.Pin(pyb.Pin.cpu.A1)

## The Nucleo pin connected to RTP ym
PIN_ym = pyb.Pin(pyb.Pin.cpu.A6)

## The Nucleo pin connected to RTP yp
PIN_yp = pyb.Pin(pyb.Pin.cpu.A7)

## The RTP objct
RTP = RTP(PIN_xm,PIN_xp,PIN_ym,PIN_yp,(176,100),(88,50),debug=False)

## The read mode for testing (ctr obj count)
mode = 'ctr'

# ---- Sample Timing ----

## Current Time step
Ts = utime.ticks_us()

## Desired time step in [us]
dt = 10**4

## Next sample time in [us]
Tns = utime.ticks_add(Ts,dt)

## Sample counter
n = 0

## Time of the first run for offsetting time values
offset = 0

## End time in seconds
end_time = 15

# ---- Data Storage ----

arr_init = [0]*int(end_time/(dt/10**6))

el = array('f',arr_init)
samp_idx = array('f',arr_init)
x_raw = array('f',arr_init)
y_raw = array('f',arr_init)
z = array('f',arr_init)
    
input('Press enter to start: ')

print('Collecting Data...')

# Collect data
while el > end_time:
   
    Ts = utime.ticks_us()

    if utime.ticks_diff(Ts,Tns) > 0:
        print('RUN')
        if n == 0:
            offset = Ts
        
        el[n] = (Ts-offset)/10**6
        samp_idx[n] = n
        
        pos = RTP.scan()
        
        x_raw[n] = pos[0]
        y_raw[n] = pos[1]
        
        z[n] = pos[2]
        
        Tns = utime.ticks_add(Ts,dt)
        print(n)
        print(type(n))
        n += 1
        
        # if el > end_time:
            # break
    else:
        print('PASS')
            
            
print('End of Collection...')

# Creates a csv file to append data to
with open ('rtpData.csv', 'w') as data:
    data.write('IDX,T,X_r,Y_r,Z\n')
    for i in samp_idx:
        data.write('{:},{:},{:},{:},{:}\n'.format(samp_idx[i], el[i], x_raw[i], y_raw[i], z[i]))

print('CSV Created!')
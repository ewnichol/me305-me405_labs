''' 
@file m_driver.py

@author E. Nicholson

@date November 10, 2020

@brief      This file contains a motor driver and code used to test its functionality

@details    The class contained within this file is a motor driver. It connects
            to four Nucleo pins bound to an H-Bridge motor driver and configures
            them for PWM using a timer. The conditional at the end of the file
            allows test code to run if this file is used stand alone. The test
            code allows the user to run a variety of debugging tests on the driver.
            
            The source code for this file can be found here: 
    
            https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me405/me405%20Term%20Project/m_driver.py
'''

import pyb
import utime
import micropython

# Preallocation of memory incase the callback function throws an exception. 
#   This allows the user to view the exception.
micropython.alloc_emergency_exception_buf(100)

class MotorDriver:
    ''' 
    @brief  A motor driver designed to work with an H-bridge connected to a Nucleo L476RG running Micropython.
    
    @details    This motor driver takes four pyb.Pin objects, two timer channel variables,
                and a pyb.Timer object. It uses these to configure a sleep pin, a fault pin
                and two PWM signal generators. The H-Bridge motor driver is
                controlled using a Pulse 1 Side PWM scheme. The driver software
                uses logic to interporate the given duty command into the correct
                PWM percentage and direction. The init parameters dead_band and new_db
                are used to set up dead compensation. If used, this driver will
                scale any requested duty to avoid falling into the motor's natural friction
                dead_band. To avoid oscilations when using deadband compensation in closed loop control,
                the new_db parameter can be used to set up a new, smaller dead_band that allows the
                control system to settle out. The driver also has the ability to enable and disable motor motion.
    '''

    def __init__ (self, nSLEEP_pin, nfault_pin, IN1_pin, IN1_ch, IN2_pin, IN2_ch, timer, dead_band=(0,0), new_db=0, debug=False, primary=False):
        '''
        @brief  Creates a motor driver by initializing GPIO pins and turning the motor off for safety
        @param nSLEEP_pin   A pyb.Pin object to use as the enable pin.
        @param nfault_pin   A pyb.Pin object used to connect to the motor fault pin, must be configured as an input
        @param IN1_pin      A pyb.Pin object to use as the input to half bridge 1.
        @param IN1_ch       The timer channel number to be used for IN1_pin.
        @param IN2_pin      A pyb.Pin object to use as the input to half bridge 2.
        @param IN2_ch       The timer channel number to be used for IN2_pin.
        @param timer        A pyb.Timer object to use for PWM generation on IN1_pin and IN2_pin.
        @param dead_band    A tuple representing the motor's deadband, leave as (0,0) if no dead band compensation is desired
        @param new_db       A number representing a new symetrical dead band around zero used to stabalize control signal
        @param debug        A boolean used to enable debug messages
        @param primary      A boolean used to set if an external interrupt should be created to listen for a fault.
        '''
        
        ##  A boolean used to enable debug messages
        self.debug = debug
        
        ## The pin used to enable / disable the motor
        self.enable_pin = nSLEEP_pin
        self.disable()
        
        ## The pin used to detect a fault in motor operations
        self.fault_pin = nfault_pin
        
        ## A Boolean indicating if the motor driver chip is asserting a fault
        self.mot_fault = False
        
        if primary == True:
            ## Motor fault ISR object
            self.MotFault = pyb.ExtInt(nfault_pin, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_NONE, self.catch_fault)
        
        ## A pyb.Timer.channel object configured to drive half bridge 1 with PWM 
        # using the given pyb.Timer, pyb.Pin, and channel number
        self.tch1 = timer.channel(IN1_ch, pyb.Timer.PWM, pin=IN1_pin)
        
        ## A pyb.Timer.channel object configured to drive half bridge 2 with PWM 
        # using the given pyb.Timer, pyb.Pin, and channel number
        self.tch2 = timer.channel(IN2_ch, pyb.Timer.PWM, pin=IN2_pin)
        
        ## A value used to create an artificial deadband about zero
        self.new_db = new_db
        
        ## A tuple representing the deadband of the BDC motor being used. This must be tested and set acordingly
        #   Leaving db as (0,0) turns off dead band compensation
        self.db = dead_band
        
        if self.debug == True:
            print ('Creating a motor driver')
    
    def enable (self):
        '''
        @brief enables the motors by setting the enable pin to high
        '''
        if self.debug:
            print ('Enabling Motor')
            
        if self.mot_fault == True:
            self.check_fault()
        else:
            self.enable_pin.high()
            utime.sleep_us(100)
            self.MotFault.enable()
    
    def disable (self):
        '''
        @brief disables the motors by setting the enable pin to low
        '''
        if self.debug:
            print ('Disabling Motor')
        self.enable_pin.low()
    
    def set_duty (self, duty):
        '''
        @brief      Sets the duty for the motor
        @details    This method sets the duty cycle to be sent to the motor to 
                    the given level. Positive values cause effort in one 
                    direction, negative values in the opposite direction.        
        @param duty     A signed integer holding the duty cycle of the PWM signal sent to the motor 
        '''
        
        if self.mot_fault == True:
            self.check_fault()
        else:
        
            self.tch1.pulse_width_percent(0)
            self.tch2.pulse_width_percent(0)
            
            try:
                if duty > self.new_db:
                    duty = self.db[1]+(duty/100)*(100-self.db[1])
                    if duty > 100:
                        duty = 100
                    self.tch1.pulse_width_percent(duty)
                elif duty < -self.new_db:
                    duty = self.db[0]+(duty/-100)*(-100-self.db[0])
                    if duty < -100:
                        duty = -100
                    self.tch2.pulse_width_percent(-1*duty)
                else:
                    duty = 0
            except:
                print('Invalid duty value')
            
    def check_fault(self):
        '''
        @brief This tests a fault state in the driver
        @details When the driver fault attribute is set to True, trying to either
                 enable the motor or set its duty will trigger a fault check.
                 Here the value of the nfault pin is checked, if the fault has cleared,
                 the driver is unlocked, if it has not, the user is prompted to
                 act. To prevent the software latch from sticking, the fault ISR
                 is disabled to allow the motor to power on without triggering
                 another momentary fault.
        '''
        
        if self.fault_pin.value() == 1:
            self.mot_fault = False
            self.MotFault.disable()
            self.enable()
            print('The fault has been cleared')
        else:
            # input('''DRV8847 has indicated a fault, and the diver has been disabled...  
            #       \n     Check the system and press enter to power cycle and retry: 
            #       ''')
            print('''DRV8847 has indicated a fault, and the diver has been disabled...  
                  \n     Check the system and press enter to power cycle and retry: 
                  ''')
            self.enable()
            while self.fault_pin.value() == 0:
                # input('The fault persists...\n    Check the system and press enter to retry: ')
                print('The fault persists...\n    Check the system and press enter to retry: ')
                self.disable()
                self.enable()
        
    def catch_fault(self,ISR_SOURCE):
        '''
        @brief A callback fuction that triggers on a motor fault condition
        @param ISR_SOURCE The line that triggered the ISR
        '''
        self.disable()
        self.mot_fault = True
        

if __name__ == '__main__':
    '''
    Test program for motor driver using ME 405 hardware.
    '''
    


    ## The pin object used for the H-bridge sleep pin
    pin_nSLEEP = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
    pin_nSLEEP.low()
    
    ## The pin object used for the motor driver fault pin. Here the user-button is used
    # to simulate a fault for testing behavior.
    pin_nFAULT = pyb.Pin(pyb.Pin.board.PC13, pyb.Pin.IN)
    
    ## The pin object used for the Motor 1's H-bridge input 1
    pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4)
    
    ## The pin object used for the Motor 1's H-bridge input 2
    pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5)
    
    ## The pin object used for the Motor 2's H-bridge input 1
    pin_IN3 = pyb.Pin(pyb.Pin.cpu.B0)
    
    ## The pin object used for the Motor 2's H-bridge input 2
    pin_IN4 = pyb.Pin(pyb.Pin.cpu.B1)
    
    ## The timer object used for PWM generation
    tim = pyb.Timer(3,freq=20000);
    
    ## A motor object passing in the pins and timer
    m1 = MotorDriver(pin_nSLEEP, pin_nFAULT, pin_IN1, 1, pin_IN2, 2, tim, dead_band=(-28,28), debug=True, primary=True)
    
    ## A second motor object passing in the pins and timer
    m2 = MotorDriver(pin_nSLEEP, pin_nFAULT, pin_IN3, 3, pin_IN4, 4, tim, dead_band=(-25,25))
    
    # Enable the motors
    m1.enable()
        
    while True:
        
        ## The test type used (running or timed)
        test_type = input('Running test, time test, motion test, fault test? (r/t/m/f): ')
        
        ## The motor selection for testing
        test_mot = input('Which motor? (A/B): ')
        
        # try:
        
        if test_type == 't':
            print('Timed Test...')
            if test_mot == 'B' or test_mot == 'b':
                print('Testing Motor B set duty...')
                while True:
                   start = utime.ticks_us()
                   m2.set_duty(40)
                   stop = utime.ticks_us()
                   pyb.delay(1000)
                   m2.set_duty(0)
                   run_time = utime.ticks_diff(stop,start)
                   print('MOTB set duty {:} us\n'.format(run_time))
                   
                   input('     Press enter to test again: ')
            else:
                print('Testing Motor A set duty...')
                while True:
                   start = utime.ticks_us()
                   m1.set_duty(40)
                   stop = utime.ticks_us()
                   pyb.delay(1000)
                   m1.set_duty(0)
                   run_time = utime.ticks_diff(stop,start)
                   print('MOTA set duty {:} us\n'.format(run_time))
                   
                   input('     Press enter to test again: ')
        elif test_type == 'f':
            print('Fault Test... \nPress or hold the blue button to simulate a fault')
            
            try:
                while True:
                    m1.set_duty(40)
                    m2.set_duty(40)
                    pyb.delay(1000)
            except:
                m1.set_duty(0)
                m2.set_duty(0)
            
        elif test_type == 'r':
            print('Running Test...')
            if test_mot == 'B' or test_mot == 'b':
                print('Testing Motor B...')
                while True:
                    duty = input('Enter a duty percentage (0-100) \n')
                    m2.set_duty(int(duty))
            else:
                print('Testing Motor A...')
                while True:
                    duty = input('Enter a duty percentage (0-100) \n')
                    m1.set_duty(int(duty))
        else:
            print('Motion Test...')
            if test_mot == 'B' or test_mot =='b':
                print('Testing Motor B...')

                while True:
                    input('Ready... press enter to continue \n')
                    print('Fwd')
                    m2.set_duty(40)
                    
                    pyb.delay(1000)
                    
                    print('Stop')
                    m2.set_duty(0)
                    
                    pyb.delay(1000)
                
                    print('Rev')
                    m2.set_duty(-40)
                    
                    pyb.delay(1000)
                    
                    print('Stop')
                    m2.set_duty(0)
            else:
                print('Testing Motor A...')
                while True:
                    input('Ready... press enter to continue \n')
                    print('Fwd')
                    m1.set_duty(40)
                    
                    pyb.delay(1000)
                    
                    print('Stop')
                    m1.set_duty(0)
                    
                    pyb.delay(1000)
                
                    print('Rev')
                    m1.set_duty(-40)
                    
                    pyb.delay(1000)
                    
                    print('Stop')
                    m1.set_duty(0)
                    
        # except:
        #     break
    

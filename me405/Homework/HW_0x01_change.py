'''
@file HW_0x00_change.py
@brief A file containing a function that computes change given a price and payment.
@author Enoch Nicholson
@date January 12, 2021
@details This file contains a function that computes change given an item price
         and a tuple describing the denomination used for payment. This file also
         contains test code that only runs when this file is used directly.
         
         The source code for this file can be found here: 
             
         https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me405/Homework/HW_0x01_change.py
'''

import numpy as np

def getChange(price, payment):
    '''
    @brief A function that computes the correct change given a price and a payment.
    @author Enoch Nicholson
    @date January 12, 2021
    @details This function accepts a price in interger cents and a tuple that
             represents the numbers of each common denomination used. The tuple
             is organized using the following mapping:
                 0 = penny
                 1 = nickle
                 2 = dime
                 3 = quarter
                 4 = dollar bill
                 5 = five dollar bill
                 6 = ten dollar bill
                 7 = twenty dollar bill
            These inputs are used to calculate the correct change for the purchase
            using the lowest number of returned coins/bills. The function returns
            a tuple using the same maping that describes the change.
    @param price Price of the item being purchased.
    @param payment A tuple describing the coins/bills used for the purchase.
    @return A tuple describing the denominations of the coins/bills returned.
    '''
    
    pay_arr = np.array(payment)
    denom_arr = np.array([1,5,10,25,100,500,1000,2000])
    
    pay_bal = np.sum(pay_arr*denom_arr)
    
    if price <= pay_bal:
        change_bal = pay_bal-price
        change = np.array([0,0,0,0,0,0,0,0])
        change_idx = 7
        
        for val in list(reversed(denom_arr)):
            n = np.floor(change_bal/val)
            change[change_idx] = n
            change_bal -= n*val
            change_idx -= 1
            # print(change_bal)
        change = tuple(change)
    else:
        change = None
        
    return change
    
if __name__ == '__main__':
    print(getChange(153,(5,0,0,0,0,0,0,0)))
    print(getChange(1112,(2,2,1,1,1,2,1,0)))
    print(getChange(2003,(5,1,0,0,0,0,0,1)))
    print(getChange(153.6,(5,0,0,0,0,0,0,0)))
'''
@file LAB_0x03_FSM.py

@author E. Nicholson

@date October 12, 2020

@brief      This file contains encoder and User interface task finite-state-machines.

@details    The two finite-state-machines in this file implement both tasks required
            by Lab 0x03. The first machine monitors, and updates an encoder object.
            The second machine creates a simple interface that allows a user to request 
            information about the current state of the encoder, and to zero the
            encoder's position.
            
            The source code for this file can be found here: 
    
            https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305%20Lab%200x03/LAB_0x03_FSM.py 
'''
from pyb import UART
import utime
# import time


class encFSM:
    '''
    @brief      A finite-state-machine that runs an encoder driver.
    @details    This class implements a finite state machine to 'update' an encoder
                object that is bound to actual hardware. It is a single state machine
                that always self transitions.
                
    @image html lab0x03_encoder_FSM_stateflow.png
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT          = 0    
    
    ## Constant defining State 1 - Update
    S1_UPDATE        = 1

    def __init__(self, ID, encoder, ref_rate, debug):
        '''
        @brief              Creates an encoder task object
        @param ID           A string identifying the object for traceing output.
        @param encoder      The encoder that is bound to the encoder FSM.
        @param ref_rate     The refresh rate of the encoder.
        @param debug        A boolean that toggles the trace output on or off
        '''
        
        # ---- General Setup ----
        
        ##  The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ##  The unique ID string for the object, used for trace output
        self.ID = ID
        
        ## A boolean that toggles the trace output to the command line
        self.debug = debug
        
        ##  The encoder objct that is maintained by this finite state machine
        self.encoder = encoder
        
        ##  The refresh rate for the encoder position in microseconds
        self.ref_rate = ref_rate        

        # ---- Timing Configuration ----

        ##  The timestamp for the initialization of the object in microseconds
        self.start_time = utime.ticks_us()
        # self.start_time = time.time()
               
        ##  The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.ref_rate)
        # self.next_time = self.start_time + self.ref_rate
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        @details    This FSM uses the encoder class from encoder.py to update
                    the encoder object it handes.
        '''
        ## The current time at the start of an update
        self.curr_time = utime.ticks_us()
        # self.curr_time = time.time()
           
        if utime.ticks_diff(self.curr_time, self.next_time) > 0:
        # if  self.curr_time > self.next_time:
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                if self.debug:
                    print('INIT Encoder Task')    
                self.transitionTo(self.S1_UPDATE)
            
            elif(self.state == self.S1_UPDATE):
                # Run State 1 Code   
                self.encoder.update()
            
            else:
                # Invalid state code (error handling)
                pass
            
            self.next_time = utime.ticks_add(self.next_time, self.ref_rate)
            # self.next_time += self.ref_rate
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
class UIFSM:
    '''
    @brief      A finite-state-machine that creates a user interface for an encoder.
    @details    This class implements a finite state machine to provide access to
                the position data within an encoder object. It also provided console acess
                to a 'zero position' command.
                
    @image html lab0x03_UI_FSM_stateflow.png
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT          = 0    
    
    ## Constant defining State 1 - Run
    S1_RUN        = 1

    def __init__(self, ID, UART_ID, ref_rate, debug):
        '''
        @brief              Creates an user interface object
        @param ID           A string identifying the object for traceing output.
        @param UART_ID      The UART ID number that will be used for serial communication
        @param ref_rate     The refresh rate of the UI task
        @param debug        A boolean that toggles the trace output on or off
        '''
        
        # ---- General Setup ----
        
        ##  The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ##  The unique ID string for the object, used for trace output
        self.ID = ID
        
        ## A boolean that toggles the trace output to the command line
        self.debug = debug
                
        ## UART object for handling simple serial communication
        self.uart = UART(UART_ID)
        
        ##  The refresh rate for the encoder position in microseconds
        self.ref_rate = ref_rate      

        # ---- Timing Configuration ----

        ##  The timestamp for the initialization of the object in microseconds
        self.start_time = utime.ticks_us()
        # self.start_time = time.time()
               
        ##  The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.ref_rate)
        # self.next_time = self.start_time + self.ref_rate
        
    def run(self, zero, pos, delta):
        '''
        @brief      Runs one iteration of the UI task
        @details    This FSM interfaces with the encoder task using the three
                    non-self arguments passed to run. The argument, zero, is
                    the set_position method from the encoder object contained
                    within the encoder FSM that this UI is connected to. The pos
                    and delta arguments represent the get_position and get_delta
                    methods respectivly. By passing these three metods to the UI,
                    they can be used in this task to retrieve data from the encoder
                    driver.
        '''
        ## The current time at the start of an update
        self.curr_time = utime.ticks_us()
        # self.curr_time = time.time()
           
        if utime.ticks_diff(self.curr_time, self.next_time) > 0:
        # if  self.curr_time > self.next_time:
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                if self.debug:
                    print('INIT User Interface Task')
                self.show_options()
                self.transitionTo(self.S1_RUN)
            
            elif(self.state == self.S1_RUN):
                # Run State 1 Code   
                if self.uart.any():
                    char = self.uart.read()
                    if char == b'z':
                        zero()
                        print('     Encoder zeroed \n')
                        self.show_options()
                              
                    elif char == b'p':
                        print('     Position: ' + str(pos()) + '\n')
                        self.show_options()
                        
                    elif char == b'd':
                        print('     Delta: ' + str(delta()) + '\n')
                        self.show_options()
                        
                    else:
                        print('Invalid' + '\n')
                        self.show_options()                        
                                
            else:
                # Invalid state code (error handling)
                pass
            
            self.next_time = utime.ticks_add(self.next_time, self.ref_rate)
            # self.next_time += self.ref_rate
    
    def show_options(self):
        '''
        @brief      This method prints the user interface menu.
        '''
        print('Encoder Menu: \n' +
              'Please Type one of the following commands \n' +
              'z -- Zero the encoder \n' +
              'p -- Print encoder position \n' +
              'd -- Print encoder delta \n')
           
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState

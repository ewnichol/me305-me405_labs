'''
@file LAB_0x03_main.py

@author E. Nicholson

@date October 12, 2020

@brief      This file configures and runs both tasks for Lab 0x03.

@details    This file initializes basic hardware (Pins), and uses them to
            generate an encoder driver object. This object is then passed to
            an encoder task that continually updates the driver. A user 
            interface task is also created. This task enables the retrieval of
            data from the encoder driver using the PUTTY comunication terminal.
            Currently both tasks use a refresh rate of 1 ms.
            
            The source code for this file can be found here: 
    
            https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305%20Lab%200x03/LAB_0x03_main.py 
'''

from encoder import encoder
from LAB_0x03_FSM import encFSM
from LAB_0x03_FSM import UIFSM
import pyb

# Configuration 

## Configures the pin for encoder channel A
pinA6 = pyb.Pin(pyb.Pin.cpu.A6)

## Configures the pin for encoder channel B
pinA7 = pyb.Pin(pyb.Pin.cpu.A7)
# pinA6 = 'Pin A6'
# pinA7 = 'Pin A7'

## Represents an encoder object. This object is created by passing two pins and a timer number
myencoder = encoder('E1', pinA6, pinA7, 3, False)

## An encoder finite state machine. It is constructed using an encoder object and a refresh rate in microseconds
task1 = encFSM('FSM1', myencoder, 10000, False)

## Represents a simple user interface
task2 = UIFSM('UI', 2, 10000, False)

# Run the tasks in sequence over and over again
while True:
    task1.run()
    task2.run(task1.encoder.set_position, task1.encoder.get_position, task1.encoder.get_delta)


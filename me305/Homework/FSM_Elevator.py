'''
@file FSM_Elevator.py

@author E. Nicholson

@date October 6, 2020

@brief      This file implements a finite-state-machine to control an elevator.

@details    This file serves as an example implementation of a finite-state-machine using
            Python. This file implements some code to control an imaginary two-floor
            elevator system. The user has two buttons to request motion. 
            button_1 coresponds to floor_1 and button_2 coresponds to floor_2.
            There are also proximity sensors at either floor to determine the 
            elevator's position.
            
            The source code for this file can be found here: 
    
            https://bitbucket.org/ewnichol/me305-me405_labs/src/master/Homework/FSM_Elevator.py
'''

from random import choice
import time

class TaskElevator:
    '''
    @brief      A finite state machine to control an elevator.
    @details    This class implements a finite state machine to control the
                operation of an elevator. The elevator has four states, moving
                up or down, and stopped on floor 1 or floor 2.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT                 = 0    
    
    ## Constant defining State 1
    S1_MOVING_DOWN          = 1    
    
    ## Constant defining State 2
    S2_MOVING_UP            = 2    
    
    ## Constant defining State 3
    S3_STOPPED_ON_FLOOR_1   = 3    
    
    ## Constant defining State 4
    S4_STOPPED_ON_FLOOR_2   = 4

    def __init__(self, ID, interval, button_1, button_2, firstprox, secondprox, Motor):
        '''
        @brief            Creates a TaskElevator object.
        @param ID         A string identifying the object for traceing output
        @param interval   Defines task timing
        @param button_1   An object from class Button representing floor_1 selection
        @param button_2   An object from class Button representing floor_2 selection
        @param firstprox  A logical parameter representing the floor_1 proximity sensor
        @param secondprox A logical parameter representing the floor_2 proximity sensor
        @param Motor      An object from class MotorDriver representing the elevator motor.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The unique ID string for the object, used for trace output
        self.ID = ID
        
        ## The button object used to request floor_1
        self.button_1 = button_1
        
        ## The button object used to request floor_2
        self.button_2 = button_2
        
        ## The logical parameter used to locate the elevator on the first floor
        self.firstprox = firstprox
        
        ## The logical parameter used to locate the elevator on the second floor
        self.secondprox = secondprox
        
        ## The motor object driving the elevator car
        self.Motor = Motor
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in seconds between runs of the task
        self.interval = interval
        
        ## The timestamp for the first iteration
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''

        ## Captures the current time to compare with self.next_time
        self.curr_time = time.time()
        
        # This conditional implements the timing system by restricting when the FSM can change states
        if self.curr_time > self.next_time:
            
            # This conditional contains all of the code to handle state actions and transitions
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                print(str(self.runs) + ': ' + self.ID + ' State 0: ' + 
                      'button_1 (' + str(self.button_1.state) + ') ' + 
                      'button_2 (' + str(self.button_2.state) + ') ' + 
                      'floor_1 (' + str(self.firstprox.state) + ') ' + 
                      'floor_2 (' + str(self.secondprox.state) + ')')  
                # The elevator always transitions to moving down on initialization
                self.transitionTo(self.S1_MOVING_DOWN)
                self.Motor.DOWN()
                # On initialization all buttons and proximity sensors are reset
                self.button_1.clearButton()
                self.button_2.clearButton()
                self.firstprox.clearButton()
                self.secondprox.clearButton()
            
            elif(self.state == self.S1_MOVING_DOWN):
                # Run State 1 Code
                print(str(self.runs) + ': ' + self.ID + ' State 1: ' + 
                      'button_1 (' + str(self.button_1.state) + ') ' + 
                      'button_2 (' + str(self.button_2.state) + ') ' + 
                      'floor_1 (' + str(self.firstprox.state) + ') ' + 
                      'floor_2 (' + str(self.secondprox.state) + ')') 
                # If the floor_1 sensor returns true, a state transition occurs
                if(self.firstprox.useButton()):
                    self.transitionTo(self.S3_STOPPED_ON_FLOOR_1)
                    # button_1 is cleared upon arrival at the first floor
                    self.button_1.clearButton()
                    # The motor is stopped
                    self.Motor.STOP()
            
            elif(self.state == self.S2_MOVING_UP):
                # Run State 2 Code
                print(str(self.runs) + ': ' + self.ID + ' State 2: ' + 
                      'button_1 (' + str(self.button_1.state) + ') ' + 
                      'button_2 (' + str(self.button_2.state) + ') ' + 
                      'floor_1 (' + str(self.firstprox.state) + ') ' + 
                      'floor_2 (' + str(self.secondprox.state) + ')') 
                # If the floor_2 sensor returns true, a state transition occurs
                if(self.secondprox.useButton()):
                    self.transitionTo(self.S4_STOPPED_ON_FLOOR_2)
                    # button_2 is cleared upon arrival on the second floor
                    self.button_2.clearButton()
                    # The motor is stopped
                    self.Motor.STOP()
            
            elif(self.state == self.S3_STOPPED_ON_FLOOR_1):
                # Transition to state 1 if the left limit switch is active
                print(str(self.runs) + ': ' + self.ID + ' State 3: ' + 
                      'button_1 (' + str(self.button_1.state) + ') ' + 
                      'button_2 (' + str(self.button_2.state) + ') ' + 
                      'floor_1 (' + str(self.firstprox.state) + ') ' + 
                      'floor_2 (' + str(self.secondprox.state) + ')') 
                # If the elevator is stopped on floor_1 and button_2 is pressed a state transition occurs
                if(self.button_2.useButton()):
                    self.transitionTo(self.S2_MOVING_UP)
                    # The first floor proximity sensor is cleared whe the elevator leaves
                    self.firstprox.clearButton()
                    # The motor moves up
                    self.Motor.UP()
            
            elif(self.state == self.S4_STOPPED_ON_FLOOR_2):
                # Run State 4 Code
                print(str(self.runs) + ': ' + self.ID + ' State 4: ' + 
                      'button_1 (' + str(self.button_1.state) + ') ' + 
                      'button_2 (' + str(self.button_2.state) + ') ' + 
                      'floor_1 (' + str(self.firstprox.state) + ') ' + 
                      'floor_2 (' + str(self.secondprox.state) + ')') 
                # If the elevator is stopped on floor_2 and button_1 is pressed a state transition occurs
                if(self.button_1.useButton()):
                    self.transitionTo(self.S1_MOVING_DOWN)
                    # The second floor proximity sensor is cleared whe the elevator leaves
                    self.secondprox.clearButton()
                    # The motor moves down
                    self.Motor.DOWN()
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = self.next_time + self.interval
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        

class Button:
    '''
    @brief      A pushbutton class
    @details    This class represents a button that the can be pushed by the 
                imaginary passenger or triggered by the elevator. As of right 
                now this class is implemented using "pseudo-hardware".
    '''
    
    def __init__(self, pin):
        '''
        @brief          Creates a Button object
        @param pin      A pin object that the button is connected to
        @param state    A boolean representing the state of the button
        '''
        
        ## The pin object used to read the Button state
        self.pin = pin
        
        ## Configures the initial button state to a randomized T/F value
        self.state = choice([1, 0])
        
        print('Button object created attached to pin '+ str(self.pin))


    def clearButton(self):
        '''
        @brief      Sets the button state to 0.
        @details    This method can be used to reset the state of a button.
        '''
        self.state = 0

    
    def useButton(self):
        '''
        @brief      Simulates the button being used.
        @details    Since there is no hardware attached this method
                    stores andreturns a randomized True or False value.
        @return     A boolean representing the state of the button.
        '''
        self.state = choice([1, 0])
        
        return self.state
    
    def getButtonState(self):
        '''
        @brief      Checks the stored button state.
        @details    This is used to pull the button state for the trace.
        @return     A boolean representing the state of the button.
        '''
        
        return self.state
    
    
class MotorDriver:
    '''
    @brief      A motor driver.
    @details    This class represents a motor driver used to make the elevator
                move up and down.
    '''
    
    def __init__(self):
        '''
        @brief Creates a MotorDriver Object
        '''
        pass
    
    def UP(self):
        '''
        @brief Moves the motor up
        '''
        print('Motor moving up, motor = 1')
    
    def DOWN(self):
        '''
        @brief Moves the motor down
        '''
        print('Motor moving down, motor = 2')
    
    def STOP(self):
        '''
        @brief Stops the motor
        '''
        print('Motor Stoped, motor = 0')




























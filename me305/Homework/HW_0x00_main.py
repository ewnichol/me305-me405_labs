'''
@file HW_0x00_main.py

@author E. Nicholson

@date October 6, 2020

@brief      This file configures and runs two elevator finite-state-machines.

@details    This main file interacts with FSM_Elevator.py to set up and run two
            finite state machines. Both machine run cooperatively, and can be
            given different timing intervals. The hardware setup shown does
            not actually interact with real hardware.
            
            The source code for this file can be found here: 
    
            https://bitbucket.org/ewnichol/me305-me405_labs/src/master/Homework/HW_0x00_main.py
'''

from FSM_Elevator import Button, MotorDriver, TaskElevator

        
# Creating objects to pass into first elevator task constructor
button_1_1 = Button('PB6')
button_2_1 = Button('PB7')
firstprox_1 = Button('PB8')
secondprox_1 = Button('PB9')
Motor_1 = MotorDriver()

# Creating objects to pass into second elevator task constructor
button_1_2 = Button('PB1')
button_2_2 = Button('PB2')
firstprox_2 = Button('PB2')
secondprox_2 = Button('PB4')
Motor_2 = MotorDriver()


# Creating a task object using the button and motor objects above
task1 = TaskElevator('E1', 0.1, button_1_1, button_2_1, firstprox_1, secondprox_1, Motor_1)
task2 = TaskElevator('E2', 0.5, button_1_2, button_2_2, firstprox_2, secondprox_2, Motor_2)

# Run the tasks in sequence over and over again
for N in range(10000000): # effectively while(True):
    task1.run()
    task2.run()

'''
@file LAB_0x06_UI_FrontEnd.py

@author E. Nicholson

@date October 26, 2020

@brief      This file holds the Lab 0x06 front end UI Finite State Machine class and the code to run it
            
            The source code for this file can be found here: 
            
            https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305%20Lab%200x06%20%26%20Lab%200x07/LAB_0x06_UI_FrontEnd.py
'''

import serial
import time
import matplotlib.pyplot as plt
import numpy as np
import csv


class FrontEnd_UIFSM:
    '''
    @brief      A front end UI that recieves user input and sends commands to the Nucleo
    @details    This FSM handles user input processing, and sends screened commands to
                the Nucleo. It also recieves data transmissions, and plots 
                test results.
    
    @image html lab0x06_FEUI_FSM.png
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT             = 0    
    
    ## Constant defining State 1 - Setup
    S1_SETUP            = 1
    
    ## Constant defining State 2 - Wait
    S2_WAIT             = 2
    
    ## Constant defining State 3 - Recieve Transmit
    S3_RECIEVE_TRANSMIT = 3
    
    ## Constant defining State 4 - Process
    S4_PROCESS          = 4
    
    ## Constant defining State 5 - Exit
    S5_EXIT             = 5

    def __init__(self, ID, ref_rate, debug):
        '''
        @brief              Creates a UI object
        @param ID           A string identifying the object for tracing output.
        @param ref_rate     The refresh rate of the Front End UI
        @param debug        A boolean that toggles the trace output on or off
        '''
        
        # ---- General Setup ----
        
        print('UI Task Created')
        self.com_open()
        
        ##  The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ##  The unique ID string for the object, used for trace output
        self.ID = ID
        
        ## A boolean that toggles the trace output to the command line
        self.debug = debug
        
        ##  The refresh rate for the encoder position in microseconds
        self.ref_rate = ref_rate    
        
        ## Boolean describing if a recording is in progress
        self.recording = False
        
        # ---- Timing Configuration ----

        ##  The timestamp for the initialization of the object in microseconds
        self.start_time = time.time()
               
        ##  The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.ref_rate
        
    def run(self):
        '''
        @brief      Runs one iteration of the encoder recorder task
        '''
        ## The current time at the start of an update
        self.curr_time = time.time()
        if (self.curr_time - self.next_time) > 0:            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                if self.debug:
                    self.trace()

                self.transitionTo(self.S1_SETUP)

            elif(self.state == self.S1_SETUP):
                # Run State 1 Code         
                
                if self.debug:
                    self.trace()
                print('Enter desired test type (step,signal): ', end = '')
                setup_type = self.get_input('setup')
                self.send(str('SG'+str(setup_type)).encode('ascii'))
                
                if setup_type == 'default':
                    self.setup_type = 'Step'
                    pass
                elif setup_type == 'step':
                    
                    ## String describing the type of test being run
                    self.setup_type = 'Step'
                    
                    print('Enter desired Kp value or press enter to skip... ', end = '')
                    KP = self.get_input('pos_float')
                    self.send(str('KP'+str(KP)).encode('ascii'))
    
                    print('Enter desired Ki value or press enter to skip... ', end = '')
                    KI = self.get_input('pos_float')
                    self.send(str('KI'+str(KI)).encode('ascii'))
                    
                    print('Enter desired test time period or press enter to skip... ', end = '')
                    Test_Time = self.get_input('float')
                    self.send(str('TT'+str(Test_Time)).encode('ascii'))
                    
                    print('Enter desired step delay or press enter to skip... ', end = '')
                    Step_Time = self.get_input('float')
                    self.send(str('SD'+str(Step_Time)).encode('ascii'))
                    
                    print('Enter desired step size or press enter to skip... ', end = '')
                    Omega_step = self.get_input('float')
                    self.send(str('OS'+str(Omega_step)).encode('ascii'))
                elif setup_type == 'signal':
                    self.setup_type = 'Custom Signal'
                    
                    print('Enter desired Kp value or press enter to skip... ', end = '')
                    KP = self.get_input('pos_float')
                    self.send(str('KP'+str(KP)).encode('ascii'))
                    
                    ## Stored user input KP
                    self.KP = KP
    
                    print('Enter desired Ki value or press enter to skip... ', end = '')
                    KI = self.get_input('pos_float')
                    self.send(str('KI'+str(KI)).encode('ascii'))
                    
                    ## Stored user input KI
                    self.KI = KI
                    
                    print('Transmitting predefined signal from SIGNAL.csv ', end = '')
                    
                    with open('reference_EWN.csv') as csv_file:
                        csv_reader = csv.reader(csv_file, delimiter=',')
                        line_count = 0
                        line_sent = 0
                        print('Data transmitted is Time [s], Omega_Ref [rpm], Theta_Ref [deg]')
                        t = 0
                        last_t = 0
                        for row in csv_reader:
                            
                            t = float(row[0])
                            # print('Time: ' + str(t) + 'Last Time: ' + str(last_t))
                            if (t - last_t) > 0.05 or line_count ==0: 
                                # print(f'\t{row[0]}, {row[1]}, {row[2]}')
                                self.send(str('DP'+f'{row[0]},{row[1]},{row[2]}').encode('ascii'))
                                line_sent += 1
                                last_t = t
                            
                            line_count += 1
                        print(f'Processed {line_count} lines, Sent {line_sent} lines.')
                else:
                    pass
                    
                
                self.transitionTo(self.S2_WAIT)
                self.send(str('START').encode('ascii'))

            elif(self.state == self.S2_WAIT):
                # Run State 2 Code         
                
                if self.debug:
                    self.trace()
                
                msg = self.recieve()
                
                if msg == 'TRANSMIT':
                    self.clear_data()
                    self.transitionTo(self.S3_RECIEVE_TRANSMIT)
                else:
                    pass

            elif(self.state == self.S3_RECIEVE_TRANSMIT):
                # Run State 4 Code
                
                if self.debug:
                    self.trace()

                msg = self.recieve()
                if msg == 'END':
                    # print(self.recieve())
                    self.transitionTo(self.S4_PROCESS) 
                else:
                    # print(str(msg))
                    self.raw.append(msg)
                    data_point = msg.strip().split(',')
                    self.tim = np.append(self.tim, float(data_point[0]))
                    self.omega_ref = np.append(self.omega_ref, float(data_point[1]))
                    self.omega_meas = np.append(self.omega_meas, float(data_point[2]))
                    if self.setup_type == 'Custom Signal':
                        self.theta_ref = np.append(self.theta_ref, float(data_point[3]))
                        self.theta_meas = np.append(self.theta_meas, float(data_point[4]))
                    self.duty = np.append(self.duty, float(data_point[5]))
                    
            elif(self.state == self.S4_PROCESS):
                # Run State 5 Code                 
                
                if self.debug:
                    self.trace()
                
                print('Processing Data')
                
                with open('data.csv','w') as f:
                    for i in range(len(self.raw)):
                        f.write(self.raw[i] + '\n')
                    print('CSV Created')
                
                if self.setup_type == 'Step':
                    fig1, axs = plt.subplots(2, sharex=True)
                    fig1.suptitle('Response Test: ' + str(self.setup_type))
                    axs[0].plot(self.tim,self.omega_ref)
                    axs[0].plot(self.tim,self.omega_meas)
                    axs[0].set_ylabel('Output Shaft Speed [rpm]')
                    axs[0].legend(['Omega Ref','Omega Measured'])
                    axs[1].plot(self.tim,self.duty)
                    axs[1].set_xlabel('Time [sec]')
                    axs[1].set_ylabel('PWM Level [%]')
                    axs[1].legend(['Control Signal'])
                    
                elif self.setup_type == 'Custom Signal':
                    
                    K = len(self.tim)
                    j = 0
                    for k in range(K):
                        j += ((self.omega_ref[k]-self.omega_meas[k])**2 + (self.theta_ref[k]-self.theta_meas[k])**2)
                    J = j/K
                
                    print('For this test, J = {:.2f}'.format(J))
                    
                    fig1, axs = plt.subplots(2, sharex=True)
                    fig1.suptitle('Response Test: ' + str(self.setup_type))
                    axs[0].plot(self.tim,self.omega_ref)
                    axs[0].plot(self.tim,self.omega_meas)
                    axs[0].set_ylabel('Shaft Speed [rpm]')
                    axs[0].text(0,-300,'J = {:.2f}'.format(J))
                    axs[0].text(10,-700,'KP = {:.2f}'.format(self.KP))
                    axs[0].text(10,-1000,'KI = {:.5f}'.format(self.KI))
                    axs[0].legend(['Omega Ref','Omega Measured'])
                    axs[1].plot(self.tim,self.theta_ref)
                    axs[1].plot(self.tim,self.theta_meas)
                    axs[1].set_ylabel('Shaft position [deg]')
                    axs[1].legend(['Theta Ref','Theta Measured'])
                    
                    fig2, ax = plt.subplots(1, sharex=True)
                    fig2.suptitle('Response Test: ' + str(self.setup_type))
                    ax.plot(self.tim,self.duty)
                    ax.set_xlabel('Time [sec]')
                    ax.set_ylabel('PWM Level [%]')
                    ax.legend(['Control Signal'])
                
                
                if input('End Task? y/n: ') == 'y':
                    self.com_close()
                    self.transitionTo(self.S5_EXIT)
                    print('You may now close the program')
                    
                else:
                    self.transitionTo(self.S1_SETUP)
                    
            elif(self.state == self.S5_EXIT):
                # Run State 4 Code                 
                
                if self.debug:
                    self.trace()

            else:
                # Invalid state code (error handling)
                pass
            
            self.next_time = time.time() + self.ref_rate
    
    def send(self, msg):
        '''
        @brief Sends a message to the Nucleo and waits to avoid message stacking
        @param msg Message to be sent
        '''
        self.ser.write(msg)
        time.sleep(.001)
        
    def recieve(self):
        '''
        @brief  Reads a message from the Nucleo and removes special characters
        @return proc_msg The message with special characters removed
        '''
        msg = self.ser.readline().decode('ascii')
        proc_msg = msg.strip()
        return proc_msg
    
    def com_open(self):
        '''
        @brief  Opens the serial communication line
        '''
        ## Serieal communication line
        self.ser = serial.Serial(port='COM3',baudrate=115373,timeout=1)
        print('Serial Line Opened')
        
    def com_close(self):
        '''
        @brief  Closes the serial communication line
        '''
        self.ser.close()
        print('Serial Line Closed')

    def clear_data(self):
        '''
        @brief      Resets data recording paramaters
        @details    This function emptys all data arrays/lists and resets the
                    data counter.
        '''
        ## list for storing raw data dump
        self.raw = []

        ## Array for storing encoder data
        self.tim = np.array([], dtype=np.float32)
        
        ## Array for storing encoder data
        self.omega_ref = np.array([], dtype=np.float32)
        
        ## Array for storing measured motor speed
        self.omega_meas = np.array([], dtype=np.float32)
        
        ## Array for storing encoder data
        self.theta_ref = np.array([], dtype=np.float32)
        
        ## Array for storing measured motor speed
        self.theta_meas = np.array([], dtype=np.float32)
        
        ## Array for storing refference signal
        self.duty = np.array([], dtype=np.float32)
        
        ## Current number of data points recorded
        self.curr_data = 0        

    def get_input(self, typ):
        '''
        @brief Obtains user input, and screens for correct type. 
        @details    This method obtains user input and check for a valid type.
                    The input request is repeated until a valid input is entered. 
        @param typ          A string defining the desired input type [int,pos_int,neg_int,float,pos_float,neg_float,setup,str]
        @return user_in     The screened user input
        '''
        while True:
            if typ == 'int':
                user_in = input('   Enter an interger: ')
                if self.check_blank(user_in):
                    break
                try:
                    user_in = int(user_in)
                    break
                except:
                    print('     Invalid')
            elif typ == 'pos_int':
                user_in = input('   Enter a positive interger: ')
                if self.check_blank(user_in):
                    break
                try:
                    user_in = int(user_in)
                    if user_in >= 0:
                        break
                    else:
                        print('     Your input was not positive')
                except:
                    print('     Invalid')
                    
            elif typ == 'neg_int':
                user_in = input('   Enter a negative interger: ')
                if self.check_blank(user_in):
                    break
                try:
                    user_in = int(user_in)
                    if user_in <= 0:
                        break
                    else:
                        print('     Your input was not negative')
                except:
                    print('     Invalid')
            elif typ == 'float':
                user_in = input('   Enter a number: ')
                if self.check_blank(user_in):
                    break
                try:
                    user_in = float(user_in)
                    break
                except:
                    print('     Invalid')
            elif typ == 'pos_float':
                user_in = input('   Enter a positive number: ')
                if self.check_blank(user_in):
                    break
                try:
                    user_in = float(user_in)
                    if user_in >= 0:
                        break
                    else:
                        print('     Your input was not positive')
                except:
                    print('     Invalid')
            elif typ == 'neg_float':
                user_in = input('   Enter a negative number: ')
                if self.check_blank(user_in):
                    break
                try:
                    user_in = float(user_in)
                    if user_in <= 0:
                        break
                    else:
                        print('     Your input was not negative')
                except:
                    print('Invalid')
            elif typ == 'setup':
                user_in = input('--> ')
                if self.check_blank(user_in):
                    break
                if user_in in ['step','signal']:
                    break
                else:
                    print('     That is not valid')
            elif typ == 'str':
                user_in = input('   Enter a string: ')
                if self.check_blank(user_in):
                    break
                break
            else:
                user_in = input('   Enter something: ')
                if self.check_blank(user_in):
                    break
                break
        
        if user_in == '':
            user_in = 'default'
            
        return user_in
    
    def check_blank(self, user_in):
        '''
        @brief Checks if the string passed is empty, ''
        @param user_in The string to be checked
        @return empty Boolean, True if empty, False if not
        '''
        if user_in == '':
            print('     Skipped...')
            empty = True
        else:
            empty = False
            
        return empty

    def trace(self):
        '''
        @brief  Prints a trace output when called
        '''
        print(self.ID + ' State: ' + str(self.state) + ' Time: ' + str(self.curr_time))

           
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
        if self.debug:
            print('----------------')

if __name__ == '__main__':    
    '''
    This block of code allows this file to run the UI FSM stand alone
    '''
    
    ## A front end UI task object
    task1 = FrontEnd_UIFSM('FSM1', .00001, False)
    
    

    
    try:
        while True:
            task1.run()
    finally:
        task1.ser.close()

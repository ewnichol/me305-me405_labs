'''
@file LAB_0x06_Control_FSM.py

@author E. Nicholson

@date October 29, 2020

@brief      This file contains a controller FSM that regulates a motor's speed

            The source code for this file can be found here: 
    
            https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305%20Lab%200x06%20%26%20Lab%200x07/LAB_0x06_Control_FSM.py
'''
import utime
import LAB_0x06_shares as shares
        
class Controller_FSM:
    '''
    @brief      This FSM regulates a motors speed using a ref signal and a measured speed
    @details    This machine handles the operation of three objects, a motor
                driver, and encoder driver, and a controller. It recieves a
                refference speed from the Lab 0x06 common shres file, and 
                compares it with the measured speed calculates using encoder data.
                The difference between ref and measured produces an error signal that
                is fed into the closed loop controller. The controller returns
                a PWM percentage which this FSM passes to the motor driver.
                
    @image html lab0x06_Control_FSM.png
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT             = 0    
    
    ## Constant defining State 1 - Control Off
    S1_CTRL_OFF         = 1
    
    ## Constant defining State 2 - COntrol On
    S2_CTRL_ON         = 2

    def __init__(self, ID, encoder, motor, controller, ref_rate, debug):
        '''
        @brief              Creates a controller task object
        @param ID           A string identifying the object for tracing output.
        @param encoder      An encoder driver object.
        @param motor        A motor driver object
        @param contoller    A controller object
        @param ref_rate     The refresh rate of the record task
        @param debug        A boolean that toggles the trace output on or off
        '''
        
        # ---- General Setup ----
        
        ##  The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ##  The unique ID string for the object, used for trace output
        self.ID = ID
        
        ##  Number of times the FSM has run in the on state during a single control period
        self.runs = 0
        
        ## A boolean that toggles the trace output to the command line
        self.debug = debug
                
        ## The encoder object that the task interfaces with
        self.encoder = encoder
        
        ## The motor object that the task interfaces with
        self.motor = motor
        
        ## The controller that the task uses
        self.controller = controller
        
        ##  The refresh rate for the encoder position in microseconds
        self.ref_rate = ref_rate
        
        # ---- Timing Configuration ----

        ##  The timestamp for the initialization of the object in microseconds
        self.start_time = utime.ticks_us()
               
        ##  The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.ref_rate)
        
    def run(self):
        '''
        @brief      Runs one iteration of the encoder recorder task
        '''
        ## The current time at the start of an update
        self.curr_time = utime.ticks_us()
           
        if utime.ticks_diff(self.curr_time, self.next_time) > 0:
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                
                if self.debug:
                    self.trace()
                    print('INIT Controller Task')
                    
                self.transitionTo(self.S1_CTRL_OFF)
            
            elif(self.state == self.S1_CTRL_OFF):
                # Run State 1 Code   
                
                if self.debug:
                    self.trace()
                
                if shares.MOTORS_ENABLED == True:
                    self.transitionTo(self.S2_CTRL_ON)
                    
                    ## Time stampt of the last update
                    self.prev_update = self.curr_time - self.ref_rate
                    self.controller.set_KP(shares.KP)
                    self.controller.set_KI(shares.KI)
                    self.motor.enable()
                    shares.Omega_meas = 0
                
                self.next_time = utime.ticks_add(self.next_time, self.ref_rate)
            
            elif(self.state == self.S2_CTRL_ON):
                # Run State 2 Code 
                #print('control on')
                if self.runs == 0:
                    ## The time span in seconds between each encoder update
                    self.update_interval = self.ref_rate
                else:
                    self.update_interval = utime.ticks_diff(self.curr_time, self.prev_update)/10**6
                       
                self.encoder.update()
                
                
                ref = shares.Omega_ref
                # pos = self.encoder.get_position('')
                # delta = self.encoder.get_delta('')
                Omega_meas = (self.encoder.get_delta('rad')/self.update_interval)*(60/(2*3.14159))
                Theta_meas = self.encoder.get_position('deg')
                # print('Pos: ' + str(pos) + ' Delta: ' + str(delta))
                ctrl_signal = self.controller.compute(ref, Omega_meas, self.update_interval)
                
                self.motor.set_duty(ctrl_signal)
                
                shares.Omega_meas = Omega_meas
                shares.Theta_meas = Theta_meas
                shares.duty = ctrl_signal
                

                
                if self.debug:
                    self.trace()
                
                if shares.MOTORS_ENABLED == False:
                    self.transitionTo(self.S1_CTRL_OFF)
                    self.runs = 0
                    self.motor.set_duty(0)
                    self.encoder.set_position()
                    # print('Pos: ' + str(self.encoder.get_position('')) + ' Delta: ' + str(self.encoder.get_delta('')))
                    self.controller.reset()
                    self.motor.disable()
                
                self.next_time = utime.ticks_add(self.next_time, self.ref_rate)
                self.runs += 1
                self.prev_update = self.curr_time
            else:
                # Invalid state code (error handling)
                pass
        
        self.runs += 1
        
    def trace(self):
        '''
        @brief  Prints a trace output when called
        '''
        print(self.ID + ' State: ' + str(self.state) + ' Time: ' + str(self.curr_time/1000000))

           
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
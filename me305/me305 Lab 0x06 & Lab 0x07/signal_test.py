'''
@file signal_test.py

@author E. Nicholson

@date October 29, 2020

@brief      This file contains a finite state machine that applies a test signal and records a response

            The source code for this file can be found here: 
    
            https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305%20Lab%200x06%20%26%20Lab%200x07/signal_test.py
'''
import utime
import LAB_0x06_shares as shares
        
class SignalTest:
    '''
    @brief      A finites state machine that applies a test signal and records the response
    @details    This FSM is designed to generate a step response given the
                defining parameters. It is currently partially configured to 
                accept a custom signal. This FSM interface with the common
                shres file used by othe Lab 0x06 tasks.
                
    @image html lab0x06_SigTest_FSM.png
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT             = 0    
    
    ## Constant defining State 1 - Wait
    S1_WAIT             = 1
    
    ## Constant defining State 2 - Test
    S2_TEST              = 2
    

    def __init__(self, ID,  ref_rate, debug):
        '''
        @brief              Creates a step response task object
        @param ID           A string identifying the object for tracing output.
        @param ref_rate     The refresh rate of the record task
        @param debug        A boolean that toggles the trace output on or off
        '''
        
        # ---- General Setup ----
        
        ##  The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ##  The unique ID string for the object, used for trace output
        self.ID = ID
        
        ## A boolean that toggles the trace output to the command line
        self.debug = debug
        
        ##  The refresh rate for the encoder position in microseconds
        self.ref_rate = ref_rate
        
        ##  The type of signal being tested, initialized to 'step'
        self.Signal_Type = 'step'
        
        ##  Boolean indicating if the test has been completed, initialized to false
        self.Test_End = False
        
        ## The delay in microseconds befor the step is applied, initialized to 0
        self.step_time = 0
        
        ## The period in microseconds of the test, initialized to 0
        self.test_time = 0
        
        ##  Length of signal, initialized to 0
        self.signal_len = 0
        
        ##  Current index of the signal
        self.signal_idx = 0
        
        ##  Time interval between signal points, dynamically updated as signal is processed
        self.signal_int = 0

        # ---- Timing Configuration ----

        ##  The timestamp for the initialization of the object in microseconds
        self.start_time = utime.ticks_us()
               
        ##  The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.ref_rate)
        
        ##  The total elapsed test time in microseconds
        self.el_test_time = 0
        
    def run(self):
        '''
        @brief      Runs one iteration of the signal test task
        '''
        ## The current time at the start of an update
        self.curr_time = utime.ticks_us()
        
        if utime.ticks_diff(self.curr_time, self.next_time) > 0:       
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                
                if self.debug:
                    self.trace()
                    print('INIT Signal Task')
                    
                self.transitionTo(self.S1_WAIT)
            
            elif(self.state == self.S1_WAIT):
                # Run State 1 Code   
                
                if self.debug:
                    self.trace()
                
                if shares.Start == True:
                    print('Testing...')
                    shares.Start = False
                    shares.MOTORS_ENABLED = True
                    self.transitionTo(self.S2_TEST)
                    self.Signal_Type = shares.Signal_Type
                    self.reset_data()
                    
                    ## Timestamp of the test start time
                    self.test_start_time = utime.ticks_us()
                    
                
                self.next_time = utime.ticks_add(self.next_time, self.ref_rate)
            
            elif(self.state == self.S2_TEST):
                # Run State 2 Code 
                
                # if self.debug:
                #     self.trace()
                
                ## Elapsed test recording time
                self.el_test_time = utime.ticks_diff(self.curr_time, self.test_start_time)
                

                if self.Signal_Type == 'step':
                    if self.debug:
                        self.trace()
                    
                    if (self.el_test_time) > self.step_time:
                        shares.Omega_ref = self.Omega_1
                    else:
                        shares.Omega_ref = self.Omega_0
                    if utime.ticks_diff(self.test_time, self.el_test_time) < 0:
                        self.Test_End = True
                    
                elif self.Signal_Type == 'signal':
                    if utime.ticks_diff(self.el_test_time, self.next_test_time) >= 0:
                        if self.debug:
                            self.trace()
                        
                        shares.Omega_ref = shares.Input_Signal_Omega[self.signal_idx]
                        shares.Theta_ref = shares.Input_Signal_Theta[self.signal_idx]
                        
                        ## Time when the next refference value will be update in microseconds
                        self.next_test_time = int(shares.Input_Signal_Tim[self.signal_idx]*10**6)
                        self.signal_idx += 1
                        
                        if self.signal_idx == self.signal_len:
                            self.Test_End = True
                
                # shares.data.append('{:f},{:f},{:f},{:f}'.format(self.el_test_time/(10**6), 
                #                      shares.Omega_ref,
                #                      shares.Omega_meas,
                #                      shares.duty))
                
                shares.Data_Tim.append(self.el_test_time/(10**6))
                shares.Data_Omega_Ref.append(shares.Omega_ref)
                shares.Data_Omega.append(shares.Omega_meas)
                shares.Data_Theta_Ref.append(shares.Theta_ref)
                shares.Data_Theta.append(shares.Theta_meas)
                shares.Data_Duty.append(shares.duty)
                                     
                    
                if  self.Test_End == True:
                    self.transitionTo(self.S1_WAIT)
                    shares.Omega_ref = shares.Omega_init
                    self.el_test_time = 0
                    shares.TRANSMIT_READY = True
                    shares.MOTORS_ENABLED = False
                    self.Test_End = False
                    
                
                self.next_time = utime.ticks_add(self.next_time, self.ref_rate)
                
            else:
                # Invalid state code (error handling)
                pass
    
    def reset_data(self):
        '''
        @brief      Resets data recording, timing, and test variables
        '''
        
        ## List for storing encoder data
        if self.Signal_Type == 'step':
            shares.Omega_init = 0
            shares.Omega_ref = 0
            ## Low Omega value
            self.Omega_0 = shares.Omega_init
            ## High Omega value
            self.Omega_1 = shares.Omega_step
            self.step_time = int(shares.Step_Time*10**6)
            self.test_time = int(shares.Test_Time*10**6)
        elif self.Signal_Type == 'signal':
            self.signal_val = shares.Input_Signal_Omega
            shares.Omega_init = shares.Input_Signal_Omega[0]
            shares.Omega_ref = shares.Input_Signal_Omega[0]
            self.signal_len = len(shares.Input_Signal_Tim)
            self.signal_idx = 0
            self.signal_int = shares.Input_Signal_Tim[0] - shares.Input_Signal_Tim[1]
            self.next_test_time = int(shares.Input_Signal_Tim[self.signal_idx]*10**6)
        
    def trace(self):
        '''
        @brief  Prints a trace output when called
        '''
         
        if self.state == self.S2_TEST and self.Signal_Type == 'step':
            print(self.ID + ' State: ' + str(self.state) 
                  + ' Test Type: ' + str(self.Signal_Type)
                  + ' Time: ' + str(self.curr_time/10**6)
                  + ' Elapsed Test Time: ' + str(self.el_test_time/10**6))
            print('     Test Time: ' + str(self.test_time/10**6) 
                  + ' Step Time: ' + str(self.step_time/10**6) 
                  + ' Diff: ' + str(utime.ticks_diff(self.test_time, self.el_test_time)/10**6))
            print('     Omega: ' + str(shares.Omega_meas) 
                  + ' Omega_Ref: ' + str(shares.Omega_ref) 
                  + ' Duty: ' + str(shares.duty))
        elif self.state == self.S2_TEST and self.Signal_Type == 'signal':
            print(self.ID 
                  # + ' State: ' + str(self.state) 
                  # + ' Test Type: ' + str(self.Signal_Type)
                  + ' Elapsed Test Time: ' + str(self.el_test_time/10**6)
                  # + ' Next Test Time: ' + str(self.next_time/10**6)
                  )
            print('   Next Signal Time: ' + str(self.next_test_time/10**6) 
                  + ' Signal Idx: ' + str(self.signal_idx)
                  # + ' Signal Interval: ' + str(self.signal_int)
                  )
            print('     Omega: ' + str(shares.Omega_meas) 
                  + ' Omega_Ref: ' + str(shares.Omega_ref) 
                  + ' Duty: ' + str(shares.duty))
           
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
''' 
@file cl_driver.py

@author E. Nicholson

@date November 10, 2020

@brief      This file contains a closed loop contol driver
            
            The source code for this file can be found here: 
    
            https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305%20Lab%200x06%20%26%20Lab%200x07/cl_driver.py
'''

class ClosedLoop:
    ''' 
    @brief  This controller driver recieves refference and measured values and returns a control signal
    
    @details    Currently this river is configured to control a motor on the ME305-405
                hardware kit. This driver is capable of full PI contol.
    
    '''

    def __init__(self, ID, KP, KI, sat_lim):
        '''
        @brief  Creates a motor driver by initializing GPIO pins and turning the motor off for safety
        @param ID           The ID string for the object
        @param KP           Initial proportional gain for the system
        @param KI           Initial integral gain for the system
        @parap sat_lim      Saturation limits for the system
        '''
        
        print ('Creating Controller ' + ID + ' with Kp = ' + str(KP) + ', KI = ' + str(KI))
        
        # ---- General Setup ----
        
        ## The unique ID string for the object, used for trace output
        self.ID = ID
        
        ## The controller's proportional gain
        self.KP = KP
        
        ## The controller's integral gain
        self.KI = KI
        
        ## The controller's output saturation limits
        self.sat_lim = sat_lim
        
        ## The refference signal
        self.ref = 0
        
        ## The measured value
        self.meas = 0
        
        ## The error signal
        self.error = 0
        
        ## The previous error signal
        self.prev_error = 0
        
        ## The trapezoidal integration of the error signal
        self.integral_error = 0
        
        ## The control signal
        self.ctrl_signal = 0
        
        
        
    
    def compute(self, ref, meas, dt):
        '''
        @brief Computes actuation values using current measured system parameters
        @param ref      Refference signal for controller
        @param meas     Measured signal for controller
        @param dt       Time interval between computations
        @return ctrl_signal The actuation signal
        '''
        self.ref = ref
        self.meas = meas
        
        self.error = (self.ref-self.meas)
        
        self.integral_error += .5*(self.error+self.prev_error)/dt
        
        # self.dif_error = (self.error-self.prev_error)/dt
        
        prop = (self.KP*self.error) 
        integral = (self.KI*self.integral_error)
        # dif = (self.KD*self.dif_error)
        
        print('P: ' + str(prop) + ' I: ' + str(integral))
        ctrl_signal = prop + integral
        
        if ctrl_signal > self.sat_lim:
            ctrl_signal = self.sat_lim
        elif ctrl_signal < -1*self.sat_lim:
            ctrl_signal = -1*self.sat_lim
        else:
            pass
        self.ctrl_signal = ctrl_signal
        
        self.prev_error = self.error
        
        return ctrl_signal
    
    def get_KP(self):
        '''
        @brief Returns the current proportional gain of the controller
        @returns Current proportional gain
        '''
        return self.KP
    
    def set_KP(self, KP):
        '''
        @brief      Sets the proportional gain of the controller
        @param KP   New proportional gain
        '''
        self.KP = KP
    
    def get_KI(self):
        '''
        @brief Returns the current integral gain of the controller
        @returns Current integral gain
        '''
        return self.KI
    
    def set_KI(self, KI):
        '''
        @brief      Sets the integral gain of the controller
        @param KP   New integral gain
        '''
        self.KI = KI
        
    def reset(self):
        '''
        @brief Resets the error parameters tracked in the controller
        '''
        self.error = 0
        self.prev_error = 0
        self.integral_error = 0
        

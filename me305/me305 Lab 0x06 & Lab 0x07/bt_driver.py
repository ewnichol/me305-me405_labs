'''
@file bt_driver.py

@author E. Nicholson

@date November 09, 2020

@brief      TXT

            The source code for this file can be found here: 
    
            LINK
'''
from pyb import UART
        
class BTDriver:
    '''
    @brief      TXT
   
    @details    TXT
    '''

    def __init__(self, ID, UART_ID, debug):
        '''
        @brief              Creates a recorder object
        @param ID           A string identifying the object for tracing output.
        @param UART_ID      The UART ID number that will be used for serial communication
        @param debug        A boolean that toggles the trace output on or off
        '''
        
        # ---- General Setup ----
        
        ##  The unique ID string for the object, used for trace output
        self.ID = ID
        
        ## A boolean that toggles the trace output to the command line
        self.debug = debug
        
        ## UART object for handling simple BT serial communication.
        # It is hard coded with a baudrate of 9600 to match the default of the
        # HC-05 bluetooth module used
        self.uart = UART(UART_ID, 9600)
                
        ## The list of standard commands
        self.cmd = [b'OFF',b'ON',b'BLINK',b'SINE',b'SAW']


    def recieve(self):
        '''
        @brief  Recieves commands & messages over BT serial line
        @details    This method also processes the recieved information and returns
                    it in a useful form.
        '''
        pass

    def send(self):
        '''
        @brief  Sends commands & messages over BT serial line
        '''
        pass

    def how_many(self):
        '''
        @brief  Checks if any characters are waiting using UART.any()
        '''
        return self.uart.any()
        
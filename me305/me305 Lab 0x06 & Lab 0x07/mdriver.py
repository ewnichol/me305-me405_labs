''' 
@file mdriver.py

@author E. Nicholson

@date November 10, 2020

@brief      This file contains a motor driver and code used to test its functionality

@details    The class contained within this file is a motor driver. It connects
            to three Nucleo pins bound to an H-Bridge motor driver and configures
            them for PWM using a timer. The conditional at the end of the file
            allows test code to run if this file is used stand alone. The test
            code rotates both motors in forward and reverse with pauses in between.
            
            The source code for this file can be found here: 
    
            https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305%20Lab%200x06/mdriver.py
'''

import pyb

class MotorDriver:
    ''' 
    @brief  A motor driver designed to work with an H-bridge connected to a Nucleo L476RG running Micropython.
    
    @details    This motor driver takes three pyb.Pin objects, two timer channel variables,
                and a pyb.Timer object. It uses these to configure a sleep pin
                and two PWM signal generators. The H-Bridge motor driver is
                controlled using a Pulse 1 Side PWM scheme. The driver software
                uses logic to interporate the given duty command into the correct
                PWM percentage and direction. The driver also has the ability 
                to enable and disable motor motion.
    
    '''

    def __init__ (self, nSLEEP_pin, IN1_pin, IN1_ch, IN2_pin, IN2_ch, timer):
        '''
        @brief  Creates a motor driver by initializing GPIO pins and turning the motor off for safety
        @param nSLEEP_pin   A pyb.Pin object to use as the enable pin.
        @param IN1_pin      A pyb.Pin object to use as the input to half bridge 1.
        @param IN1_ch       The timer channel number to be used for IN1_pin.
        @param IN2_pin      A pyb.Pin object to use as the input to half bridge 2.
        @param IN2_ch       The timer channel number to be used for IN2_pin.
        @param timer        A pyb.Timer object to use for PWM generation on IN1_pin and IN2_pin. 
        '''
        
        print ('Creating a motor driver')
        
        ## The pin used to enable / disable the motor
        self.enable_pin = nSLEEP_pin
        self.disable()
        
        ## A pyb.Timer.channel object configured to drive half bridge 1 with PWM 
        # using the given pyb.Timer, pyb.Pin, and channel number
        self.tch1 = tim.channel(IN1_ch, pyb.Timer.PWM, pin=IN1_pin)
        
        ## A pyb.Timer.channel object configured to drive half bridge 2 with PWM 
        # using the given pyb.Timer, pyb.Pin, and channel number
        self.tch2 = tim.channel(IN2_ch, pyb.Timer.PWM, pin=IN2_pin)
    
    def enable (self):
        '''
        @brief enables the motors by setting the enable pin to high
        '''
        
        print ('Enabling Motor')
        self.enable_pin.high()
    
    def disable (self):
        '''
        @brief disables the motors by setting the enable pin to low
        '''
        print ('Disabling Motor')
        self.enable_pin.low()
    
    def set_duty (self, duty):
        '''
        @brief      Sets the duty for the motor
        @details    This method sets the duty cycle to be sent to the motor to 
                    the given level. Positive values cause effort in one 
                    direction, negative values in the opposite direction.        
        @param duty     A signed integer holding the duty cycle of the PWM signal sent to the motor 
        '''
        
        self.tch1.pulse_width_percent(0)
        self.tch2.pulse_width_percent(0)
        
        try:
            if duty >= 0:
                if duty > 100:
                    duty = 100
                self.tch1.pulse_width_percent(duty)
            elif duty < 0:
                if duty < -100:
                    duty = -100
                self.tch2.pulse_width_percent(-1*duty) 
        except:
            print('Invalid duty value')
            
          
        

if __name__ == '__main__':
    '''
    Test program for motor driver using ME 305 hardware.
    '''
    
    ## The pin object used for the H-bridge sleep pin
    pin_nSLEEP = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
    pin_nSLEEP.low()
    
    ## The pin object used for the Motor 1's H-bridge input 1
    pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4)
    
    ## The pin object used for the Motor 1's H-bridge input 2
    pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5)
    
    ## The pin object used for the Motor 2's H-bridge input 1
    pin_IN3 = pyb.Pin(pyb.Pin.cpu.B0)
    
    ## The pin object used for the Motor 2's H-bridge input 2
    pin_IN4 = pyb.Pin(pyb.Pin.cpu.B1)
    
    ## The timer object used for PWM generation
    tim = pyb.Timer(3,freq=20000);
    
    ## A motor object passing in the pins and timer
    m1 = MotorDriver(pin_nSLEEP, pin_IN1, 1, pin_IN2, 2, tim)
    
    ## A second motor object passing in the pins and timer
    m2 = MotorDriver(pin_nSLEEP, pin_IN3, 3, pin_IN4, 4, tim)
    
    # Enable the motors
    m1.enable()

    print('Fwd')
    m1.set_duty(40)
    m2.set_duty(40)
    
    pyb.delay(1000)
    
    print('Stop')
    m1.set_duty(0)
    m2.set_duty(0)
    
    pyb.delay(1000)

    print('Rev')
    m1.set_duty(-40)
    m2.set_duty(-40)
    
    pyb.delay(1000)
    
    print('Stop')
    m1.set_duty(0)
    m2.set_duty(0)
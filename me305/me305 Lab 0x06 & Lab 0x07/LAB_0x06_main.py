''' 
@file LAB_0x06_main.py

@author E. Nicholson

@date November 24, 2020

@brief      This main file configures hrdware and runs three tasks

@details    This file binds all of the necessary hardware to objects allowing us to
            interface with the ME 305-405 kit. It passes these objects to drivers
            that represent the motor, encoder, and controller. These drivers
            are handled by three FSMs that run at the end of this script.
            
            The source code for this file can be found here: 
    
            https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305%20Lab%200x06%20%26%20Lab%200x07/LAB_0x06_main.py
'''

import pyb
import LAB_0x06_shares as shares
from LAB_0x06_UI_BackEnd import BackEndUIFSM
from signal_test import SignalTest
from m_driver import MotorDriver
from enc_driver import EncDriver
from cl_driver import ClosedLoop
from LAB_0x06_Control_FSM import Controller_FSM

# Create the pin objects used for interfacing with the motors

## The motor sleep pin
pin_nSLEEP = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
pin_nSLEEP.low()

## The pin connected to Motor 1's first H-bridge channel
pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4)

## The pin connected to Motor 1's second H-bridge channel
pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5)

# Creates pin objects for interfacing with the encoders

## The pin object connected to Encoder 1's channel 1
pin_E1CH1 = pyb.Pin(pyb.Pin.cpu.B6)

## The pin object connected to Encoder 1's channel 2
pin_E1CH2 = pyb.Pin(pyb.Pin.cpu.B7)

## A timer object used for PWM generation
mot_tim = pyb.Timer(3,freq=20000);

## A motor object representing Motor 1
m1 = MotorDriver(pin_nSLEEP, pin_IN1, 1, pin_IN2, 2, mot_tim, False)

## An encoder object representing the encoder attached to Motor 1
e1 = EncDriver('E1', pin_E1CH1, 1, pin_E1CH2, 2, 4, 1000, False)

## A controller driver object
c1 = ClosedLoop('CL1', shares.KP, shares.KI, shares.sat_lim)

## A controller task that connects the motor, encoder and controller objects
Controller1 = Controller_FSM('CONT1', e1, m1, c1, 1000, False)

## The pin connected to Motor 2's first H-bridge channel
pin_IN3 = pyb.Pin(pyb.Pin.cpu.B0)

## The pin connected to Motor 2's second H-bridge channel
pin_IN4 = pyb.Pin(pyb.Pin.cpu.B1)

## The pin object connected to Encoder 2's channel 1
pin_E2CH1 = pyb.Pin(pyb.Pin.cpu.C6)

## The pin object connected to Encoder 2's channel 2
pin_E2CH2 = pyb.Pin(pyb.Pin.cpu.C7)

## A motor object representing Motor 2
m2 = MotorDriver(pin_nSLEEP, pin_IN3, 3, pin_IN4, 4, mot_tim, False)

## An encoder object representing the encoder attached to Motor 2
e2 = EncDriver('E2', pin_E2CH1, 1, pin_E2CH2, 2, 8, 1000, False)

## A controller driver object
c2 = ClosedLoop('CL1', shares.KP, shares.KI, shares.sat_lim)

## A controller task that connects the motor, encoder and controller objects
Controller2 = Controller_FSM('CONT2', e2, m2, c2, 1000, False)

## A user interface task object that handles back end communication and data sharing
UI = BackEndUIFSM('BEUI', 2, 1000, False)

## A test object that applies a ref signal to the controller task, and records the response
Test1 = SignalTest('SIG1', 20000, False)

while True:
    UI.run()
    Test1.run()
    Controller1.run()
    # Controller2.run()
''' 
@file LAB_0x06_shares.py

@author E. Nicholson

@date November 24, 2020

@brief      A file on the nucleo acting as a repository for inter-task communication variables

@details    This file initializes a range of variables for inter-task 
            communication. While running, the finite state machines on the nucleo
            will use this file to share information.
            
            The source code for this file can be found here: 
    
            https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305%20Lab%200x06%20%26%20Lab%200x07/LAB_0x06_shares.py
'''

import array

# BEST STEP RESPONSE VALUES
# ## Default proportional gain, set to best tuned value [%/rpm]
# KP = .12

# ## Default integral gain, set to best tuned value [%/rpm-s]
# KI = 0.00004

# BEST SIGNAL RESPONSE VALUES
## Default proportional gain, set to best tuned value [%/rpm]
KP = .13

## Default integral gain, set to best tuned value [%/rpm-s]
KI = 0.00015

## The initial omega value [rpm]
Omega_init = 0

## Controller saturation limit, used to protect hardware during testing [%]
sat_lim = 70

## The signal type being used by the response testing task
Signal_Type = 'step'

## The size of the desired stap input [rpm]
Omega_step = 900

## The time that the step occurs [sec]
Step_Time = 2

## The total test time [sec]
Test_Time = 10

## The time values of an input signal
Input_Signal_Tim = array.array('f',[])

## The omega of an input signal
Input_Signal_Omega = array.array('f',[])

## The theta of an input signal
Input_Signal_Theta = array.array('f',[])

## A boolean that control the start of a response test
Start = False

## A boolean that control data transmission
TRANSMIT_READY = False

## A boolean that control motor motion
MOTORS_ENABLED = False

## The data time stamps recored during the response test
Data_Tim = array.array('f',[])

## The omega ref values recored during the response test
Data_Omega_Ref = array.array('f',[])

## The measured omega values recored during the response test
Data_Omega = array.array('f',[])

## The theta ref values recored during the response test
Data_Theta_Ref = array.array('f',[])

## The measured theta values recored during the response test
Data_Theta = array.array('f',[])

## The duty signal values recored during the response test
Data_Duty = array.array('f',[])

## The current duty commanded by the controller [%]
duty = 0

## The current ref omega [rpm]
Omega_ref = 0

## The current measured omega [rpm]
Omega_meas = 0

## The current ref theta [deg]
Theta_ref = 0

## The current measured theta [deg]
Theta_meas = 0


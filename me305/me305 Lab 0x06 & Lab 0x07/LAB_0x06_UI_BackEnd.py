'''
@file LAB_0x06_UI_BackEnd.py

@author E. Nicholson

@date November 24, 2020

@brief      This file holds the Lab 0x06 back end UI Finite State Machine class

            The source code for this file can be found here: 
    
            https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305%20Lab%200x06%20%26%20Lab%200x07/LAB_0x06_UI_BackEnd.py
'''
from pyb import UART
import utime
import LAB_0x06_shares as shares
import array
        
class BackEndUIFSM:
    '''
    @brief      A finite state machine that handles the back end of the Lab 0x06 UI
    @details    This FSM recieves commands from the front end UI, and places the
                information it recieves in the common shares file. This FSM also
                handles the transmission of data upon the completion of a test.
    
    @image html lab0x06_BEUI_FSM.png
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT             = 0    
    
    ## Constant defining State 1 - Listen
    S1_LISTEN           = 1
    
    ## Constant defining State 2 - Transmit
    S2_TRANSMIT         = 2

    def __init__(self, ID, UART_ID, ref_rate, debug):
        '''
        @brief              Creates a recorder object
        @param ID           A string identifying the object for tracing output.
        @param UART_ID      The UART ID number that will be used for serial communication
        @param ref_rate     The refresh rate of the record task
        @param debug        A boolean that toggles the trace output on or off
        '''
        
        # ---- General Setup ----
        
        ##  The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ##  The unique ID string for the object, used for trace output
        self.ID = ID
        
        ## A boolean that toggles the trace output to the command line
        self.debug = debug
        
        ## UART object for handling simple serial communication
        self.uart = UART(UART_ID)
        
        ##  The refresh rate for the UI position in microseconds
        self.ref_rate = ref_rate
        
        # ---- Timing Configuration ----

        ##  The timestamp for the initialization of the object in microseconds
        self.start_time = utime.ticks_us()
               
        ##  The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.ref_rate)
        
    def run(self):
        '''
        @brief      Runs one iteration of the Back End UI task
        '''
        ## The current time at the start of an update
        self.curr_time = utime.ticks_us()
           
        if utime.ticks_diff(self.curr_time, self.next_time) > 0:
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                
                if self.debug:
                    self.trace()
                    print('INIT ENCREC Task')
                    
                self.transitionTo(self.S1_LISTEN)
            
            elif(self.state == self.S1_LISTEN):
                # Run State 1 Code   
                
                if self.debug:
                    self.trace()
                
                cmd = self.get_cmd()
                if self.debug:
                    print('Current command: ' + str(cmd))
                
                if cmd == None:
                    pass
                elif cmd[0:2] == b'SG':
                    if cmd[2:] == b'default':
                        pass
                    elif cmd[2:] == b'step':
                        shares.Signal_Type = 'step'
                    elif cmd[2:] == b'signal': 
                        shares.Signal_Type = 'signal'
                elif cmd[0:2] == b'KP':
                    if cmd[2:] == b'default':
                        pass
                    else:
                        shares.KP = float(cmd[2:])
                elif cmd[0:2] == b'KI':
                    if cmd[2:] == b'default':
                        pass
                    else:
                        shares.KI = float(cmd[2:])
                elif cmd[0:2] == b'OS':
                    if cmd[2:] == b'default':
                        pass
                    else:
                        shares.Omega_step = float(cmd[2:])
                elif cmd[0:2] == b'TT':
                    if cmd[2:] == b'default':
                        pass
                    else:
                        shares.Test_Time = float(cmd[2:])
                elif cmd[0:2] == b'SD':
                    if cmd[2:] == b'default':
                        pass
                    else:
                        shares.Step_Time = float(cmd[2:])
                elif cmd[0:2] == b'DP':
                    data_point = str(cmd[2:], 'utf-8').split(',')
                    shares.Input_Signal_Tim.append(float(data_point[0]))
                    shares.Input_Signal_Omega.append(float(data_point[1]))
                    shares.Input_Signal_Theta.append(float(data_point[2]))
                elif cmd == b'START':
                    shares.Start = True
                else:
                    print('Invalid Command')
                
                if shares.TRANSMIT_READY == True:
                    self.transitionTo(self.S2_TRANSMIT)
                    self.reset_data()
                    print('TRANSMIT')
                
                self.next_time = utime.ticks_add(self.next_time, self.ref_rate)
            
                        
            elif(self.state == self.S2_TRANSMIT):
                # Run State 2 Code     
                
                if self.debug:
                    self.trace()
                
                print('{:f},{:f},{:f},{:f},{:f},{:f}'.format(shares.Data_Tim[self.data_sent], 
                                      shares.Data_Omega_Ref[self.data_sent],
                                      shares.Data_Omega[self.data_sent],
                                      shares.Data_Theta_Ref[self.data_sent],
                                      shares.Data_Theta[self.data_sent],
                                      shares.Data_Duty[self.data_sent]))     
                      
                self.data_sent += 1
                
                if self.debug:
                    print('DATA SENT' + str(self.data_sent))
               
                if self.data_sent >= self.tot_data:
                    print('END')
                    print('Transmission complete')
                    self.transitionTo(self.S1_LISTEN)
                    shares.TRANSMIT_READY = False
                    shares.Step = False
                    shares.Data_Tim = array.array('f',[])
                    shares.Data_Omega_Ref = array.array('f',[])
                    shares.Data_Omega = array.array('f',[])
                    shares.Data_Theta_Ref = array.array('f',[])
                    shares.Data_Theta = array.array('f',[])
                    shares.Data_Duty = array.array('f',[])
                    shares.Input_Signal_Tim = array.array('f',[])
                    shares.Input_Signal_Omega = array.array('f',[])
                    shares.Input_Signal_Theta = array.array('f',[])

                self.next_time = utime.ticks_add(self.next_time, self.ref_rate)
                
                
            else:
                # Invalid state code (error handling)
                pass
            
    def get_cmd(self):
        '''
        @brief Reads the serial line and assigns a command
        @return cmd     A processed command  
        '''
        
        if self.uart.any() != 0:
            cmd = self.uart.read()
            # print(cmd)
        else:
            cmd = None
        return cmd
    
    def reset_data(self):
        '''
        @brief      Resets data transmitting parameters
        '''
        
        ## Running cound of the number of data points transmitted
        self.data_sent = 0
        
        ## Total number of points for transmittal
        self.tot_data = len(shares.Data_Tim)
        
        
    def trace(self):
        '''
        @brief  Prints a trace output when called
        '''
        print(self.ID + ' State: ' + str(self.state) + ' Time: ' + str(self.curr_time/1000000))

           
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
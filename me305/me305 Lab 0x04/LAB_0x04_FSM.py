'''
@file LAB_0x04_FSM.py

@author E. Nicholson

@date October 29, 2020

@brief      A file containing the encoder recorder class
            
@details    This file contains an encoder recorder finite-state-machine class.

            The source code for this file can be found here: 
    
            https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305%20Lab%200x04/LAB_0x04_FSM.py
'''
from pyb import UART
import utime
        
class ENCREC_FSM:
    '''
    @brief      An encoder recorder finite-state-machine
    @details    This class implements a FSM to capture recordings of encoder
                position. The machine waits for a command. Once it is recieved,
                the FSM creates time stamped encoder readings. Once the recoding
                complete, the data is transmitted over the serial line. The following
                image shows the state flow diagram for this FSM.
                
    @image html lab0x04_REC_FSM_stateflow.png
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT             = 0    
    
    ## Constant defining State 1 - Wait
    S1_WAIT             = 1
    
    ## Constant defining State 2 - Record
    S2_RECORD           = 2
    
    ## Constant defining State 3 - Transmit
    S3_TRANSMIT         = 3

    def __init__(self, ID, encoder, UART_ID, ref_rate, rec_rate, rec_per, debug):
        '''
        @brief              Creates a recorder object
        @param ID           A string identifying the object for tracing output.
        @param encoder      The encoder object.
        @param UART_ID      The UART ID number that will be used for serial communication
        @param ref_rate     The refresh rate of the record task
        @param rec_rate     The record rate of the record task
        @param rec_per      The recording period
        @param debug        A boolean that toggles the trace output on or off
        '''
        
        # ---- General Setup ----
        
        ##  The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ##  The unique ID string for the object, used for trace output
        self.ID = ID
        
        ## A boolean that toggles the trace output to the command line
        self.debug = debug
                
        ## The encoder object that the task interfeces with
        self.encoder = encoder
        
        ## UART object for handling simple serial communication
        self.uart = UART(UART_ID)
        
        ##  The refresh rate for the encoder position in microseconds
        self.ref_rate = ref_rate
        
        ##  The record rate for the encoder position in microseconds
        self.rec_rate = int(rec_rate*10**6)
                    
        ##  The recording period in microseconds
        self.rec_per = int(rec_per*10**6)
        


        # ---- Timing Configuration ----

        ##  The timestamp for the initialization of the object in microseconds
        self.start_time = utime.ticks_us()
        # self.start_time = time.time()
               
        ##  The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.ref_rate)
        # self.next_time = self.start_time + self.ref_rate
        
    def run(self):
        '''
        @brief      Runs one iteration of the encoder recorder task
        '''
        ## The current time at the start of an update
        self.curr_time = utime.ticks_us()
           
        if utime.ticks_diff(self.curr_time, self.next_time) > 0:
            
            cmd = self.get_cmd()
            if self.debug:
                print('Current command: ' + str(cmd))
                
            self.encoder.update()
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                
                if self.debug:
                    self.trace()
                    print('INIT ENCREC Task')
                    
                self.transitionTo(self.S1_WAIT)
            
            elif(self.state == self.S1_WAIT):
                # Run State 1 Code   
                
                if self.debug:
                    self.trace()
                
                if cmd == 'record':
                    
                    print('Recording position of encoder ' + self.encoder.ID +
                                    ' at ' + str((10**6)/self.rec_rate) + ' Hz for ' +
                                    str(self.rec_per/(10**6)) + ' seconds')
                    
                    ## The time that data collection began
                    self.data_start_time = utime.ticks_us()
                    self.clear_data()  
                    self.transitionTo(self.S2_RECORD)
                else:
                    pass
                
                self.next_time = utime.ticks_add(self.next_time, self.ref_rate)
            
            elif(self.state == self.S2_RECORD):
                # Run State 2 Code 
                
                if self.debug:
                    self.trace()
                
                ## Elapsed data recording time
                self.el_data_time = utime.ticks_diff(self.curr_time, self.data_start_time)
                
                self.data.append('{:f},{:f}'.format(self.el_data_time/(10**6), 
                                                     self.encoder.get_position()*25.71429))
                
                if cmd == 'save' or utime.ticks_diff(self.rec_per, self.el_data_time) < 0:
                    print('    Recording complete')
                    print('Data transmitting... Please wait')
                    print('TRANSMIT')
                    
                    ## The total number of data points collected
                    self.tot_data = len(self.data)
                    
                    if self.debug:
                        print('DATA LEN: ' + str(self.tot_data))
                    
                    ## The number of data points transmitted
                    self.data_sent = 0
                    self.transitionTo(self.S3_TRANSMIT)
                    self.next_time = utime.ticks_add(self.next_time, self.ref_rate)
                else:
                    self.next_time = utime.ticks_add(self.next_time, self.rec_rate)
                        
            elif(self.state == self.S3_TRANSMIT):
                # Run State 3 Code     
                
                if self.debug:
                    self.trace()
                

                print(self.data[self.data_sent])                    
                
                self.data_sent += 1
                
                if self.debug:
                    print('DATA SENT' + str(self.data_sent))
               
                if self.data_sent >= self.tot_data:
                    print('END')
                    print('Transmission complete')
                    self.transitionTo(self.S1_WAIT)
                    self.encoder.set_position()
                    
                self.next_time = utime.ticks_add(self.next_time, self.ref_rate)
                
            else:
                # Invalid state code (error handling)
                pass
            
    def get_cmd(self):
        '''
        @brief  Reads the serial line and assigns a command
        '''
        recieve = self.uart.readline()
        
        if recieve == b'g':
            cmd = 'record'
        elif recieve == b's':
            cmd = 'save'
        else:
            cmd = None
                
        return cmd
    
    def clear_data(self):
        '''
        @brief      Resets data recording list
        '''
        ## List for storing encoder data
        self.data = []
        
        
    def trace(self):
        '''
        @brief  Prints a trace output when called
        '''
        print(self.ID + ' State: ' + str(self.state) + ' Time: ' + str(self.curr_time/1000000))

           
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
'''
@file encoder.py

@author E. Nicholson

@date October 12, 2020

@brief      This file contains a class that acts as an encoder driver.

@details    This file contains the class 'encoder.' This class facilitates
            configuring a Nucleo - L476RG to interface with an encoder.The
            object that this class creates correctly tracks encoder position, and 
            recent position delta. The encoder position can also be zeroed.
            
            The source code for this file can be found here: 
    
            https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305%20Lab%200x03/encoder.py 
'''

import pyb

class encoder:
    '''
    @brief      An encoder driver object.
    @details    This class configures hardware, to monitor an encoder's position.
                The encoder ticks are counted using a timer object, and the 
                timer is queried to get updated position values. The diver class
                corrects timer overflow errors, and is able to handle encoder 
                rotation CW and CCW.
    '''

    def __init__(self, ID, Pin1, Pin2, tim_no, debug):
        '''
        @brief          Creates an encoder object
        @param ID       A string identifying the object for traceing output.
        @param Pin1     The pin object that will be bound to encoder channel A
        @param Pin2     The pin object that will be bound to encoder channel B
        @param tim_no   The timer number that will be used to read the encoder
        @param debug    A boolean that toggles the trace output on or off
        '''
        
        
        # ---- General Setup ----
        
        ##  The unique ID string for the object, used for trace output
        self.ID = ID
        
        ## A boolean that toggles the trace output to the command line
        self.debug = debug
        
        ##  The timer objct that is used to read the encoder. The prescaler is
        # set to zero and the period is set to 0xFFFF or 65535
        self.tim = pyb.Timer(tim_no, prescaler=0, period=0xFFFF)
        # print('Timer ' + str(tim_no) + ' initialized!')
        
        # This binds the two pins passed to the class to the first two channels
        # of the desired timer.
        self.tim.channel(1, pyb.Timer.ENC_AB, pin=Pin1)
        self.tim.channel(2, pyb.Timer.ENC_AB, pin=Pin2)
        # print('Channel 1 bound to ' + Pin1)
        # print('Channel 2 bound to ' + Pin2)

       
        # ---- Indexing & Counting ----

        ##  The current position of the encoder
        self.pos = 0

        ##  The previous position of the encoder
        self.prev_pos = 0
        
        ##  The difference between the current position and the previous position
        self.delta = 0
        
        # ---- Debug ----
        
        if self.debug:
            print('Encoder object created')
                
    def update(self):
        '''
        @brief      Updates the encoder
        @details    This function passes the position from the last call to the
                    prev_pos atribute, calculates a new delta value, corrects
                    the delta value, and updates the encoder position.
        '''
        
        self.prev_pos = self.pos
        
        self.delta = self.tim.counter() - self.prev_pos

        
        if self.delta > 0xFFFF/2:
            self.delta -= 0xFFFF
        elif self.delta < -0xFFFF/2:
            self.delta += 0xFFFF
        else:
            pass
        
        self.pos += self.delta 
        
        if self.debug:
            self.trace()
            
    def get_position(self):
        '''
	    @brief     Returns the encoder position
        '''
        return self.pos


    def set_position(self):
        '''
	    @brief     Sets the encoder position to a specified value. For Lab 3, hard coded to zero
        '''
        self.pos = 0
        self.prev_pos = 0
        self.tim.counter(0)
        
    def get_delta(self):
        '''
        @brief     Returns the current encoder delta
        '''
        return self.delta
    
    def trace(self):
        '''
        @brief  Prints a trace output when called
        '''
        print(self.ID + '\n'
              + 'Pos: ' + str(self.pos) + '\n'
              + 'Delta: ' + str(self.delta) + '\n')

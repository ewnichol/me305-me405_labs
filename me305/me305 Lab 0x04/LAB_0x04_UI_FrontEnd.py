'''
@file LAB_0x04_UI_FrontEnd.py

@author E. Nicholson

@date October 26, 2020

@brief      A file contains a class implementing a user interface finite-state-machine

@details    This file contains a class that implements a user interface for
            an encoder data recorder. The recorder runs on a Nucleo - L476RG.
            This file also has a conditional that runs the UI FSM when 
            used standalone. 
            
            The source code for this file can be found here: 
    
            https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305%20Lab%200x04/LAB_0x04_UI_FrontEnd.py
'''

import keyboard
import serial
import time
import matplotlib.pyplot as plt
import numpy as np


class FEUI_FSM:
    '''
    @brief      An user interface finite-state-machine
    @details    The UI sends commands the the Nucleo, recieves messages / data transmissions, 
                and processes the data. The data is recorded in a CSV and plotted.
                The following images shows a state flow diagram for the UI FSM.
                
    @image html lab0x04_UI_FSM_stateflow.png
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT             = 0    
    
    ## Constant defining State 1 - Command
    S1_COMMAND          = 1
    
    ## Constant defining State 2 - Recieve Transmit
    S2_RECIEVE_TRANSMIT = 2
    
    ## Constant defining State 3 - Process
    S3_PROCESS          = 3
    
    ## Constant defining State 4 - Exit
    S4_EXIT             = 4

    def __init__(self, ID, ref_rate, rec_rate, rec_per, debug):
        '''
        @brief              Creates a UI object
        @param ID           A string identifying the object for traceing output.
        @param ref_rate     The refresh rate of the record task
        @param rec_rate     The refresh rate of the record task
        @param ref_rate     The refresh rate of the record task
        @param debug        A boolean that toggles the trace output on or off
        '''
        
        # ---- General Setup ----
        
        print('UI Task Created')
        self.com_open()
        
        ##  The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ##  The unique ID string for the object, used for trace output
        self.ID = ID
        
        ## A boolean that toggles the trace output to the command line
        self.debug = debug
        
        ##  The refresh rate for the encoder position in microseconds
        self.ref_rate = ref_rate    

        ##  The sampling period for the encoder position in seconds
        self.rec_rate = rec_rate 
        
        ##  The recording period in seconds
        self.rec_per = rec_per
        
        ## Boolean describing if a recording is in progress
        self.recording = False
        
        # ---- Timing Configuration ----

        ##  The timestamp for the initialization of the object in microseconds
        self.start_time = time.time()
               
        ##  The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.ref_rate
        
    def run(self):
        '''
        @brief      Runs one iteration of the encoder recorder task
        '''
        ## The current time at the start of an update
        self.curr_time = time.time()
        if (self.curr_time - self.next_time) > 0:            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                if self.debug:
                    self.trace()
                    print('INIT')
                
                print('Press g to begin recording encoder position')
                self.transitionTo(self.S1_COMMAND)

            elif(self.state == self.S1_COMMAND):
                # Run State 1 Code         
                
                if self.debug:
                    self.trace()
                
                msg = self.recieve()
                
                if keyboard.is_pressed('g') and self.recording == False:
                    self.ser.write(str('g').encode('ascii'))
                    print('    command sent...')
                    print('Recording in process, press s anytime to save the results')
                    self.recording = True

                elif keyboard.is_pressed('s') and self.recording == True:
                    self.ser.write(str('s').encode('ascii'))
                    print('    command sent...')
                    self.recording = False
                else:
                    pass

                if msg == 'TRANSMIT':
                    self.clear_data()
                    self.transitionTo(self.S2_RECIEVE_TRANSMIT)
                elif msg:
                    print(msg)

            elif(self.state == self.S2_RECIEVE_TRANSMIT):
                # Run State 2 Code
                
                if self.debug:
                    self.trace()

                msg = self.recieve()
                if msg == 'END':
                    print(self.recieve())
                    self.transitionTo(self.S3_PROCESS) 
                else:
                    print(msg)
                    self.raw.append(msg)
                    data_point = msg.strip().split(',')
                    print()
                    self.tim = np.append(self.tim, float(data_point[0]))
                    self.pos = np.append(self.pos, float(data_point[1]))
                    
            elif(self.state == self.S3_PROCESS):
                # Run State 3 Code                 
                
                if self.debug:
                    self.trace()
                
                print('Processing Data')
                
                with open('data.csv','w') as f:
                    for i in range(len(self.raw)):
                        f.write(self.raw[i] + '\n')
                    print('CSV Created')
                
                plt.plot(self.tim,self.pos)
                plt.xlabel('Time [sec]')
                plt.ylabel('Encoder Position [deg]')
                
                plt.figure()
                
                print('Ooh look at the pretty plot')
                
                if input('End Task? y/n: ') == 'y':
                    self.com_close()
                    self.transitionTo(self.S4_EXIT)
                    print('You may now close the program')
                    
                else:
                    print('Press g to begin recording encoder position')
                    self.transitionTo(self.S1_COMMAND)
                    
            elif(self.state == self.S4_EXIT):
                # Run State 4 Code                 
                
                if self.debug:
                    self.trace()

            else:
                # Invalid state code (error handling)
                pass
            
            self.next_time = time.time() + self.ref_rate
            
    def recieve(self):
        '''
        @brief  Reads a message from the Nucleo and removes special characters
        '''
        msg = self.ser.readline().decode('ascii')
        return msg.strip()
    
    def com_open(self):
        '''
        @brief  Opens the serial communication line
        '''
        ## Serieal communication line
        self.ser = serial.Serial(port='COM3',baudrate=115373,timeout=1)
        print('Serial Line Opened')
        
    def com_close(self):
        '''
        @brief  Closes the serial communication line
        '''
        self.ser.close()
        print('Serial Line Closed')

    def clear_data(self):
        '''
        @brief      Resets data recording paramaters
        @details    This function emptys all data arrays/lists and resets the
                    data counter.
        '''
        ## list for storing raw data dump
        self.raw = []
        
        ## Array for storing encoder data
        self.pos = np.array([], dtype=np.float32)
        
        ## Array for storing encoder data
        self.tim = np.array([], dtype=np.float32)
        
        ## Current number of data points recorded
        self.curr_data = 0        

    def trace(self):
        '''
        @brief  Prints a trace output when called
        '''
        print(self.ID + ' State: ' + str(self.state) + ' Time: ' + str(self.curr_time))

           
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
        if self.debug:
            print('----------------')

if __name__ == '__main__':    
    '''
    This block of cose allows this fine to run the UI FSM stand alone
    '''
    
    ## An encoder finite state machine. It is constructed using an encoder object and a refresh rate in seconds
    task1 = FEUI_FSM('FSM1', .00001, .2, 10, False)
    
    
    # Run the tasks in sequence over and over again
    while True:
        task1.run()

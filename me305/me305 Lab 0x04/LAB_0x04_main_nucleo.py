'''
@file LAB_0x04_main_nucleo.py

@author E. Nicholson

@date October 26, 2020

@brief      Configures and runs the encoder recorder finite-state-machine

@details    This file creates an encoder driver object and passes it to an
            encoder recorder. The recorder is then run indefinitely, 
            keeping the encoder updated, and communicating with an user 
            interface on a PC.
            
            The source code for this file can be found here: 
    
            https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305%20Lab%200x04/LAB_0x04_main_nucleo.py
'''
from encoder import encoder
import LAB_0x04_FSM
import pyb

# Configuration 

## Configures the pin for encoder channel A
pinA6 = pyb.Pin(pyb.Pin.cpu.B6)

## Configures the pin for encoder channel B
pinA7 = pyb.Pin(pyb.Pin.cpu.B7)

## Represents an encoder object. This object is created by passing two pins and a timer number
myencoder = encoder('E1', pinA6, pinA7, 4, False)


## An encoder finite state machine. It is constructed using an encoder, an UART_ID number, 
# a refresh rate in microseconds, a record rate in seconds, and a recording period in seconds
task1 = LAB_0x04_FSM.ENCREC_FSM('ERT1', myencoder, 2, 1000, .2, 10,  False)

# Run the tasks in sequence over and over again
while True:
    task1.run()

'''
@file LAB_0x02_main.py

@author E. Nicholson

@date October 12, 2020

@brief      This uses FSM_LED_pattern.py to control a virtual LED and a physical LED on a Nucleo - L476RG.

@details    This file first configures the hardware needed to control the 
            physical LED. It sets up a Pin object connected to the on board 
            user LED. It then creates a timer object, and links it to the 
            user LED in pulse width modulation mode. Using finite state 
            machines contained within FSM_LED_pattern.py, two tasks are created.
            One representing the virtual LED the other representing the physical
            LED. Finally, the file runs both tasks in 'while True:' loop.
            
            The source code for this file can be found here: 
    
            https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305%20Lab%200x02/LAB_0x02_main.py
'''

from FSM_LED_pattern import virtual_LED_task
from FSM_LED_pattern import physical_LED_task
import pyb

## Configuring the on board user LED on the hardware
pinA5 = pyb.Pin(pyb.Pin.cpu.A5)

## Initialize the timer that will control the LED through pulse width modulation
tim2 = pyb.Timer(2, freq = 20000)

## Passes the LED to the timer and sets up PWM
phys_LED = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)

## Creating a virtual_LED object, specifying the ID, interval [sec], Debug
task1 = virtual_LED_task('V1', .5, False)

## Creating a phyisical_LED object using the pin and timer objects above, specifying the ID, 
#  total pattern interval [sec], sine duty cycle [decimal], Debug
task2 = physical_LED_task('P1', phys_LED, 20, 0.5, 1, 2, False)

# Run the tasks in sequence over and over again
while True:
    task1.run()
    task2.run()

"""
@file       mytest.py
@brief      A test for my Nucleo.
@details    This file is used to test passing scripts and running them on my
            Nucleo hardware.
            
@author     E. Nicholson
"""

for idx in range(10):
    print(10-idx)
print('Hello, Nucleo World!')

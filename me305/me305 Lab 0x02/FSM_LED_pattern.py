'''
@file FSM_LED_pattern.py

@author E. Nicholson

@date October 12, 2020

@brief      This file implements a finite-state-machine to control the user LED on a Nucleo - L476RG.

@details    This file contains two classes representing two different finite
            state machines. The first, 'virtual_LED,' allows the user to specify
            the rate, in seconds, at which the console prints 'LED ON' then 
            'LED OFF.' The second, 'physical_LED,' can be used to control an 
            actual LED. This FSM first flashes a SINE brightness pattern followed
            by a SAW brightness pattern. The total cycle time, sine duty cycle, 
            and individual pattern period can be controlled.
            
            The source code for this file can be found here: 
    
            https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305%20Lab%200x02/FSM_LED_pattern.py
'''

import utime
from math import sin, pi

class virtual_LED_task:
    '''
    @brief      A finite state machine to control a virtual LED.
    @details    This class implements a finite state machine to 'blink' a
                virtual LED at a fixed, user specified interval.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT     = 0    
    
    ## Constant defining State 1
    S1_ON       = 1    
    
    ## Constant defining State 2
    S2_OFF      = 2

    def __init__(self, ID, interval, debug):
        '''
        @brief            Creates a virtual_LED_task object.
        @param ID         A string identifying the object for traceing output
        @param interval   Defines task timing interval
        @param debug      A boolean that toggles the trace output on or off
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The unique ID string for the object, used for trace output
        self.ID = ID
        
        ##  The amount of time in milliseconds between runs of the task
        self.interval = int(interval*1000)

        ## A boolean that toggles the trace output to the command line
        self.debug = debug

        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_ms()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        
        ## Captures the current time
        self.curr_time = utime.ticks_ms()
        if utime.ticks_diff(self.curr_time, self.next_time) > 0:
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                print('INIT')
                if self.debug:
                    print(self.ID + ': State 0')
                self.transitionTo(self.S1_ON)
            
            elif(self.state == self.S1_ON):
                # Run State 1 Code
                print('LED ON')
                if self.debug:
                    print(self.ID + ': State 1')
                self.transitionTo(self.S2_OFF)
            
            elif(self.state == self.S2_OFF):
                # Run State 2 Code
                print('LED OFF')                
                if self.debug:
                    print(self.ID + ': State 2')
                self.transitionTo(self.S1_ON)
            
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState

class physical_LED_task:
    '''
    @brief      A finite state machine to control the Nucleo user LED.
    @details    This class implements a finite state machine to control the
                on board user LED on a Nucleo - L476RG. When this class is used
                the on board LED will display a 'pattern_cycle' consisting of
                a SINE wave brightness pattern followed by a SAW brightness
                pattern. The user can specify how long the pattern cycle is,
                what percent of the cycle is a SINE pattern, and what the periods
                of the SINE and SAW patterns are. Both patterns are generated using
                a common index. I decided on 0-345 in steps of 15 (24 total
                items). I operate on the current index differently depending on
                what pattern state the FSM is in.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT          = 0    
    
    ## Constant defining State 1
    S1_SINE          = 1    
    
    ## Constant defining State 2
    S2_SAW           = 2

    def __init__(self, ID, phys_LED, cycle_time, duty, SINE_pattern_period, SAW_pattern_period, debug):
        '''
        @brief                          Creates a physical_LED_task object
        @param ID                       A string identifying the object for traceing output.
        @param cycle_time               Total time [sec] for one pattern cycle
        @param duty                     Defines the percentage of the total cycle time that the SINE pattern runs
        @param SINE_ pettern_period     Defines the period for the SINE pattern [sec]
        @param SAW_ pettern_period      Defines the period for the SAW pattern [sec]
        @param debug                    A boolean that toggles the trace output on or off
        '''
        
        # ---- General Setup ----
        
        ##  The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ##  The unique ID string for the object, used for trace output
        self.ID = ID
        
        ## A boolean that toggles the trace output to the command line
        self.debug = debug
        
        ##  The timer objct that has been pre configured to a specific channel and LED
        self.LED = phys_LED

        # ---- Indexing & Counting ----

        ##  The starting value for my index
        self.pattern_idx = 0

        ##  The amount that I want my pattern index to advance each iteration
        self.pattern_idx_delta = 15
        
        ##  Counter that describes the number of times the task has run
        self.runs = 0

        # ---- Timing Configuration ----

        ##  The total time in milliseconds for one full pattern cycle
        self.cycle_time = int(cycle_time)*1000

        ##  The amount of time in milliseconds that the SINE pattern will run
        self.SINE_cycle = int(cycle_time*duty)*1000

        ##  The period of time in milliseconds for one SINE pattern repetition
        self.SINE_pattern_period = int(SINE_pattern_period*1000)

        ##  The period of time in milliseconds for one SAW pattern repetition
        self.SAW_pattern_period = int(SAW_pattern_period*1000)
        
        ##  SINE timing interval, desired pattern period divided by the total number of values in my index
        self.SINE_interval = int(self.SINE_pattern_period/24)

        ##  SAW timing interval, desired pattern period divided by the total number of values in my index
        self.SAW_interval = int(self.SAW_pattern_period/24)

        ##  The timestamp for the first iteration
        self.start_time = utime.ticks_ms()
        
        ##  Initializes cycle start time to first cycle values
        self.cycle_start_time = self.start_time
        
        ##  Initializes elapsed cycle time to zero
        self.el_cycle_time = 0
        
        ##  The "timestamp" for when the task should run next, configured to start with the SINE pattern
        self.next_time = utime.ticks_add(self.start_time, self.SINE_interval)
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        
        ## Captures the current time
        self.curr_time = utime.ticks_ms()
        
        ## Total elapsed time, used for trace output
        self.el_time = utime.ticks_diff(self.curr_time, self.start_time)        
        self.el_cycle_time = utime.ticks_diff(self.curr_time, self.cycle_start_time)
        
        if utime.ticks_diff(self.curr_time, self.next_time) > 0:
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                print('INIT')
                if self.debug:
                    print(self.ID + ': State 0')
                self.transitionTo(self.S1_SINE)
            
            elif(self.state == self.S1_SINE):
                # Run State 1 Code
                
                # if needed, the pattern index is reset
                if self.pattern_idx == 360:
                    self.pattern_idx = 0
                
                # calculates the brightness and updates the LED
                self.updateLED(self.LED)
                
                if utime.ticks_diff(self.el_cycle_time, self.SINE_cycle) > 0:
                    self.transitionTo(self.S2_SAW)
                    self.pattern_idx = 360
                    self.next_time = utime.ticks_add(self.next_time, self.SAW_interval)
                else:
                    self.next_time = utime.ticks_add(self.next_time, self.SINE_interval)
                    # advances the index
                    self.pattern_idx += self.pattern_idx_delta
                
                if self.debug:
                    print(self.ID + ': State 1 --> State ' + str(self.state) 
                          + ' | Time: ' + str(self.el_time/1000)
                          + ' | Cycle Time: ' + str(self.el_cycle_time/1000))
                    if self.pattern_idx == 360:
                        print('--- PERIOD ---')

            
            elif(self.state == self.S2_SAW):
                # Run State 2 Code
                
                # if needed, the pattern index is reset
                if self.pattern_idx == 360:
                    self.pattern_idx = 0
                
                # calculates the brightness and updates the LED
                self.updateLED(self.LED)                

                
                # advances the index
                self.pattern_idx += self.pattern_idx_delta
                
                if utime.ticks_diff(self.el_cycle_time, self.cycle_time) > 0:
                    self.transitionTo(self.S1_SINE)
                    self.pattern_idx = 360
                    self.el_cycle_time = 0
                    self.cycle_start_time = utime.ticks_ms()
                    self.next_time = utime.ticks_add(self.next_time, self.SINE_interval)
                else:
                    # advances the index
                    self.pattern_idx += self.pattern_idx_delta
                    self.next_time = utime.ticks_add(self.next_time, self.SAW_interval)
                
                if self.debug:
                    print(self.ID + ': State 2 --> State ' + str(self.state) 
                          + ' | Time: ' + str(self.el_time/1000)
                          + ' | Cycle Time: ' + str(self.el_cycle_time/1000))
                    if self.pattern_idx == 360:
                        print('--- PERIOD ---')
            
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1


    def calcbright(self):
        '''
        @brief      Calculates the correct brightness value for the current index and state.
        @details    This function checks the current state and then calculates
                    a brightness value using the current index. The index is
                    passed through a function that yeilds the correct pattern.
        @returns    Returns brightness value
        '''
        if self.state == self.S1_SINE:
            brightness = sin(pi*self.pattern_idx/180)*50+50
        elif self.state == self.S2_SAW:
            brightness = 100*self.pattern_idx/345
        else:
            brightness = 0
        
        return brightness

    def updateLED(self, LED):
        '''
        @brief      Changes an LED's brightness
        @detials    This function takes a timer object that is bound to a physical LED
                    and changes the PWN percentage to a specified amount.
        '''
        self.LED.pulse_width_percent(self.calcbright())
        
        
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        




























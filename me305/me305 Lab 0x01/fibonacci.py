'''
@file fibonacci.py

@author E. Nicholson

@date September 28, 2020

@brief This file contains the fibonacci calculation function, and user inteface (UI) code.

@details    This file behaves differently based on how it is used. If it is run stand 
alone, the UI code  prompts the user for an input and checks that the input
is valid. If the file is imported as a module, the function fib can be used
without the UI code. The function had an internal input check to ensure that
the index passed to fib() is valid. The source code for this file can be found
here: 
    
https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305%20Lab%200x01/fibonacci.py
            

'''


def fib(idx):    
    '''
    @brief  Fibonacci function documentation
    
    @details    This function takes a positive interger input and returns the 
    Fibonacci number coresponding to the given index. The first part of the 
    function is a try-except statement designed to catch any invalid inputs if 
    the funtion is called as a module with an idx argument that is not a 
    positive interger. The next three conditionals handle the fibonacci 
    calculation and output. The first two handle the 0 and 1 index cases. The 
    third conditional calculates the fibonacci number for all indices greater 
    than one.
    
    @param idx This is the index of the desired fibonacci number.
    '''
    
    try:
        assert(type(idx) == int)
        assert(idx >= 0), 'Your input must be an interger greater than '
        'or equal to zero.'
    except:
        print('This is not a valid input!')
        return None
            
    print ('Calculating Fibonacci number at '
       'index n = {:}.'.format(idx))
    
    if idx == 0:
        print('        The Fibonacci number at your index is 0')
    if idx == 1:
        print('        The Fibonacci number at your index is 1')
    if idx >= 2:
        
        ## This is the satarting seed of the fibonacci sequence. As the calcultion progresses seq is appended with additional fibbonacci numbers.
        seq = [0, 1]
        
        ## This is a counter used to track if the desired index has been reached.
        i = 2
    
        while i <= idx:
            seq.append(seq[i-1] + seq[i-2])
            i += 1
        print('        The Fibonacci number at your '
              'index is {:}.'.format(seq[idx]))
       
'''
User Interface Code
This conditional is configured to run only if fibonacci.py is run directly.
If this is the case, the user will be prompted to enter their desired
fibonacci index. The try-accept statement checks that the user's input is a
positive interger. If not, the input is rejected and the user is prompted
again. Once the input passes, the loop is broken, and fib(idx) is run using
the user's input.
'''
if __name__ == '__main__':    
    while True:
        try:
            ## This allows the user testing the program to enter different fibonacci indices.
            index = int(input('Input desired Fibonacci index: '))
            assert(index >= 0), 'Your input must be an interger greater '
            'than or equal to zero.'
            break
        except:
            print('This is not a valid input!')
    fib(index)
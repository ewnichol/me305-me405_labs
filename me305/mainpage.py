'''
@file   mainpage.py
@brief  This page contains a full list of projects for ME 305 with links to further documentation.

@mainpage

@section sec_intro Introduction & Links
@author Enoch Nicholson

This section of the documentation portfolio showcases all of the work completed
in ME 305. During Fall Quarter 2020.

This documentation fully captures the following assignments:
    
    Homeworks:
        * \ref pg_hw0
        
    Labs:
        * \ref pg_lab1
        * \ref pg_lab2
        * \ref pg_lab3
        * \ref pg_lab4
        * \ref pg_lab5
        * \ref pg_lab6
        * \ref pg_lab7

To return to the ME 305 - ME 405 Landing Page use this link:
    
https://ewnichol.bitbucket.io/

The mainpage file for this documentation can be found here: 
    
https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305/mainpage.py

@page pg_hw0 HW 0x00: Cooperative Multitasking- Finite State Machines
@author E. Nicholson
@date October 6, 2020

This homework asignment was an introduction to finite-state-machines (FSM). 
By modifying the windsheild wiper FSM code that we developed in lecture, we designed
a FSM to control a two-floor elevator. The elevator has two buttons, two
and two proximity sensors, and one motor. All three of these object types were
implemented wth psudo-code to test functionality.

The following is the state transition diagram for the elevator FSM.

@image html hw0x00_stateflow.png

Two files were created for this assignemnt. The source code for these files can be found here:
    * HW_0x00_main.py: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305/Homework/HW_0x00_main.py
    * FSM_Elevator.py: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305/Homework/FSM_Elevator.py

@page pg_lab1 Lab 0x01: Fibonacci
@author E. Nicholson
@date September 29, 2020

Lab 1 was designed to introduce coding in python and working with doxygen. 
Our task was to create a fibonacci calculator that could return the fibonacci 
number coresponding to a given idex. For more detailed information see the \ref fibonacci package.

One file was created for this assignemnt. The source code for this file can be found here:
    * fibonacci.py: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305/me305%20Lab%200x01/fibonacci.py
    
@page pg_lab2 Lab 0x02: Getting Started with Hardware
@author E. Nicholson
@date October 12, 2020

Lab 2 was designed to introduce hardware interfacing and to reinforce our 
understanding of Finite-state-machines. Our task was to create two FSMs, one 
that controls a virtual LED (a print statement), another to control a physical LED.
The virtual LED was required to blink at a fixed rate. The physical LED was required
to display a pattern cycle. I created mine to show a sine wave then a sawtooth
wave.

The two bigest challenges were generating the phsical LED patterns, and correctly timing all events.
For the pattern, I chose to use a single index that I could operate on to get 
different outputs. My index was 0-345 in steps of 15. The following plot shows three periods of
the signals that were generated.

@image html lab0x02_signals.png

To create the sine pattern I used:
        
    brightness = sin(index)*50 + 50

To create the sawtooth pattern I used:
        
    brightness = (index/345)*100

The timing was achieved using four user specified values, and by tracking time.
The user must select the following:
    * cycle_time: how long the sine-saw pattern will run [sec]
    * duty: what percent of the cycle time will be the sine pattern
    * SINE_pattern_period: sine wave period [sec]
    * SAW_pattern_period: sawtooth period [sec]

These values are processed to produce three values:
    1. SINE_cycle: numebr of millisecons that the sine pattern should run per cycle
    2. SINE_interval: number of milliseconds between each brightness transition in the sine pattern
    3. SAW_interval: number of milliseconds between each brightness transition in the sine pattern

By tracking the current elapsed time and current cycle time. I was able to schedule
the SINE-SAW transition by comparing current cycle time to SINE_cycle. I was also able
to reset the pattern once the current cycle time exceeded its limit. The two intervals
that I made were used to evenly space the 24 brightness transitions within each
pattern period.

I created state flow diagrams for both finite-state-machines. The following is
the diagram for the virtual LED FSM. This simpleFSM has two states. All of the 
transitions are 'Always' and no actions are performed on transition.

@image html lab0x02_vLED_stateflow.png

The state flow diagram for the physical LED FSM was a slightly more complicated. 
There are still only two states, but transitions have meaningful logic and actions.
The S0_INIT state always transitions to S1_SINE after configuring the FSM. Once in
sine state, the machine stays there, performing 'self transitions' until the elapsed
cycle time exceeds the allowed sine cycle time. S2_SAW behaves almost identically
to S1_SINE. The only differences are that cycle time is used instead of sine cycle
time and cycle time is reset upon trasition back to S1_SINE.

@image html lab0x02_pLED_stateflow.png

Two files were created for this assignemnt. The source code for these files can be found here:
    * LAB_0x02_main.py: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305/me305%20Lab%200x02/LAB_0x02_main.py
    * FSM_LED_pattern.py: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305/me305%20Lab%200x02/FSM_LED_pattern.py

@page pg_lab3 Lab 0x03: Incremental Encoders
@author E. Nicholson
@date October 19, 2020

In Lab 3, we created software that interfaced the 14 PPR encoder on a310 rpm gearmotor. An 
encoder driver class was used to configure a Nucleo - L476RG to read the encoder 
and to track position. This driver was written to be able to handle overflows 
and underflows of the hardware timer. Two finite-state-machines were used to finalize the
implementaion the functionality required by the lab. One FSM was dedicated to updating
the encoder object. The other was used to create a simple user interface. This
allowed a user to request data stored within the encoder object using the serial
console.

Since our encoder to handle overflows and underflows of the timer counter. To
do this, the position tracked by the encoder is incremented using the delta between 
the current timer count and the previous position. This allows our software to check
and correct the delta before incrementing encoder position. If our software sees a
delta magnitude greater than one half of the full timer count, the program corrects
the delta by adding or subtracting one full timer period. 

Both FSM are currently set to run with a refresh rate of 10 ms. The encoders 
that we are using for testing are fairly coarse (14 PPR), and it is attached to 
a realtively slow motor (600 RPM). Since we are using a 16 bit counter, we need
update our encoder, at minimum, every 24 seconds to avoid missing counts. Because
we use a UI to read data from the encoder, a refresh rate of 24 seconds is too long.
So the refresh rate was set far faster to enable the UI to always get current data.

This image shows the state flow diagram for the encoder task FSM:

@image html lab0x03_encoder_FSM_stateflow.png

This image shows a detailed state flow diagram for the User Interface FSM. All
the conditional actions contained within the RUN state are shown as self transitions. 

@image html lab0x03_UI_FSM_stateflow.png

To communicate between the encoder task and the UI task, arguments were added to
the run method of the UI class. These arguments were set up to recieve the three
methods from the encoder driver class that return encoder data and zero position.
This method of sharing scales far better than using global variables. For larger programs,
using a shared data file may be a better method to exchange information.

Three files were created for this assignemnt. The source code for these files can be found here:
    * LAB_0x03_main.py: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305/me305%20Lab%200x03/LAB_0x03_main.py 
    * LAB_0x03_FSM.py: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305/me305%20Lab%200x03/LAB_0x03_FSM.py 
    * encoder.py: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305/me305%20Lab%200x03/encoder.py 

@page pg_lab4 Lab 0x04: Extending Your Interface
@author E. Nicholson
@date November 1, 2020

Lab 4 built on the work we completed in lab 3. This lab's focus was
on extending the simple UI we created. We used our encoder driver with a new UI
system to record and plot encoder position. The UI we created ran locally on our
machine instead of on the Nucleo. We were required to communicate with our hardware
over the serial port to send commands and recieve data.The Nucleo side software 
consisted of the encoder driver we created in lab 3, a finite state machine that 
facilitated data recoding, and a main file that configured hardware and ran 
the FSM. On the pc, a single file implemented and ran the user interface FSM. 
This image shows the state flow diagram for the Nucleo based FSM:

@image html lab0x04_REC_FSM_stateflow.png

While the machine runs, it waits for the command to initiate a recording sequence. Once this 
command is recieved, it begins recording. The recording period and sampling rate
are set in the Nucleo main file. The recording stops when the recoring period completes
or when a commond to stop recording is recieved. Upon completion of a recoring,
the Nucleo sends all of the data over the serial port and returns to the beginning.
This next image shows a detailed state flow diagram for the User Interface FSM.  

@image html lab0x04_UI_FSM_stateflow.png

This FSM sends user commands to the Nucleo in the command state. Once the PC 
recieves a message stating that a transmission is available, the FSM transitions
to read the data. Once all of the data is collected, it is plotted using matplotlib
and saved permanently in a CSV file. Once a recording cycle has completed, the
user has the option to exit or continue. Exiting closes the serial port, while
continuing returns to the command state.

The following task diagram summarizes the communication that occurs between both FSMs. 


@image html lab0x04_TaskDiagram.png

Four files were used for this assignemnt. The source code for these files can be found here:
    * LAB_0x04_main_nucleo.py: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305/me305%20Lab%200x04/LAB_0x04_main_nucleo.py 
    * LAB_0x04_FSM.py: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305/me305%20Lab%200x04/LAB_0x04_FSM.py
    * LAB_0x04_UI_FrontEnd.py: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305/me305%20Lab%200x04/LAB_0x04_UI_FrontEnd.py
    * encoder.py: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305/me305%20Lab%200x04/encoder.py
    
@page pg_lab5 Lab 0x05: Use Your Interface
@author E. Nicholson
@date November 9, 2020

Lab 5 required us to use bluetooth communication to control our hardware. This
allowed us to remotely send commands using a smartphone. To visualize the effects
of these commands, we used the Nucleo's onboard LED. I chose to configure mine 
to display five different states: OFF, ON, BLINK, SINE, and SAW. The last three
can be displayed at any frequency between 0.001 HZ and 10 Hz. MIT's app inventor 
was used to create the front end UI. I used the example app as a base, and added
for the additional patterns that I wanted to disply. To control frequency, I included
a slider. The following image shows the display screen of my app.

@image html Screenshot_20201110-073054.png

The status readouts at the top of the screen display the commands that the app is 
sending to the Nucleo. The log at the bottom of the screen displays the status
messages from the Nucleo. I inclued both to ensure that the app and Nucleo were
synced correctly.

On the nucleo, I use an LED driver to control the LED's brightness. This driver
is maintained by a finite state machine that communicates with the mobile app.
The FSM passes the desired state information to the LED driver, and varries the rate
at which the driver is updated based on the frequency sent by the mobile app.
The UI FSM refreshes at a rate set in the main file. This rate does not change. 
Instead, the frequency at which the LED is updated is set by frequency commands
from the app. The following image shows the state flow diagram for the UI FSM.

@image html lab0x05_UI_FSM_stateflow.png

This FSM primarily waits in S2_UPDATE. When characters are waiting in the buffer,
the FSM transitions to S1_COMMAND state and reads the information. If it 
recieves a valid command (state change or frequency cahnge) the LED driver and 
update rate are adjusted. The FSM then sends a status and returns to S2_UPDATE. 
The following task diagram summarizes the communication that occurs between the
two halves of the UI.

@image html lab0x05_TaskDiagram.png

Three python files an one app inventor file were used for this assignemnt. The source code for these files can be found here:
    * LAB_0x05_main.py: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305%20Lab%200x05/LAB_0x05_main.py
    * LAB_0x05_UI_BackEnd_FSM.py: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305/me305%20Lab%200x05/LAB_0x05_UI_BackEnd_FSM.py
    * LAB_0x05_userLED_driver.py: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305/me305%20Lab%200x05/LAB_0x05_userLED_driver.py
    * ME305_LAB0x05_UI_EWN.apk: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305/me305%20Lab%200x05/ME305_LAB0x05_UI_EWN.apk

@page pg_lab6 Lab 0x06: DC Motors
@author E. Nicholson
@date November 24, 2020

The goal of this lab asignment was velocity regulation on one of the
brushed DC motors included with the ME 305-405 hardware kit. We were tasked with
creating motor driver that could be used to set a DC motor's % effort to a given
value between -100% and 100%. We then had to create a controller that used this 
driver and the encoder driver we created previously to regulate the motor's speed.
I began to tackle this lab by laying out the behavior I needed using a task 
diagram. The following image shows the diagram I created.
@image html lab0x06_TaskComDiag.png

Most of the tasks/files for this lab used my previous work as a starting point.
The only completely new files were the motor driver and the closed loop driver.
My lab uses a total of four finite state machines. One operates on my PC, and 
three operate on the Nucleo. The first of the FSMs implements the front end of
the user interface. It accepts inputs from a user, screens them, and sends them
the the Nucleo. The inputs entered into the front end are controller test parameters
like controller gains and test signal shape. This FSM also recieves and processes
the test results from the Nucleo. It functions very similarly to the front end UI
used in Lab 0x04. The largest difference is that this FSM does not operate cooperatively.
I elected to get user input using the input command instead of the keyboard module.
I felt that it was a cleaner / more responsive approach. Since there is only one 
task running on my PC, it can afford to be non-cooperative. The following image 
shows the state flow diagram for this FSM:
@image html lab0x06_FEUI_FSM.png

The next FSM is the Nucleo side (back end) of the user interface. It revieves
transmissions from the front end, and updates the shared data file with the 
information. Once a test is complete, this FSM transmits the results back to the
front end. The basic structure of this FSM was taken from Lab 0x04's back end.
I used the recorder FSM as a basis for all three nucleo side tasks. This FSM uses
the communication functionality that built for Lab 0x04. The following image 
shows the state flow diagram for this FSM:
@image html lab0x06_BEUI_FSM.png

The data recording functionality from Lab 0x04 was packaged into the Signal 
Response finite state machine. This FSM reads the test parameters from the shared
data file and generates an appropriate test signal. This signal is output in real time
to the shared data file. This FSM also records the controller's response to the
signal. The following image shows the state flow diagram for the Signal response FSM:
@image html lab0x06_SigTest_FSM.png

The final FSM handles motor control. This machine handles three different drivers:
encoder, motor, and controller. When the signal response FSM begins a test, the contoller
FSm turns on. It recieved the desired controller gains and passes them to the controller
driver. One running, the FSM tracks motor speed using the encoder driver. It 
compares this speed to the current desired refference speed in the shared data file.
It then passes these falues to the controller driver and recieves a control signal.
This control signal is a %effort value for the motor. Thi controll FSM then sends this
duty percentage to the motor driver. The following image shows the state flow diagram
for the controlelr FSM:
@image html lab0x06_Control_FSM.png

Once all of these tasks were working together correctly, I began tuning my PI controller.
The following table outlines the tuning process that I used.
@image html lab0x06_Step_Resp_Log.png

My first KP values was not large enough. This resulted in a large steady state
error. To correct this, I increased KP.
@image html lab0x06_Step_Resp_1.png

The next test was much better, but It slightly over shot the desired speed. Because
of this I desided to lower KP for my third test.
@image html lab0x06_Step_Resp_2.png

The final P-Controller was not quite as good as the second one, but it did not
overshoot the step. I chose to keep KP at the third test value and add I-Control
to adress the steady state error.
@image html lab0x06_Step_Resp_3.png

The first PI controller did a very good job. It reached the set point with almost no overshoot,
and its oscillations about the set point were minimal. For the final controller,
I decided to drop KP and increase KI. I felt that this could slightly improve the
undershoot that occurs just after the initial reaction.
@image html lab0x06_Step_Resp_4.png

The final response showd that this worked well. Test 5 was only a slight improvement
over test 4, but it was the best controller.
@image html lab0x06_Step_Resp_5.png

Nine python files were created so far for this assignemnt. The source code for these files can be found here:
    * LAB_0x06_main.py: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305/me305%20Lab%200x06%20%26%20Lab%200x07/LAB_0x06_main.py
    * LAB_0x06_shares.py: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305/me305%20Lab%200x06%20%26%20Lab%200x07/LAB_0x06_shares.py
    * LAB_0x06_UI_FrontEnd.py: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305/me305%20Lab%200x06%20%26%20Lab%200x07/LAB_0x06_UI_FrontEnd.py
    * LAB_0x06_UI_BackEnd.py: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305/me305%20Lab%200x06%20%26%20Lab%200x07/LAB_0x06_UI_BackEnd.py
    * LAB_0x06_Control_FSM.py: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305/me305%20Lab%200x06%20%26%20Lab%200x07/LAB_0x06_Control_FSM.py
    * signal_test.py: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305/me305%20Lab%200x06%20%26%20Lab%200x07/signal_test.py
    * m_driver.py: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305/me305%20Lab%200x06%20%26%20Lab%200x07/m_driver.py
    * enc_driver.py: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305/me305%20Lab%200x06%20%26%20Lab%200x07/enc_driver.py
    * cl_driver.py: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305/me305%20Lab%200x06%20%26%20Lab%200x07/cl_driver.py
    
@page pg_lab7 Lab 0x07: Reference Tracking
@author E. Nicholson
@date December 3, 2020

The final lab for ME 305 required us to add a signal tracking function to the 
contol code that we developed in Lab 0x06. No major structural revisions were 
needed in the Lab 0x06 code. The task communication digram and all of the FSMs
remained identical. Please see the Lab 0x06 page for these diagrams (\ref pg_lab6).

I elected to transmit the refference CSV from my PC to the Nucleo. I felt that this
would be a good way to practice sending batch data from PC to Nucleo. To send the data,
I used the existing send-recieve functionality that I already created for
Lab 0x06. Since the given refference csv has far more data points than I needed, 
I configured the front end FSM to send a signal values spaced out by 0.05s. 
This reduced the number of data points by about 50 times and did not harm the
refference signal quality.

Once The Nucleo could recieve a refference signal, I added code to the Signal 
Response FSM that changed how the refference siganl fed to the controller was 
generated. In Lab 0x06, I used the test parameters sent from the PC to generate
a timed step signal. For this lab, I configured the FSM to send the stored refference 
signal values as the test progressed. Essentially, every iteration, the Signal 
Response FSM updated the controller's set point to the reference omega value at
the current test time. From the perspective of the controller FSM nothing changed 
between Labs 6 and 7.

When the refference signal was sucessfully being sent to the controller, I started
to tune the system. I encountered two significant problems during this process.
The first was memory alocation on the Nucleo. Initially, I struggled to store 
more than 100 data points. To solve this, I changed all of my data storage variables 
to arrays instead of lists, and I eliminated a few areas that needlessly duplcated
my data arrays. The second issue that I faced had to do with friction in my motor
assemblies. I found that one of my pulley sets had a tendancy to bind. I tried 
taking the assembly apart, but I could not get it to stop binding. I ended up 
completing the lab using the other motor instead. While it did not have the binding
issue, it did have a significant amount of friction. I was unable to control the motor
at the low speeds of the given refference file. I found that my gains were either too small
to overcome the system friction, or too large to produce a stable response.
After reading a Piazza post from one of my calssmates who was struggaling with
the same issue, I decided to scale the refference signal to speeds that I knew
motor could stably maintain. The scale factor that I used was 20 This produced 
an initial speed of 600rpm, a second speed of -1200rpm, and a ramp peak of 600rpm.
Using this profile, I was able to sucessfully control my motor to follow the signal.
The scaled profile that I used is included in the file links below.

Starting with the final controller from Lab 0x06, I retuned my controller to minimize
the performance metric ('J') that I calculated. The best controller that I came up with
used KP = 0.13, KI = 0.00015. This controller produced a J of 46817.59, under half
the J of the fisr controller I tried. The following image shows the results of 
my best response test:
@image html lab0x06_Sig_Resp_1.png

This image shows the control signal over the course of the test. My gains were 
slightly on the high side since my controller hit the saturation limits I set at 70%,
but these higher gains produced significantly lower J's than previous test. 
@image html lab0x06_Sig_Resp_2.png

The same nine python files that were created for Lab 0x06 were used for this assignemnt. 
They were silightly modified to add refference signal tracking. The tenth file
listed is the scaled refference signal that I used to overcome friction issues.
The source code for these files can be found here:
    * LAB_0x06_main.py: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305/me305%20Lab%200x06%20%26%20Lab%200x07/LAB_0x06_main.py
    * LAB_0x06_shares.py: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305/me305%20Lab%200x06%20%26%20Lab%200x07/LAB_0x06_shares.py
    * LAB_0x06_UI_FrontEnd.py: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305/me305%20Lab%200x06%20%26%20Lab%200x07/LAB_0x06_UI_FrontEnd.py
    * LAB_0x06_UI_BackEnd.py: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305/me305%20Lab%200x06%20%26%20Lab%200x07/LAB_0x06_UI_BackEnd.py
    * LAB_0x06_Control_FSM.py: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305/me305%20Lab%200x06%20%26%20Lab%200x07/LAB_0x06_Control_FSM.py
    * signal_test.py: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305/me305%20Lab%200x06%20%26%20Lab%200x07/signal_test.py
    * m_driver.py: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305/me305%20Lab%200x06%20%26%20Lab%200x07/m_driver.py
    * enc_driver.py: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305/me305%20Lab%200x06%20%26%20Lab%200x07/enc_driver.py
    * cl_driver.py: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305/me305%20Lab%200x06%20%26%20Lab%200x07/cl_driver.py
    * reference_EWN.csv: https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305/me305%20Lab%200x06%20%26%20Lab%200x07/reference_EWN.csv
'''
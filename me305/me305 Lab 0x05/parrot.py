'''
@file parrot.py

@author E. Nicholson

@date November 9, 2020

@brief      Simple tool to check bluetooth connection / information
            
            The source code for this file can be found here: 
    
            https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305%20Lab%200x05/parrot.py
'''

import pyb

## Configuring the on board user LED on the hardware
pinA5 = pyb.Pin(pyb.Pin.cpu.A5)

uart = pyb.UART(2)

print('init parrot')

while True:
    if uart.any() != 0:
        val = uart.read()
        print('NUCLEO recieved: ' + str(val))

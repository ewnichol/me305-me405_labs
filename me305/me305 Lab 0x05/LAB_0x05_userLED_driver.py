'''
@file LAB_0x05_userLED_driver.py

@author E. Nicholson

@date November 09, 2020

@brief      This file contains an LED driver
            
            The source code for this file can be found here: 
    
            https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305%20Lab%200x05/LAB_0x05_userLED_driver.py
'''

from math import sin, pi

class LED_Pat_Driver:
    '''
    @brief      Enables the selection of a brightness pattern for a PWM controlled LED
    @details    This driver allows a user to controll the brightness pattern of 
                a PWM driven LED. The LED can be set to one of five states: OFF,
                ON, BLINK, SINE, SAW. When update() is called, the driver sets
                the brightness of the LED depending on the state. For OFF and ON,
                update maintains the correct brightness percentage (0 or 100). For
                BLINK, SINE, and SAW, update advances the pattern one iteration.
                SINE and SAW are implemented using the same method as LAB 0x02.
    '''

    def __init__(self, ID, phys_LED, debug):
        '''
        @brief                          Creates a physical_LED_task object
        @param ID                       A string identifying the object for traceing output.
        @param phys_LED                 The LED object controlled by this driver
        @param debug                    A boolean that toggles the trace output on or off
        '''
        
        # ---- General Setup ----
        
        ##  The starting state of the LED.
        self.state = b'OFF'
        
        ## LED brightness
        self.brightness = 0
            
        ##  The unique ID string for the object, used for trace output
        self.ID = ID
        
        ## A boolean that toggles the trace output to the command line
        self.debug = debug
        
        ##  The timer objct that has been pre configured to a specific channel and LED
        self.LED = phys_LED

        # ---- Indexing & Counting ----

        ##  The starting value for my index
        self.pattern_idx = 0

        ##  The amount that I want my pattern index to advance each iteration
        self.pattern_idx_delta = 15
        
    def update(self):
        '''
        @brief  Updates the LED once
        '''

        if(self.state == b'OFF'):
            # Run OFF
            self.brightness = 0
            
        elif(self.state == b'ON'):
            # Run ON
            self.brightness = 100
        
        elif(self.state == b'BLINK'):
            # Run BLINK
            if self.brightness == 0:
                self.brightness = 100
            else:
                self.brightness = 0
            
        elif(self.state == b'SINE'):
            # Run SINE
            
            # if needed, the pattern index is reset
            if self.pattern_idx == 360:
                self.pattern_idx = 0
            
            self.brightness = sin(pi*self.pattern_idx/180)*50+50
           
            # advances the index
            self.pattern_idx += self.pattern_idx_delta
        
        elif(self.state == b'SAW'):
            # Run SAW
            
            # if needed, the pattern index is reset
            if self.pattern_idx == 360:
                self.pattern_idx = 0
            
            self.brightness = 100*self.pattern_idx/345               

            # advances the index
            self.pattern_idx += self.pattern_idx_delta
        
        else:
            # Invalid state code (error handling)
            self.brightness = 0
        
        
        self.LED.pulse_width_percent(self.brightness) 
        
    def Change_State(self, newState):
        '''
        @brief      Updates the variable defining the state of the LED and resets the pattern index
        '''
        self.pattern_idx = 0
        self.state = newState
        




























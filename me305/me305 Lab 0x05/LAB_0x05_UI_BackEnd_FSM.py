'''
@file LAB_0x05_UI_BackEnd_FSM.py

@author E. Nicholson

@date November 09, 2020

@brief      Contins the FSM that implements the back end of Lab 0x05's UI

            The source code for this file can be found here: 
    
            https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305%20Lab%200x05/LAB_0x05_UI_BackEnd_FSM.py
'''
from pyb import UART
import utime
        
class UI_FSM:
    '''
    @brief      Implements a FSM that represents the back end of Lab 0x05's UI
   
    @details    This class represents a finite state machine that communicates
                with a front end UI on a bluetooth connected anrdoid device. The
                FSM recieves commands from the device to control an LED that is
                driven by LED_Pat_Driver. The mobile user can adjust the state of
                the LED, and the frequency at which a brightness pattern is displayed.
                The FSM updates the LED while waiting for commands. The LED update
                speed is able to be configured separtely from the UI refresh rate.
                The following image shows the state flow diagram for this FSM.
                
    @image html lab0x05_UI_FSM_stateflow.png
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT             = 0    
    
    ## Constant defining State 1 - Command
    S1_COMMAND          = 1
    
    ## Constant defining State 2 - Update
    S2_UPDATE           = 2
    

    def __init__(self, ID, LED, UART_ID, ref_rate, debug):
        '''
        @brief              Creates a recorder object
        @param ID           A string identifying the object for tracing output.
        @param LED          The LED object.
        @param UART_ID      The UART ID number that will be used for serial communication
        @param ref_rate     The refresh rate of the record task
        @param debug        A boolean that toggles the trace output on or off
        '''
        
        # ---- General Setup ----
        
        ##  The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ##  The unique ID string for the object, used for trace output
        self.ID = ID
        
        ## A boolean that toggles the trace output to the command line
        self.debug = debug
                
        ## The encoder object that the task interfeces with
        self.LED = LED
        
        ## UART object for handling simple serial communication.
        # It is hard coded with a baudrate of 9600 to match the default of the
        # HC-05 bluetooth module used
        self.uart = UART(UART_ID, 9600)
        # self.uart = UART(UART_ID)
                
        ## The list of standard commands
        self.cmd = [b'OFF',b'ON',b'BLINK',b'SINE',b'SAW']

        # ---- Timing Configuration ----

        ##  The base level refresh rate for the encoder position in microseconds
        self.base_ref_rate = ref_rate
        
        ##  The refresh rate for the FSM in microseconds
        self.ref_rate = self.base_ref_rate
        
        ##  The initail frequency at which the LED blinks / the pattern cycle is run in Hz recorded for refference
        self.base_freq = 1
        
        ##  The initail frequency at which the LED blinks / the pattern cycle is run in Hz
        self.freq = self.base_freq

        ##  The timestamp for the initialization of the object in microseconds
        self.start_time = utime.ticks_us()
               
        ##  The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.ref_rate)
        
        ##  The "timestamp" for when the LED should update next
        self.next_update = utime.ticks_add(self.start_time, int(10**6/(self.freq)))
        
    def run(self):
        '''
        @brief      Runs one iteration of the UI task
        '''
        ## The current time at the start of an update
        self.curr_time = utime.ticks_us()
           
        if utime.ticks_diff(self.curr_time, self.next_time) > 0:
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                
                if self.debug:
                    self.trace()
                    # print('INIT UI Back End Task')
                    
                self.transitionTo(self.S1_COMMAND)
            
            elif(self.state == self.S1_COMMAND):
                # Run State 1 Code   
                
                if self.debug:
                    self.trace()
                
                if self.uart.any() != 0:
                    com = self.uart.read()
                    
                    isFreq = False
                    isCMD = False
                    
                    try:
                        freq = float(com)
                        isFreq = True
                    except:
                        cmd = com
                        isCMD = True
                        
                    if isFreq:
                        self.freq = freq/1000
                        # print('Frequency Sent: ' + str(self.freq))

                        
                    if isCMD:
                        # print('cmd understood: ' + str(cmd))
                        if cmd in self.cmd:
                            self.LED.Change_State(cmd)
                        else:
                            print('INVALID COMMAND')
                
                self.transitionTo(self.S2_UPDATE)
                self.send_status()
            
            elif(self.state == self.S2_UPDATE):
                # Run State 2 Code 
                
                if self.debug:
                    self.trace()
                
                if utime.ticks_diff(self.curr_time, self.next_update) > 0:
                    self.LED.update()
                    print('LED State: ' + str(self.LED.state) + ' Frequency: ' + str(self.freq) + ' Brichtness: ' + str(self.LED.brightness))
                    if self.LED.state in [b'SINE',b'SAW']:
                        self.next_update = utime.ticks_add(self.next_update, int(10**6/(self.freq*24)))
                    else:
                        self.next_update = utime.ticks_add(self.next_update, int(10**6/(self.freq)))
                    
                if self.uart.any() != 0:
                    self.transitionTo(self.S1_COMMAND)
                
            else:
                # Invalid state code (error handling)
                pass
            
            self.next_time = utime.ticks_add(self.next_time, self.ref_rate)
        
        # print('C:'+str(self.curr_time)+' N:'+str(self.next_time)+' U:'+str(self.next_update))

    def send_status(self):
        '''
        @brief      Sends a status message to the connected device over bluetooth
        '''
        if self.LED.state == b'OFF':
            self.uart.write('LED Turned off \r\n')
        elif self.LED.state == b'ON':
            self.uart.write('LED Turned on \r\n')
        elif self.LED.state == b'BLINK':
            self.uart.write('LED blinking at ' + str(self.freq) + ' Hz \r\n')
        elif self.LED.state == b'SINE':
            self.uart.write('LED SINE at ' + str(self.freq) + ' Hz \r\n')
        elif self.LED.state == b'SAW':
            self.uart.write('LED SAW at ' + str(self.freq) + ' Hz \r\n')

    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        # print('change state')
        
'''
@file LAB_0x05_main.py

@author E. Nicholson

@date November 09, 2020

@brief      This file configures and runs the backend of the Lab 0x05 UI.

@details    This main file set up the User LED on the Nucleo to be driven with
            PWM. It passes this timer object to an LED driver that implements
            control over the brightness pattern. The LED driver is then run by
            a UI FSM that recieves command via Bluetooth to control the LED.
            
            The source code for this file can be found here: 
    
            https://bitbucket.org/ewnichol/me305-me405_labs/src/master/me305%20Lab%200x05/LAB_0x05_main.py
'''

import pyb
from LAB_0x05_userLED_driver import LED_Pat_Driver
from LAB_0x05_UI_BackEnd_FSM import UI_FSM


## Configuring the on board user LED on the hardware
pinA5 = pyb.Pin(pyb.Pin.cpu.A5)

## Initialize the timer that will control the LED through pulse width modulation
tim2 = pyb.Timer(2, freq = 20000)

## Passes the LED to the timer and sets up PWM
phys_LED = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)

## Sends the User LED to the LED Driver
drivenLED = LED_Pat_Driver('LED1', phys_LED, False)

## Initializes the UI back end FSM
task1 = UI_FSM('UI1', drivenLED, 3, 1000, False)

# Run the tasks in sequence over and over again
while True:
    task1.run()


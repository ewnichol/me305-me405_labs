'''
@file   mainpage.py
@brief  This homepage contains a full list of projects with links to further documentation.

@mainpage

@section sec_intro Introduction
@author Enoch Nicholson

This documentation portfolio showcases all of the work completed in ME 305 & ME 405.

comment

ME 305 Documentation can be found here:

https://ewnichol.bitbucket.io/me305/

ME 405 Documentation can be found here:
    
https://ewnichol.bitbucket.io/me405/
'''